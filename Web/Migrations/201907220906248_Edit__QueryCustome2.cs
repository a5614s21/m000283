namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__QueryCustome2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "ultrasound_pic", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic_alt", c => c.String());
            DropColumn("dbo.query_custome", "ultrasound_pic1");
            DropColumn("dbo.query_custome", "ultrasound_pic1_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic2");
            DropColumn("dbo.query_custome", "ultrasound_pic2_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic3");
            DropColumn("dbo.query_custome", "ultrasound_pic3_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic4");
            DropColumn("dbo.query_custome", "ultrasound_pic4_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic5");
            DropColumn("dbo.query_custome", "ultrasound_pic5_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic6");
            DropColumn("dbo.query_custome", "ultrasound_pic6_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic7");
            DropColumn("dbo.query_custome", "ultrasound_pic7_alt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.query_custome", "ultrasound_pic7_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic7", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic6_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic6", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic5_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic5", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic4_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic4", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic3_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic3", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic2_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic2", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic1_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic1", c => c.String());
            DropColumn("dbo.query_custome", "ultrasound_pic_alt");
            DropColumn("dbo.query_custome", "ultrasound_pic");
        }
    }
}
