namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Assess_Table : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.assess_table",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        barcode = c.String(),
                        user_id = c.String(),
                        name = c.String(),
                        sex = c.String(),
                        birthday = c.String(),
                        checkdate = c.String(),
                        no = c.String(),
                        age = c.String(),
                        chart = c.String(),
                        sogi = c.String(),
                        email = c.String(),
                        chiefcomplaint = c.String(),
                        pasthistory = c.String(),
                        familyhistory = c.String(),
                        allergyhistory = c.String(),
                        vaccinationhistory = c.String(),
                        livinghabit = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.assess_table");
        }
    }
}
