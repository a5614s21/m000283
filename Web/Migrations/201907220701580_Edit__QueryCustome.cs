namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__QueryCustome : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "pic1", c => c.String());
            AddColumn("dbo.query_custome", "pic1_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic2", c => c.String());
            AddColumn("dbo.query_custome", "pic2_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic3", c => c.String());
            AddColumn("dbo.query_custome", "pic3_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic4", c => c.String());
            AddColumn("dbo.query_custome", "pic4_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic5", c => c.String());
            AddColumn("dbo.query_custome", "pic5_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic6", c => c.String());
            AddColumn("dbo.query_custome", "pic6_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic7", c => c.String());
            AddColumn("dbo.query_custome", "pic7_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic8", c => c.String());
            AddColumn("dbo.query_custome", "pic8_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic9", c => c.String());
            AddColumn("dbo.query_custome", "pic9_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic10", c => c.String());
            AddColumn("dbo.query_custome", "pic10_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic11", c => c.String());
            AddColumn("dbo.query_custome", "pic11_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic12", c => c.String());
            AddColumn("dbo.query_custome", "pic12_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic13", c => c.String());
            AddColumn("dbo.query_custome", "pic13_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic14", c => c.String());
            AddColumn("dbo.query_custome", "pic14_alt", c => c.String());
            AddColumn("dbo.query_custome", "pic15", c => c.String());
            AddColumn("dbo.query_custome", "pic15_alt", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.query_custome", "pic15_alt");
            DropColumn("dbo.query_custome", "pic15");
            DropColumn("dbo.query_custome", "pic14_alt");
            DropColumn("dbo.query_custome", "pic14");
            DropColumn("dbo.query_custome", "pic13_alt");
            DropColumn("dbo.query_custome", "pic13");
            DropColumn("dbo.query_custome", "pic12_alt");
            DropColumn("dbo.query_custome", "pic12");
            DropColumn("dbo.query_custome", "pic11_alt");
            DropColumn("dbo.query_custome", "pic11");
            DropColumn("dbo.query_custome", "pic10_alt");
            DropColumn("dbo.query_custome", "pic10");
            DropColumn("dbo.query_custome", "pic9_alt");
            DropColumn("dbo.query_custome", "pic9");
            DropColumn("dbo.query_custome", "pic8_alt");
            DropColumn("dbo.query_custome", "pic8");
            DropColumn("dbo.query_custome", "pic7_alt");
            DropColumn("dbo.query_custome", "pic7");
            DropColumn("dbo.query_custome", "pic6_alt");
            DropColumn("dbo.query_custome", "pic6");
            DropColumn("dbo.query_custome", "pic5_alt");
            DropColumn("dbo.query_custome", "pic5");
            DropColumn("dbo.query_custome", "pic4_alt");
            DropColumn("dbo.query_custome", "pic4");
            DropColumn("dbo.query_custome", "pic3_alt");
            DropColumn("dbo.query_custome", "pic3");
            DropColumn("dbo.query_custome", "pic2_alt");
            DropColumn("dbo.query_custome", "pic2");
            DropColumn("dbo.query_custome", "pic1_alt");
            DropColumn("dbo.query_custome", "pic1");
        }
    }
}
