namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__LabData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.lab_data", "content", c => c.String());
            DropColumn("dbo.lab_data", "itemNo");
            DropColumn("dbo.lab_data", "itemName");
            DropColumn("dbo.lab_data", "eitemName");
            DropColumn("dbo.lab_data", "itemCategory");
            DropColumn("dbo.lab_data", "eitemCategory");
            DropColumn("dbo.lab_data", "err");
            DropColumn("dbo.lab_data", "value");
            DropColumn("dbo.lab_data", "Ref");
            DropColumn("dbo.lab_data", "orderindex");
            DropColumn("dbo.lab_data", "system_name");
            DropColumn("dbo.lab_data", "category_name");
            DropColumn("dbo.lab_data", "category_orderindex");
        }
        
        public override void Down()
        {
            AddColumn("dbo.lab_data", "category_orderindex", c => c.String());
            AddColumn("dbo.lab_data", "category_name", c => c.String());
            AddColumn("dbo.lab_data", "system_name", c => c.String());
            AddColumn("dbo.lab_data", "orderindex", c => c.String());
            AddColumn("dbo.lab_data", "Ref", c => c.String());
            AddColumn("dbo.lab_data", "value", c => c.String());
            AddColumn("dbo.lab_data", "err", c => c.String());
            AddColumn("dbo.lab_data", "eitemCategory", c => c.String());
            AddColumn("dbo.lab_data", "itemCategory", c => c.String());
            AddColumn("dbo.lab_data", "eitemName", c => c.String());
            AddColumn("dbo.lab_data", "itemName", c => c.String());
            AddColumn("dbo.lab_data", "itemNo", c => c.String());
            DropColumn("dbo.lab_data", "content");
        }
    }
}
