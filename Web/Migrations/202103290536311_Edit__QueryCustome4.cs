namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__QueryCustome4 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "password", c => c.String());
            AddColumn("dbo.query_custome", "setpw_datetime", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.query_custome", "setpw_datetime");
            DropColumn("dbo.query_custome", "password");
        }
    }
}
