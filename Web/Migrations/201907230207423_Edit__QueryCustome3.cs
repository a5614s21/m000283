namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__QueryCustome3 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "pic", c => c.String());
            AddColumn("dbo.query_custome", "pic_alt", c => c.String());
            DropColumn("dbo.query_custome", "ultrasound_pic");
            DropColumn("dbo.query_custome", "ultrasound_pic_alt");
        }
        
        public override void Down()
        {
            AddColumn("dbo.query_custome", "ultrasound_pic_alt", c => c.String());
            AddColumn("dbo.query_custome", "ultrasound_pic", c => c.String());
            DropColumn("dbo.query_custome", "pic_alt");
            DropColumn("dbo.query_custome", "pic");
        }
    }
}
