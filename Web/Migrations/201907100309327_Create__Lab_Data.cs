namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Lab_Data : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.lab_data",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        barcode = c.String(),
                        checkdate = c.String(),
                        itemNo = c.String(),
                        itemName = c.String(),
                        eitemName = c.String(),
                        itemCategory = c.String(),
                        eitemCategory = c.String(),
                        err = c.String(),
                        value = c.String(),
                        Ref = c.String(),
                        orderindex = c.String(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                        system_name = c.String(),
                        category_name = c.String(),
                        category_orderindex = c.String(),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.lab_data");
        }
    }
}
