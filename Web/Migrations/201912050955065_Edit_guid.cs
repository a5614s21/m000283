namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit_guid : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.assess_table", "query_guid", c => c.String());
            AddColumn("dbo.gauge_data", "query_guid", c => c.String());
            AddColumn("dbo.lab_data", "query_guid", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.lab_data", "query_guid");
            DropColumn("dbo.gauge_data", "query_guid");
            DropColumn("dbo.assess_table", "query_guid");
        }
    }
}
