namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__GaugeData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.gauge_data", "old_barcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.gauge_data", "old_barcode");
        }
    }
}
