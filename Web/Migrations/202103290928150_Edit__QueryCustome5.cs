namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__QueryCustome5 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "first", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.query_custome", "first");
        }
    }
}
