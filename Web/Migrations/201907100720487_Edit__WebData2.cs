namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData2 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "googlemapurl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "googlemapurl");
        }
    }
}
