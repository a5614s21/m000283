namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Edit_User : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.user", "autologout_hour", c => c.Int());
            Sql("execute sp_addextendedproperty 'MS_Description', N'�۰ʵn�X(��)' ,'SCHEMA', N'dbo','TABLE', N'user', 'COLUMN', N'autologout_hour'");
        }

        public override void Down()
        {
            DropColumn("dbo.user", "autologout_hour");
        }
    }
}