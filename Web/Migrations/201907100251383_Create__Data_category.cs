namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Create__Data_category : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.data_category",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        itemNo = c.String(),
                        itemName = c.String(),
                        itemSystem = c.String(),
                        itemCategory = c.String(),
                        orderindex = c.Int(),
                        status = c.String(maxLength: 1),
                        modifydate = c.DateTime(),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });
            
        }
        
        public override void Down()
        {
            DropTable("dbo.data_category");
        }
    }
}
