namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addDownloadUrl : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "downloadUrl", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.query_custome", "downloadUrl");
        }
    }
}
