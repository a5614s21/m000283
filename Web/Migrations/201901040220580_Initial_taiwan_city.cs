namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial_taiwan_city : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.mail_contents",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        guid = c.String(nullable: false, maxLength: 64),
                        title = c.String(),
                        content = c.String(),
                        modifydate = c.DateTime(),
                        status = c.String(maxLength: 1),
                        create_date = c.DateTime(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => new { t.id, t.guid });

            Sql("execute sp_addextendedproperty 'MS_Description', N'標題' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'title'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'內容' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'content'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'異動日期' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'modifydate'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'建檔日期' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'create_date'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'狀態' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'status'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'語系' ,'SCHEMA', N'dbo','TABLE', N'mail_contents', 'COLUMN', N'lang'");

            CreateTable(
                "dbo.taiwan_city",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        city = c.String(),
                        district = c.String(),
                        zip = c.String(),
                        lay = c.String(),
                        sortIndex = c.Int(),
                        lang = c.String(maxLength: 30),
                    })
                .PrimaryKey(t => t.id);

            Sql("execute sp_addextendedproperty 'MS_Description', N'縣市' ,'SCHEMA', N'dbo','TABLE', N'taiwan_city', 'COLUMN', N'city'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'區域' ,'SCHEMA', N'dbo','TABLE', N'taiwan_city', 'COLUMN', N'district'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'郵遞區號' ,'SCHEMA', N'dbo','TABLE', N'taiwan_city', 'COLUMN', N'zip'");

            AddColumn("dbo.news", "seo_keywords", c => c.String());
            AddColumn("dbo.news", "seo_description", c => c.String());
            AddColumn("dbo.news_category", "seo_keywords", c => c.String());
            AddColumn("dbo.news_category", "seo_description", c => c.String());
            AddColumn("dbo.web_data", "seo_keywords", c => c.String());
            AddColumn("dbo.web_data", "seo_description", c => c.String());

            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'web_data', 'COLUMN', N'seo_description'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'news', 'COLUMN', N'seo_description'");

            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO關鍵字' ,'SCHEMA', N'dbo','TABLE', N'news_category', 'COLUMN', N'seo_keywords'");
            Sql("execute sp_addextendedproperty 'MS_Description', N'SEO敘述' ,'SCHEMA', N'dbo','TABLE', N'news_category', 'COLUMN', N'seo_description'");


        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "seo_description");
            DropColumn("dbo.web_data", "seo_keywords");
            DropColumn("dbo.news_category", "seo_description");
            DropColumn("dbo.news_category", "seo_keywords");
            DropColumn("dbo.news", "seo_description");
            DropColumn("dbo.news", "seo_keywords");
            DropTable("dbo.taiwan_city");
            DropTable("dbo.mail_contents");
        }
    }
}
