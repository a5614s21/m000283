namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class Edit_QueryCustome6 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.query_custome", "image_zip", c => c.String());
        }

        public override void Down()
        {
            DropColumn("dbo.query_custome", "image_zip");
        }
    }
}
