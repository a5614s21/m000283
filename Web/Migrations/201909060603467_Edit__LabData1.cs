namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__LabData1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.lab_data", "old_barcode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.lab_data", "old_barcode");
        }
    }
}
