namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__WebData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.web_data", "medical_servicedate", c => c.String());
            AddColumn("dbo.web_data", "outpatient_servicedate", c => c.String());
            AddColumn("dbo.web_data", "service_line", c => c.String());
            AddColumn("dbo.web_data", "registered_line", c => c.String());
            AddColumn("dbo.web_data", "network_registration", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.web_data", "network_registration");
            DropColumn("dbo.web_data", "registered_line");
            DropColumn("dbo.web_data", "service_line");
            DropColumn("dbo.web_data", "outpatient_servicedate");
            DropColumn("dbo.web_data", "medical_servicedate");
        }
    }
}
