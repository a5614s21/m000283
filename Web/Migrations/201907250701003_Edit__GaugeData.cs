namespace Web.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Edit__GaugeData : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.gauge_data", "content", c => c.String());
            DropColumn("dbo.gauge_data", "itemNo");
            DropColumn("dbo.gauge_data", "itemName");
            DropColumn("dbo.gauge_data", "eitemName");
            DropColumn("dbo.gauge_data", "itemCategory");
            DropColumn("dbo.gauge_data", "eitemCategory");
            DropColumn("dbo.gauge_data", "err");
            DropColumn("dbo.gauge_data", "value");
            DropColumn("dbo.gauge_data", "Ref");
            DropColumn("dbo.gauge_data", "orderindex");
            DropColumn("dbo.gauge_data", "system_name");
            DropColumn("dbo.gauge_data", "category_name");
            DropColumn("dbo.gauge_data", "category_orderindex");
        }
        
        public override void Down()
        {
            AddColumn("dbo.gauge_data", "category_orderindex", c => c.String());
            AddColumn("dbo.gauge_data", "category_name", c => c.String());
            AddColumn("dbo.gauge_data", "system_name", c => c.String());
            AddColumn("dbo.gauge_data", "orderindex", c => c.String());
            AddColumn("dbo.gauge_data", "Ref", c => c.String());
            AddColumn("dbo.gauge_data", "value", c => c.String());
            AddColumn("dbo.gauge_data", "err", c => c.String());
            AddColumn("dbo.gauge_data", "eitemCategory", c => c.String());
            AddColumn("dbo.gauge_data", "itemCategory", c => c.String());
            AddColumn("dbo.gauge_data", "eitemName", c => c.String());
            AddColumn("dbo.gauge_data", "itemName", c => c.String());
            AddColumn("dbo.gauge_data", "itemNo", c => c.String());
            DropColumn("dbo.gauge_data", "content");
        }
    }
}
