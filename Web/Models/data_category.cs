﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Web.Models
{
    public class data_category
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string itemNo { get; set; }

        public string itemName { get; set; }

        public string itemSystem { get; set; }

        public string itemCategory { get; set; }

        public int? orderindex { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }
    }
}