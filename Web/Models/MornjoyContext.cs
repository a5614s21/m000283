﻿namespace Web.Models
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class MornjoyContext : DbContext
    {
        // 您的內容已設定為使用應用程式組態檔 (App.config 或 Web.config)
        // 中的 'Model' 連接字串。根據預設，這個連接字串的目標是
        // 您的 LocalDb 執行個體上的 'Web.Models.Model' 資料庫。
        // 
        // 如果您的目標是其他資料庫和 (或) 提供者，請修改
        // 應用程式組態檔中的 'Model' 連接字串。
        public MornjoyContext()
            : base("name=Model")
        {
        }


        public virtual DbSet<ashcan> ashcan { get; set; }
        public virtual DbSet<contacts> contacts { get; set; }
        public virtual DbSet<google_analytics> google_analytics { get; set; }
        public virtual DbSet<language> language { get; set; }
        public virtual DbSet<news> news { get; set; }
        public virtual DbSet<news_category> news_category { get; set; }
        public virtual DbSet<role_permissions> role_permissions { get; set; }
        public virtual DbSet<roles> roles { get; set; }
        public virtual DbSet<smtp_data> smtp_data { get; set; }
        public virtual DbSet<system_data> system_data { get; set; }
        public virtual DbSet<system_menu> system_menu { get; set; }
        public virtual DbSet<user> user { get; set; }
        public virtual DbSet<user_role> user_role { get; set; }
        public virtual DbSet<web_data> web_data { get; set; }
        public virtual DbSet<taiwan_city> taiwan_city { get; set; }
        public virtual DbSet<mail_contents> mail_contents { get; set; }
        public virtual DbSet<firewalls> firewalls { get; set; }
        public virtual DbSet<system_log> system_log { get; set; }


        public virtual DbSet<data_category> data_category { get; set; }
        public virtual DbSet<query_custome> query_custome { get; set; }
        public virtual DbSet<assess_table> assess_table { get; set; }
        public virtual DbSet<gauge_data> gauge_data { get; set; }
        public virtual DbSet<lab_data> lab_data { get; set; }

        // 針對您要包含在模型中的每種實體類型新增 DbSet。如需有關設定和使用
        // Code First 模型的詳細資訊，請參閱 http://go.microsoft.com/fwlink/?LinkId=390109。


    }

  
}