﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;
using System.Linq;

namespace Web.Models
{
    public class query_custome
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string title { get; set; }

        public string barcode { get; set; }

        public string customername { get; set; }

        public string user_id { get; set; }

        public string name { get; set; }

        public string sex { get; set; }

        public string birthday { get; set; }

        public string checkdate { get; set; }

        public string no { get; set; }

        public string age { get; set; }

        public string chart { get; set; }

        public string sogi { get; set; }

        public string email { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string pic { get; set; }

        public string pic_alt { get; set; }

        public string password { get; set; }

        public DateTime? setpw_datetime { get; set; }

        public string first { get; set; }

        public string image_zip { get; set; }
        public string downloadUrl { get; set; }

        [NotMapped]
        public List<string> CriticalPics 
        {
            get 
            {
                if (string.IsNullOrEmpty(this.pic))
                    return new List<string>();

                var pics = JObject.Parse(this.pic) as dynamic;

                string cpics = pics.cpic;

                if (string.IsNullOrEmpty(cpics))
                    return new List<string>();

                return cpics.Split(',').ToList();
            }
        }
        [NotMapped]
        public List<string> CriticalPicAlts 
        {
            get
            {
                if (string.IsNullOrEmpty(this.pic_alt))
                    return new List<string>();

                var picalts = JObject.Parse(this.pic_alt) as dynamic;

                string cpicalts = picalts.cpic_alt;

                if (string.IsNullOrEmpty(cpicalts))
                    return new List<string>();

                return cpicalts.Split('§').ToList();
            }
        }

    }
}