﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Web.Models
{
    public class assess_table
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string barcode { get; set; }

        public string user_id { get; set; }

        public string name { get; set; }

        public string sex { get; set; }

        public string birthday { get; set; }

        public string checkdate { get; set; }

        public string no { get; set; }

        public string age { get; set; }

        public string chart { get; set; }

        public string sogi { get; set; }

        public string email { get; set; }

        public string chiefcomplaint { get; set; }

        public string pasthistory { get; set; }

        public string familyhistory { get; set; }

        public string allergyhistory { get; set; }

        public string vaccinationhistory { get; set; }

        public string livinghabit { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string query_guid { get; set; }
    }
}