﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Spatial;

namespace Web.Models
{
    public class gauge_data
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int id { get; set; }

        [Key]
        [Column(Order = 1)]
        [StringLength(64)]
        public string guid { get; set; }

        public string barcode { get; set; }

        public string old_barcode { get; set; }

        public string checkdate { get; set; }

        public string content { get; set; }

        [StringLength(1)]
        public string status { get; set; }

        public DateTime? modifydate { get; set; }

        public DateTime? create_date { get; set; }

        [StringLength(30)]
        public string lang { get; set; }

        public string query_guid { get; set; }
    }
}