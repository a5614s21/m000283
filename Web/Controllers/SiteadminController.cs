﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Models;
using Newtonsoft.Json;
using Web.Service;
using System.Collections.Specialized;
using System.Collections;
using System.Web.UI;
using Web.Repository;
using System.Configuration;
using Google.Apis.Auth.OAuth2;
using System.Data.Entity;
using System.Linq.Dynamic;
using System.Web.Script.Serialization;
using Web.ViewModel;
using Pechkin;
using System.Drawing.Printing;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http;
using static Web.MvcApplication;

namespace Web.Controllers
{
    public class SiteadminController : Controller
    {
        #region -- 預設 --

        public string adminCathType = ConfigurationManager.ConnectionStrings["adminCathType"].ConnectionString;
        public string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;

        //預設語系
        //後台登入記錄方式 Session or Cookie
        public string projeftNo = ConfigurationManager.ConnectionStrings["projeftNo"].ConnectionString;

        private MornjoyContext DB = new MornjoyContext();
        //案件編號

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //登入判斷
            ViewBag.projeftNo = projeftNo;

            ViewBag.username = "";
            ViewBag.role_guid = null;

            ViewBag.defLang = defLang;

            ViewBag.permissions = Session["permissions"];
            ViewBag.permissionsTop = Session["permissionsTop"];

            //if (Request.Cookies["keepMeAccount"] != null || Session["sysUsername"] != null)
            //{
            //    if (adminCathType == "Session" && Session["sysUsername"] != null && Session["sysUsername"].ToString() != "")
            //    {
            //        ViewBag.username = Session["sysUsername"].ToString();
            //    }
            //    else
            //    {
            //        if (Request.Cookies["sysLogin"] != null)
            //        {
            //            HttpCookie aCookie = Request.Cookies["sysLogin"];
            //            if (aCookie != null && aCookie.Value != "" && aCookie.Value.IndexOf("&") != -1)
            //            {
            //                List<string> sysUser = aCookie.Value.Split('&').ToList();
            //                string sysUsername = Server.HtmlEncode(sysUser[0].Replace("sysUsername=", ""));
            //                ViewBag.username = sysUsername;
            //            }
            //        }
            //    }
            //}

            System.Web.HttpContext context = System.Web.HttpContext.Current;
            if (context.Session["sysUsername"] != null || context.Request.Cookies["sysLogin"] != null)
            {
                if (adminCathType == "Session")
                {
                    if (context.Session["sysUsername"] != null && context.Session["sysUsername"].ToString() != "")
                    {
                        ViewBag.username = Session["sysUsername"].ToString();
                    }
                }
                else
                {
                    if (context.Request.Cookies["sysLogin"] != null)
                    {
                        HttpCookie aCookie = Request.Cookies["sysLogin"];
                        if (aCookie != null && aCookie.Value != "" && aCookie.Value.IndexOf("&") != -1)
                        {
                            List<string> sysUser = aCookie.Value.Split('&').ToList();
                            string sysUsername = Server.HtmlEncode(sysUser[0].Replace("sysUsername=", ""));
                            ViewBag.username = sysUsername;
                        }
                    }
                }
            }

            if (ViewBag.username == "sysadmin")
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").ToList();
            }
            else
            {
                ViewBag.system_menu = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.status == "Y").Where(m => m.area != "admin").ToList();
            }

            ViewBag.system_menu_data = null;
            ViewBag.tables = "";
            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();
                ViewBag.system_menu_data = DB.system_menu.OrderBy(m => m.sortindex).Where(m => m.tables == tables).FirstOrDefault();

                string topCategory = ViewBag.system_menu_data.category;
                ViewBag.system_menu_top_data = DB.system_menu.Where(m => m.guid == topCategory).FirstOrDefault();
                ViewBag.tables = RouteData.Values["tables"].ToString();

                if (tables == "notes_data")
                {
                    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                    {
                        string act_path = "edit/" + RouteData.Values["id"].ToString();

                        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                    }
                }
                if (tables == "governances")
                {
                    if (RouteData.Values["id"] != null && RouteData.Values["id"].ToString() != "")
                    {
                        string act_path = "list/" + RouteData.Values["id"].ToString();

                        ViewBag.system_menu_data = DB.system_menu.Where(m => m.tables == tables).Where(m => m.act_path == act_path).FirstOrDefault();
                    }
                }
            }

            ViewBag.language = null;
            ViewBag.viewLanguage = "語系";
            var language = DB.language.OrderBy(m => m.sortIndex).Where(m => m.status == "Y").ToList();
            if (language.Count > 0)
            {
                ViewBag.language = language;
                ViewBag.viewLanguage = language[0].title;
            }

            ViewBag.userData = FunctionService.ReUserData();//回傳使用者資訊

            //系統資訊
            Guid systemGuid = Guid.Parse("4795DABF-18DE-490E-9BB2-D57B4D99C127");
            ViewBag.systemData = DB.system_data.Where(m => m.guid == systemGuid).FirstOrDefault();

            //
            ViewBag.WebDataGuid = DB.system_menu.Where(x => x.tables == "web_data" && x.status == "Y").SingleOrDefault();
        }

        #endregion

        #region -- 登入/登出 --

        /// <summary>
        /// 登入
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult login()
        {
            ViewBag.username = "";
            if (Request.Cookies["keepMeAccount"] != null)
            {
                HttpCookie aCookie = Request.Cookies["keepMeAccount"];

                string keepAdminID = Server.HtmlEncode(aCookie.Value.Replace("keepMeAccount=", ""));

                if (keepAdminID != null && keepAdminID != "")
                {
                    ViewBag.account = keepAdminID;
                }
            }

            bool isLogin = FunctionService.systemUserCheck();
            if (isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin"));
            }

            return View();
        }

        /// <summary>
        /// 登出
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Logout()
        {
            Response.Cookies.Clear();

            //FormsAuthentication.SignOut();

            HttpCookie c = new HttpCookie("sysLogin");
            c.Values.Remove("adminDataID");
            c.Values.Remove("adminCategory");
            c.Values.Remove("adminUsername");
            c.Values.Remove("sysUsername");
            c.Values.Remove("sysUserGuid");

            c.Expires = DateTime.Now.AddDays(-1);
            Response.Cookies.Add(c);

            Session.Clear();

            return RedirectPermanent(Url.Content("~/siteadmin/login"));
        }

        #endregion

        #region -- 首頁/列表/修改 --

        /// <summary>
        /// 新增/修改
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult edit()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            //權限
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            ViewBag.mainCol = null;
            ViewBag.mediaCol = null;
            ViewBag.otherCol = null;
            ViewBag.guid = "";
            ViewBag.data = null;
            ViewBag.addTitle = "";

            ViewBag.ultrasound = null;
            ViewBag.digitaldualenergy = null;
            ViewBag.chestdualenergy = null;
            ViewBag.xray = null;
            ViewBag.triple = null;
            ViewBag.endoscope = null;
            ViewBag.body = null;
            ViewBag.electrocardiogram = null;
            ViewBag.autonomic = null;
            ViewBag.fundus = null;

            ViewBag.addId = "";
            //dataTableCategory
            if (Session["dataTableCategory"] != null)
            {
                ViewBag.addId = "/" + Session["dataTableCategory"].ToString();
            }

            if (RouteData.Values["tables"] != null)
            {
                ViewBag.useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                ViewBag.RetuenID = "";
                //修改資料
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.guid = RouteData.Values["id"].ToString();
                    string Data = TablesService.getPresetData(RouteData.Values["tables"].ToString(), ViewBag.guid);

                    var obj = Newtonsoft.Json.Linq.JArray.Parse(Data.ToString());

                    if (ViewBag.useLang == "Y")
                    {
                        Dictionary<String, Object> langValData = new Dictionary<string, object>();
                        foreach (var item in obj)
                        {
                            langValData.Add(item["lang"].ToString(), item);
                        }

                        ViewBag.data = langValData;
                    }
                    else
                    {
                        ViewBag.data = obj[0];
                    }

                    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                    //因為DateTime的值Bind進入頁面，應該是ToString()了會出現中文格式，再反序列化後會失敗
                    if (ViewBag.data.setpw_datetime != null)
                    {
                        ViewBag.data.setpw_datetime = ViewBag.data.setpw_datetime.ToString("yyyy/MM/dd HH:mm:ss");
                    }
                }

                //子層
                if (Request["c"] != null)
                {
                    ViewBag.addTitle = TablesService.getDataTitle(RouteData.Values["tables"].ToString(), Request["c"].ToString());
                }

                ViewBag.Tables = RouteData.Values["tables"].ToString();

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), ViewBag.data, "edit");

                #region 基本欄位設定

                if (((Dictionary<String, Object>)colData["main"]).Count > 0)
                {
                    ViewBag.mainCol = colData["main"];
                }
                if (((Dictionary<String, Object>)colData["media"]).Count > 0)
                {
                    ViewBag.mediaCol = colData["media"];
                }
                if (((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    ViewBag.otherCol = colData["other"];
                }

                #endregion

                #region 延伸欄位設定

                if (colData.ContainsKey("ultrasound") && ((Dictionary<String, Object>)colData["ultrasound"]).Count > 0)
                {
                    ViewBag.ultrasound = colData["ultrasound"];
                }
                if (colData.ContainsKey("digitaldualenergy") && ((Dictionary<String, Object>)colData["digitaldualenergy"]).Count > 0)
                {
                    ViewBag.digitaldualenergy = colData["digitaldualenergy"];
                }
                if (colData.ContainsKey("chestdualenergy") && ((Dictionary<String, Object>)colData["chestdualenergy"]).Count > 0)
                {
                    ViewBag.chestdualenergy = colData["chestdualenergy"];
                }
                if (colData.ContainsKey("xray") && ((Dictionary<String, Object>)colData["xray"]).Count > 0)
                {
                    ViewBag.xray = colData["xray"];
                }
                if (colData.ContainsKey("triple") && ((Dictionary<String, Object>)colData["triple"]).Count > 0)
                {
                    ViewBag.triple = colData["triple"];
                }
                if (colData.ContainsKey("endoscope") && ((Dictionary<String, Object>)colData["endoscope"]).Count > 0)
                {
                    ViewBag.endoscope = colData["endoscope"];
                }
                if (colData.ContainsKey("body") && ((Dictionary<String, Object>)colData["body"]).Count > 0)
                {
                    ViewBag.body = colData["body"];
                }
                if (colData.ContainsKey("electrocardiogram") && ((Dictionary<String, Object>)colData["electrocardiogram"]).Count > 0)
                {
                    ViewBag.electrocardiogram = colData["electrocardiogram"];
                }
                if (colData.ContainsKey("autonomic") && ((Dictionary<String, Object>)colData["autonomic"]).Count > 0)
                {
                    ViewBag.autonomic = colData["autonomic"];
                }
                if (colData.ContainsKey("fundus") && ((Dictionary<String, Object>)colData["fundus"]).Count > 0)
                {
                    ViewBag.fundus = colData["fundus"];
                }

                if (colData.ContainsKey("user") && ((Dictionary<String, Object>)colData["user"]).Count > 0)
                {
                    ViewBag.userCol = colData["user"];
                }

                if (colData.ContainsKey("review") && ((Dictionary<String, Object>)colData["review"]).Count > 0)
                {
                    ViewBag.reviewCol = colData["review"];
                }
                if (colData.ContainsKey("verify") && ((Dictionary<String, Object>)colData["verify"]).Count > 0)
                {
                    ViewBag.verifyCol = colData["verify"];
                }
                if (colData.ContainsKey("verifyForum") && ((Dictionary<String, Object>)colData["verifyForum"]).Count > 0)
                {
                    ViewBag.verifyColForum = colData["verifyForum"];
                }

                if (colData.ContainsKey("reInfo") && ((Dictionary<String, Object>)colData["reInfo"]).Count > 0)
                {
                    ViewBag.reInfoCol = colData["reInfo"];
                }
                if (colData.ContainsKey("reUser") && ((Dictionary<String, Object>)colData["reUser"]).Count > 0)
                {
                    ViewBag.reUserCol = colData["reUser"];
                }
                if (colData.ContainsKey("modifyer") && ((Dictionary<String, Object>)colData["modifyer"]).Count > 0)
                {
                    ViewBag.modifyerCol = colData["modifyer"];
                }

                if (colData.ContainsKey("delay") && ((Dictionary<String, Object>)colData["delay"]).Count > 0)
                {
                    ViewBag.delayCol = colData["delay"];
                }

                if (colData.ContainsKey("delay2") && ((Dictionary<String, Object>)colData["delay2"]).Count > 0)
                {
                    ViewBag.delay2Col = colData["delay2"];
                }

                if (colData.ContainsKey("viewSet") && ((Dictionary<String, Object>)colData["viewSet"]).Count > 0)
                {
                    ViewBag.viewSetCol = colData["viewSet"];
                }

                if (colData.ContainsKey("hidden") && ((Dictionary<String, Object>)colData["hidden"]).Count > 0)
                {
                    ViewBag.hiddenCol = colData["hidden"];
                }
                if (colData.ContainsKey("indexUse") && ((Dictionary<String, Object>)colData["indexUse"]).Count > 0)
                {
                    ViewBag.reIndexUse = colData["indexUse"];
                }
                if (colData.ContainsKey("image_zip") && ((Dictionary<String, Object>)colData["image_zip"]).Count > 0)
                {
                    ViewBag.image_zip = colData["image_zip"];
                }
                if (colData.ContainsKey("critical") && ((Dictionary<String, Object>)colData["critical"]).Count > 0)
                {
                    ViewBag.critical = colData["critical"];
                }

                #endregion
            }
            return View();
        }

        /// <summary>
        /// 首頁
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }

            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }

            //GoogleAnalyticsService GaData = new GoogleAnalyticsService();
            //ServiceAccountCredential credential = GaData.GoogleAnalytics();

            //即時人數
            //Google.Apis.Analytics.v3.Data.RealtimeData RealtimeData = GaData.nowUser(credential);
            //ViewBag.RealtimeData = RealtimeData.TotalsForAllResults["rt:pageviews"].ToString();

            //本月流量次數
            //string thisMonth = int.Parse(DateTime.Now.ToString("MM")).ToString();
            //ViewBag.ViewYearUseMonth = JsonConvert.DeserializeObject(GaData.ViewYearUseMonth(thisMonth));

            //最新公告
            ViewBag.News = DB.news.Where(m => m.lang == defLang).OrderByDescending(m => m.startdate).ToList().Skip(0).Take(3);

            //聯絡我們
            ViewBag.contactData = DB.contacts.OrderByDescending(m => m.create_date).ToList().Skip(0).Take(4);

            return View();
        }

        /// <summary>
        /// 列表
        /// </summary>
        /// <returns></returns>
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult list()
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }
            if (ViewBag.permissions == null)
            {
                ViewBag.permissions = Session["permissions"];
                ViewBag.permissionsTop = Session["permissionsTop"];
            }
            Session.Remove("dataTableCategory");//移除暫存細項

            ViewBag.RetuenToList = "N";
            ViewBag.statusData = null;//狀態篩選顯示
            ViewBag.statusVal = null;//狀態篩選值

            ViewBag.RetuenID = "";

            if (RouteData.Values["tables"] != null)
            {
                string tables = RouteData.Values["tables"].ToString();

                ViewBag.Tables = tables;

                ViewBag.addTitle = "";
                ViewBag.addId = "";
                Session.Remove("dataTableCategory");

                //DataTable用額外參數
                ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': ''}";
                if (RouteData.Values["id"] != null)
                {
                    ViewBag.dataTableAddPostVal = "{'name': 'category', 'value': '" + RouteData.Values["id"].ToString() + "'}";

                    Session.Add("dataTableCategory", RouteData.Values["id"].ToString());//紀錄傳值

                    //取得標題

                    string reTables = ViewBag.Tables;

                    ViewBag.addTitle = TablesService.getDataTitle(reTables, RouteData.Values["id"].ToString());
                    ViewBag.addId = "?c=" + RouteData.Values["id"].ToString();
                    ViewBag.RetuenToList = "Y";
                    ViewBag.RetuenID = RouteData.Values["id"].ToString();
                }
                /*if(ViewBag.forum_status != null)
                {
                    ViewBag.dataTableAddPostVal += ",{'name': 'forum_status', 'value': '" + ViewBag.forum_status.ToString() + "'}";
                }*/

                ViewBag.listMessage = TablesService.listMessage(tables);//取得列表說明

                //取得表頭
                ViewBag.dataTableTitle = TablesService.dataTableTitle(tables);
                ViewBag.columns = "";
                ViewBag.columnsLength = 0;//移除動作排序

                Dictionary<String, Object> defaultOrderBy = TablesService.defaultOrderBy(tables);//取得預設排續欄位名稱
                if (!defaultOrderBy.ContainsKey("orderByKey"))
                {
                    defaultOrderBy.Add("orderByKey", "create_date");
                    defaultOrderBy.Add("orderByType", "desc");
                }

                ViewBag.defaultOrderBy = 1;//預設排續欄位
                ViewBag.defaultOrderType = defaultOrderBy["orderByType"].ToString();//預設排續(asc或desc)

                foreach (KeyValuePair<string, object> item in ViewBag.dataTableTitle)
                {
                    ViewBag.columns += "{\"data\": \"" + item.Key + "\" },";
                    if (item.Key == defaultOrderBy["orderByKey"].ToString())
                    {
                        ViewBag.defaultOrderBy = ViewBag.columnsLength;
                    }
                    ViewBag.columnsLength++;
                }

                ViewBag.columnsLength = ViewBag.columnsLength - 1;//移除動作排序

                #region 取得檢視篩選

                Dictionary<String, Object> tempData = new Dictionary<string, object>();
                tempData.Add("nums", 1);

                Dictionary<String, Object> colData = TablesService.getColData(RouteData.Values["tables"].ToString(), tempData, "list");

                if (colData.ContainsKey("other") && ((Dictionary<String, Object>)colData["other"]).Count > 0)
                {
                    Dictionary<String, Object> otherCol = (Dictionary<String, Object>)colData["other"];

                    if (otherCol["status"] != null && otherCol["status"].ToString() != "")
                    {
                        var mJObj = Newtonsoft.Json.Linq.JArray.Parse(otherCol["status"].ToString());
                        string[] statusData = mJObj[0]["data"].ToString().Split('/');//狀態篩選顯示
                        string[] statusVal = mJObj[0]["Val"].ToString().Split('/');//狀態篩選值

                        ViewBag.statusData = statusData;//狀態篩選顯示
                        ViewBag.statusVal = statusVal;//狀態篩選值
                    }
                }

                #endregion
            }

            HttpCookie cookie = Request.Cookies["keywords"];
            if (cookie != null)
            {
                ViewBag.keyword = HttpUtility.UrlDecode(cookie.Value);
            }
            return View();
        }

        #endregion

        #region -- 其他功能 --

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string Ajax(FormCollection form)
        {
            Dictionary<String, Object> dic = new Dictionary<string, object>();

            NameValueCollection getData = FunctionService.reSubmitFormDataJson(HttpUtility.UrlDecode(form["Val"].ToString()));//回傳JSON資料
            dic.Add("Func", form["Func"].ToString());

            switch (form["Func"].ToString())
            {
                #region 切換縣市區域

                case "changeCity":
                    dic["district"] = "";
                    if (getData != null)
                    {
                        List<taiwan_city> district = FunctionService.TaiwanCitys(getData["city"].ToString());
                        var jsonSerialiser = new JavaScriptSerializer();
                        var districtJson = jsonSerialiser.Serialize(district);
                        dic["district"] = districtJson;
                    }
                    break;

                #endregion

                #region 徹底刪除

                case "removeData":

                    if (getData != null)
                    {
                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            Guid guid = Guid.Parse(guidArr[i].ToString());
                            var ashcan = DB.ashcan.Where(m => m.guid == guid).FirstOrDefault();

                            switch (ashcan.tables)
                            {
                                case "forum":
                                    DB.Database.ExecuteSqlCommand("delete from forum where top_guid = '" + ashcan.from_guid + "'");
                                    DB.Database.ExecuteSqlCommand("delete from forum_message where forum_guid = '" + ashcan.from_guid + "'");
                                    break;
                            }
                            if (getData["type"].ToString() == "del")
                            {
                                //刪除其餘API相關資料表的資料
                                if (ashcan.tables == "query_custome")
                                {
                                    AssessTableService AssessTable = new AssessTableService();
                                    GaugeDataService GaugeData = new GaugeDataService();
                                    LabDataService LabData = new LabDataService();

                                    AssessTable.RemoveDataByQueryCustomeGuid(ashcan.from_guid);
                                    GaugeData.RemoveDataByQueryCustomeGuid(ashcan.from_guid);
                                    LabData.RemoveDataByQueryCustomeGuid(ashcan.from_guid);
                                }

                                DB.Database.ExecuteSqlCommand("delete from [" + ashcan.tables + "] where guid = '" + ashcan.from_guid + "'");
                            }
                            else
                            {
                                DB.Database.ExecuteSqlCommand("update [" + ashcan.tables + "] set status = 'N' where guid = '" + ashcan.from_guid + "'");
                            }

                            DB.Database.ExecuteSqlCommand("delete from [ashcan] where guid = '" + guid.ToString() + "'");
                        }
                    }
                    break;

                #endregion

                #region 修改狀態

                case "changeStatus":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系

                        string[] guidArr = getData["guid"].ToString().Split(',');
                        string[] titleArr = getData["title"].ToString().Split('|');
                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("status", getData["status"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("status", getData["status"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "status", guidArr[i].ToString(), "edit", null);
                            }
                        }

                        //資源回收桶
                        if (getData["status"].ToString() == "D")
                        {
                            ashcan ashcan = new ashcan();
                            for (int i = 0; i < guidArr.Length; i++)
                            {
                                ashcan.title = titleArr[i].ToString();
                                ashcan.tables = getData["tables"].ToString();
                                ashcan.from_guid = guidArr[i].ToString();
                                ashcan.create_date = DateTime.Now;
                                ashcan.modifydate = DateTime.Now;
                                DB.ashcan.Add(ashcan);

                                DB.SaveChanges();
                            }
                        }

                        dic["status"] = getData["status"].ToString();
                    }
                    break;

                #endregion

                #region 修改排序

                case "cahngeSortIndex":

                    if (getData != null)
                    {
                        string useLang = TablesService.useLang(getData["tables"].ToString());//是否使用語系
                        string[] guidArr = getData["guid"].ToString().Split(',');

                        for (int i = 0; i < guidArr.Length; i++)
                        {
                            /*Dictionary<String, Object> temp = new Dictionary<string, object>();
                            temp.Add("guid", guidArr[i].ToString());
                            temp.Add("sortIndex", getData["sortIndex"].ToString());
                            string jsonData = JsonConvert.SerializeObject(temp);
                            //呼叫寫入或修改
                            string guid = TablesService.saveData(getData["tables"].ToString(), guidArr[i].ToString(), "edit", jsonData, "sortIndex");
                            */
                            if (useLang == "Y")
                            {
                                if (ViewBag.language != null)
                                {
                                    foreach (language item in ViewBag.language)
                                    {
                                        Dictionary<String, Object> temp = new Dictionary<string, object>();
                                        temp.Add("guid", guidArr[i].ToString());
                                        temp.Add("sortIndex", getData["sortIndex"].ToString());
                                        temp.Add("lang", item.lang);
                                        string jsonData = JsonConvert.SerializeObject(temp);
                                        //呼叫寫入或修改
                                        string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                                    }
                                }
                            }
                            else
                            {
                                Dictionary<String, Object> temp = new Dictionary<string, object>();
                                temp.Add("guid", guidArr[i].ToString());
                                temp.Add("sortIndex", getData["sortIndex"].ToString());
                                string jsonData = JsonConvert.SerializeObject(temp);
                                //呼叫寫入或修改
                                string guid = TablesService.saveData(getData["tables"].ToString(), jsonData, "sortIndex", guidArr[i].ToString(), "edit", null);
                            }
                        }
                    }
                    break;

                #endregion

                #region 登入

                case "sysLogin":

                    if (getData != null)
                    {
                        system_log system_Log = new system_log();

                        dic["re"] = "OK";

                        if (getData["verification"].ToString() != Session["_ValCheckCode"].ToString())
                        {
                            dic["re"] = "codeError";
                            system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，驗證碼輸入錯誤!</code>";
                            system_Log.status = "N";
                        }
                        else
                        {
                            string username = getData["username"].ToString();

                            var users = DB.user.Where(m => m.username == username);
                            if (users.Count() <= 0)
                            {
                                dic["re"] = "usernameError";
                                system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，查無此帳號!</code>";
                                system_Log.status = "N";
                            }
                            else
                            {
                                string password = FunctionService.md5(getData["password"].ToString());

                                users = users.Where(m => m.password == password);
                                if (users.Count() <= 0)
                                {
                                    dic["re"] = "passwordError";
                                    system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，密碼比對錯誤!</code>";
                                    system_Log.status = "N";
                                }
                                else
                                {
                                    users = users.Where(m => m.status == "Y");
                                    if (users.Count() <= 0)
                                    {
                                        dic["re"] = "statusError";
                                        system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，帳號為停用狀態!</code>";
                                        system_Log.status = "N";
                                    }
                                    else
                                    {
                                        //判斷IP
                                        int firewallsCount = DB.firewalls.Where(m => m.status != "D").Count();

                                        bool ipCheck = true;

                                        //有IP判斷是否可通行
                                        if (firewallsCount > 0 && getData["username"].ToString() != "sysadmin")
                                        {
                                            string nowIP = FunctionService.GetIP();
                                            firewalls firewalls = DB.firewalls.Where(m => m.ip == nowIP).FirstOrDefault();

                                            if (firewalls == null)
                                            {
                                                dic["re"] = "ipError";
                                                ipCheck = false;
                                                system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                system_Log.status = "N";
                                            }
                                            else
                                            {
                                                if (firewalls.status == "N")
                                                {
                                                    dic["re"] = "ipError";
                                                    ipCheck = false;
                                                    system_Log.notes = "<code class=\"highlighter-rouge\">登入失敗，IP比對錯誤!</code>";
                                                    system_Log.status = "N";
                                                }
                                            }
                                        }

                                        if (ipCheck)
                                        {
                                            system_Log.notes = "登入成功";
                                            system_Log.status = "Y";

                                            if (adminCathType == "Session")
                                            {
                                                Session.Add("sysUsername", username);
                                                Session.Add("sysUserGuid", users.FirstOrDefault().guid);
                                            }
                                            else
                                            {
                                                //產生一個Cookie
                                                HttpCookie cookie = new HttpCookie("sysLogin");
                                                //設定過期日
                                                cookie.Expires = DateTime.Now.AddDays(365);
                                                cookie.Values.Add("sysUsername", username);//增加属性
                                                cookie.Values.Add("sysUserGuid", users.FirstOrDefault().guid);
                                                Response.AppendCookie(cookie);//确定写入cookie中
                                            }

                                            FunctionService.getAdminPermissions(username);//權限
                                                                                          //更新登入日期
                                            var saveData = DB.user.Where(m => m.username == username).Where(m => m.status == "Y").FirstOrDefault();
                                            saveData.logindate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                            DB.SaveChanges();
                                        }
                                    }

                                    if (getData["keepMe"] != null)
                                    {
                                        if (getData["keepMe"] == "Y")
                                        {
                                            //產生一個Cookie
                                            HttpCookie cookie2 = new HttpCookie("keepMeAccount");
                                            //設定單值
                                            cookie2.Value = getData["username"].ToString();
                                            //設定過期日
                                            cookie2.Expires = DateTime.Now.AddDays(365);
                                            //寫到用戶端
                                            Response.Cookies.Add(cookie2);
                                        }
                                    }
                                }
                            }

                            dic["url"] = Url.Content("~/siteadmin");
                        }

                        system_Log.title = "後台登入帳號：" + getData["username"].ToString();
                        system_Log.ip = FunctionService.GetIP();
                        system_Log.types = "sysLogin";
                        system_Log.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                        system_Log.guid = Guid.NewGuid().ToString();
                        if (getData["username"].ToString() != "sysadmin")
                        {
                            DB.system_log.Add(system_Log);
                            DB.SaveChanges();
                        }
                    }
                    break;

                    #endregion
            }

            string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
            //輸出json格式
            return json;
        }

        /// <summary>
        /// 帳號判斷
        /// </summary>
        [HttpGet]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public void CkAccount()
        {
            dynamic data = null;
            string username = Request["username"].ToString();

            if (RouteData.Values["tokens"] != null)
            {
                string[] inputAccount = RouteData.Values["tokens"].ToString().Split('@');

                switch (inputAccount[0])
                {
                    case "user":
                        data = DB.user.Where(m => m.username == username).Count();
                        break;
                }
            }
            else
            {
                data = DB.user.Where(m => m.username == username).Count();
            }

            if (data != null && data > 0)
            {
                Response.Write("false");
            }
            else
            {
                Response.Write("true");
            }
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public string dataTable()
        {
            EFUnitOfWork unitwork = new EFUnitOfWork(DB);
            Dictionary<String, Object> listData = new Dictionary<string, object>();

            //
            HttpCookie cookie = Request.Cookies["keywords"];
            var keyword = "";
            if (cookie != null)
            {
                keyword = cookie.Value;
            }
            //

            string json = "";

            dynamic data = null;

            if (RouteData.Values["tables"] != null)
            {
                NameValueCollection requests = Request.Params;

                data = TablesService.getListData(RouteData.Values["tables"].ToString(), requests, Url.Content("~/"), keyword);
            }

            listData = data;

            json = JsonConvert.SerializeObject(listData, Formatting.Indented);

            return json;
        }

        [HttpPost]
        [ValidateInput(false)]
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult save(FormCollection form)
        {
            if (RouteData.Values["tables"] != null)
            {
                string useLang = TablesService.useLang(RouteData.Values["tables"].ToString());//是否使用語系

                //呼叫寫入或修改
                string guid = FunctionService.getGuid();
                string actType = "add";
                if (form["guid"].ToString() != "")
                {
                    guid = form["guid"].ToString();
                    actType = "edit";
                }

                Dictionary<String, Object> dic = new Dictionary<string, object>();
                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            Dictionary<String, Object> subForm = new Dictionary<string, object>();
                            subForm.Add("lang", item.lang);

                            foreach (var key in form.AllKeys)
                            {
                                subForm.Add(key.Replace("_" + item.lang, ""), form[key].ToString());
                            }

                            if (actType == "add")
                            {
                                subForm.Remove("id");
                            }

                            dic.Add(item.lang, subForm);
                        }
                    }
                }
                else
                {
                    foreach (var key in form.AllKeys)
                    {
                        dic.Add(key, form[key].ToString());
                    }
                    if (actType == "add")
                    {
                        dic.Remove("id");
                    }
                }

                string Field = "";
                if (RouteData.Values["tables"].ToString() == "forum_message")
                {
                    Field = "forum_message";
                }

                if (useLang == "Y")
                {
                    if (ViewBag.language != null)
                    {
                        int i = 0;
                        foreach (language item in ViewBag.language)
                        {
                            string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic[item.lang]);
                            TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic[item.lang]);
                        }
                    }
                }
                else
                {
                    string json = JsonConvert.SerializeObject((Dictionary<string, object>)dic);
                    TablesService.saveData(RouteData.Values["tables"].ToString(), json, Field, guid, actType, (Dictionary<string, object>)dic);
                }

                string addId = "";
                //dataTableCategory
                if (Session["dataTableCategory"] != null)
                {
                    addId = "?c=" + Session["dataTableCategory"].ToString();
                }

                //完成轉跳
                TempData["success"] = true;
                switch (RouteData.Values["tables"].ToString())
                {
                    case "web_data":
                    case "smtp_data":
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid + addId);
                        break;

                    default:
                        TempData["Url"] = Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/list/");
                        break;
                }
                Response.Write("<script>window.location.href='" + Url.Content("~/siteadmin/" + RouteData.Values["tables"].ToString() + "/edit/" + guid + addId) + "';</script>");
            }
            return null;
        }

        #endregion

        #region -- GoogleAnalytics --

        public string analytics()
        {
            var GaData = new GoogleAnalyticsService();
            ServiceAccountCredential credential = GaData.GoogleAnalytics();
            string json = "";
            if (RouteData.Values["key"] != null)
            {
                switch (RouteData.Values["key"].ToString())
                {
                    //線上使用者/單次頁面/停留時間/跳出率
                    case "nowUser":
                        json = GaData.NowUser(credential);
                        break;
                    //瀏覽器使用率
                    case "browser":
                        json = GaData.ViewBrowser();
                        break;
                    //流量來源
                    case "organicSearches":
                        json = GaData.OrganicSearches(credential);
                        break;
                    //流量來源(波浪圖)
                    case "organicSearchesList":
                        json = GaData.OrganicSearchesList(credential);
                        break;
                    //年度數量
                    case "year":
                        json = GaData.ViewYear();
                        break;
                    //今日參觀
                    case "todayOrganicSearches":
                        json = GaData.TodayOrganicSearches(credential);
                        break;
                    //客服信函
                    case "totalContact":
                        json = GaData.TotalContact();
                        break;

                    //總瀏覽次數
                    case "totalPathViews":
                        json = GaData.TotalPathViews(credential);
                        break;
                    //總流量
                    case "totalOrganicSearches":
                        json = GaData.TotalOrganicSearches(credential);
                        break;
                    //熱門頁面
                    case "hotpages":
                        json = GaData.Hotpages(credential);
                        break;
                    //關鍵字
                    case "keyword":
                        json = GaData.Keyword(credential);
                        break;
                    //年齡層
                    case "age":
                        json = GaData.Age(credential);
                        break;
                }
            }
            return json;
        }

        #endregion

        //撈取報告書
        [HttpPost]
        public string GetData(string CustomerName, string StartDate, string EndDate, string Name)
        {
            try
            {
                StartDate = StartDate.Replace('-', '/');
                EndDate = EndDate.Replace('-', '/');
                var result = "";

                ///Model
                ApiViewModel Model = new ApiViewModel();
                ApiService Api = new ApiService();
                QueryCustomeService QueryCustome = new QueryCustomeService();
                AssessTableService AssessTable = new AssessTableService();
                GaugeDataService GaugeData = new GaugeDataService();
                LabDataService LabData = new LabDataService();

                ///Content

                //取得體檢人員名單
                Model.QueryCustome_ListViewModel = Api.GetQueryCustome(CustomerName, StartDate, EndDate, Name);
                if (Model.QueryCustome_ListViewModel != null && Model.QueryCustome_ListViewModel.Count() >= 1)
                {
                    //取得身體評估表
                    Model.AssessTable_ListViewModel = Api.GetAssessTable(Model.QueryCustome_ListViewModel);
                    //取得生理量測資料
                    Model.GaugeData_ListViewModel = Api.GetGaugeData(Model.QueryCustome_ListViewModel);
                    //取得檢驗資料
                    Model.LabData_ListViewModel = Api.GetLabData(Model.QueryCustome_ListViewModel);

                    //將資料新增至資料庫
                    var success = Api.InsertData(Model.QueryCustome_ListViewModel, Model.AssessTable_ListViewModel, Model.GaugeData_ListViewModel, Model.LabData_ListViewModel);

                    ////新增體檢人員名單
                    //var Q = QueryCustome.InsertQueryCustom(Model.QueryCustome_ListViewModel);
                    ////新增身體評估表
                    //var A = AssessTable.InsertAssessTable(Model.AssessTable_ListViewModel);
                    ////新增生理量測資料
                    //var G = GaugeData.InsertGaugeData(Model.GaugeData_ListViewModel);
                    ////新增檢驗資料
                    //var L = LabData.InsertLabData(Model.LabData_ListViewModel);

                    //if (Q && A && G && L)
                    if (success)
                    {
                        result = "Success";
                    }
                    else
                    {
                        result = "Fail";
                    }
                }
                else
                {
                    result = "Error";
                }

                return result;
            }
            catch (Exception ex)
            {
                return "Error";
            }
        }

        public FileStreamResult PrintAll(string barcodelist)
        {
            try
            {
                //Language
                string lang = "tw";

                //Model
                QueryCustomeService queryCustomeService = new QueryCustomeService();

                MemoryStream workStream = new MemoryStream();
                ZipFile zip = new ZipFile(Encoding.Default);

                var barcode = barcodelist.TrimEnd(',').Split(',');
                foreach (var bid in barcode)
                {
                    //string url = "http://localhost:9283/siteadmin/DownloadReport?bid=" + bid;
                    //string url = "http://iis.youweb.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;
                    string url = "http://medirep.mornjoy.com.tw/siteadmin/DownloadReport?bid=" + bid;
                    MemoryStream pdfStream = new MemoryStream();

                    var data = queryCustomeService.GetData_Backend(lang, bid);

                    using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
                    {
                        ObjectConfig oc = new ObjectConfig();

                        oc.SetPrintBackground(true)
                             .SetPageUri(url);

                        byte[] pdf = pechkin.Convert(oc);
                        zip.AddEntry(data.barcode + "_" + data.name + "_" + DateTime.Parse(data.checkdate).ToString("yyyyMMdd") + "_報告書.pdf", pdf);
                    }
                    pdfStream.Close();
                }

                zip.Save(workStream);
                workStream.Position = 0;

                FileStreamResult fileResult = new FileStreamResult(workStream, System.Net.Mime.MediaTypeNames.Application.Zip);
                fileResult.FileDownloadName = "MultiplePDFs.zip";

                return fileResult;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Report(string bid)
        {
            bool isLogin = FunctionService.systemUserCheck();
            if (!isLogin)
            {
                return RedirectPermanent(Url.Content("~/siteadmin/login"));
            }

            //Language
            string lang = "tw";

            //Model
            HomeViewModel homeVM = new HomeViewModel();
            WebDataService webDataService = new WebDataService();
            QueryCustomeService queryCustomeSerice = new QueryCustomeService();
            AssessTableService assessTableService = new AssessTableService();
            DataService dataService = new DataService();

            ///Content

            //專線
            homeVM.WebData_ViewModel = webDataService.GetWebData(lang);

            //身體構造圖
            var SystemOverview = dataService.GetSystemVOerviewData_Backend(lang, bid);
            homeVM.SystemOverview_Left_ViewModel = SystemOverview["Left"];
            homeVM.SystemOverview_Right_ViewModel = SystemOverview["Right"];

            //目錄

            //使用者資訊
            homeVM.QueryCustome_ViewModel = queryCustomeSerice.GetData_Backend(lang, bid);

            //基本資訊
            homeVM.BasicAssessment_ViewModel = assessTableService.GetData_Backend(lang, bid);

            //異常清單
            homeVM.Abnormality_ViewModel = dataService.GetAbnormalityData_Backend(lang, bid);

            //理學檢查
            homeVM.Physical_ViewModel = dataService.GetPhysicalData_Backend(lang, bid);

            //檢驗
            homeVM.Examine_ViewModel = dataService.GetExamineData_BackEnd(lang, bid);

            #region -- 超音波檢查 --

            ViewBag.ImagingData = 0;

            //超音波
            homeVM.Ultrasound_ViewModel = dataService.GetUltrasoundData_Backend(lang, bid);
            if (homeVM.Ultrasound_ViewModel != null && homeVM.Ultrasound_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //數位斷層
            homeVM.Digital_ViewModel = dataService.GetDigitalData_Backend(lang, bid);
            if (homeVM.Digital_ViewModel != null && homeVM.Digital_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //胸部能量
            homeVM.ChestDualEnergy_ViewModel = dataService.GetChestDualEnergyData_Backend(lang, bid);
            if (homeVM.ChestDualEnergy_ViewModel != null && homeVM.ChestDualEnergy_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //一般X光
            homeVM.Xray_ViewModel = dataService.GetXrayData_Backend(lang, bid);
            if (homeVM.Xray_ViewModel != null && homeVM.Xray_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //三連片
            homeVM.Triple_ViewModel = dataService.GetTripleData_Backend(lang, bid);
            if (homeVM.Triple_ViewModel != null && homeVM.Triple_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //內視鏡
            homeVM.Endoscope_ViewModel = dataService.GetEndoscopeData_Backend(lang, bid);
            if (homeVM.Endoscope_ViewModel != null && homeVM.Endoscope_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //身體組成分析
            homeVM.Body_ViewModel = dataService.GetBodyData_Backend(lang, bid);
            if (homeVM.Body_ViewModel != null && homeVM.Body_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //心電圖
            homeVM.Electrocardiogram_ViewModel = dataService.GetElectrocardiogramData_Backend(lang, bid);
            if (homeVM.Electrocardiogram_ViewModel != null && homeVM.Electrocardiogram_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //自律神經檢查
            homeVM.Autonomic_ViewModel = dataService.GetAutonomicData_Backend(lang, bid);
            if (homeVM.Autonomic_ViewModel != null && homeVM.Autonomic_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //眼底攝影
            homeVM.Fundus_ViewModel = dataService.GetFundusData_Backend(lang, bid);
            if (homeVM.Fundus_ViewModel != null && homeVM.Fundus_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            #endregion

            //其他檢查
            homeVM.Other_ViewModel = dataService.GetOtherData_Backend(lang, bid);

            return View(homeVM);
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Print(string bid,string userid,string startdate,string enddate)
        {
            UpdateGAWork work = new UpdateGAWork();
            string filePath = Path.Combine(Server.MapPath("~/fileuploads"));
            //work.startprint(bid, Session["USERID"] as string, filePath);
            if (userid == "all")
            {
                DateTime start = DateTime.Parse(startdate);
                DateTime end = DateTime.Parse(enddate);
                var tempalluser = (from a in DB.query_custome
                                   where a.status == "Y"
                                   select a).ToList();
                foreach (var item in tempalluser)
                {
                    DateTime checkdate = DateTime.Parse(item.checkdate);
                    if (checkdate >= start && checkdate <= end)
                    {
                        work.startprint(item.barcode, item.user_id, filePath, item.guid);

                        var tempdata = (from a in DB.query_custome
                                        where a.barcode == item.barcode && a.user_id == item.user_id
                                        select a).FirstOrDefault();
                        tempdata.downloadUrl = "/fileuploads/" + item.guid + item.barcode + ".pdf";
                        DB.Entry(tempdata).State = EntityState.Modified;
                        DB.SaveChanges();
                    }
                }
            }
            else
            {
                

                var tempdata = (from a in DB.query_custome
                                where a.barcode == bid && a.user_id == userid
                                select a).FirstOrDefault();

                work.startprint(bid, userid as string, filePath,tempdata.guid);

                tempdata.downloadUrl = "/fileuploads/" + tempdata.guid + bid + ".pdf";
                DB.Entry(tempdata).State = EntityState.Modified;
                DB.SaveChanges();
            }


            return RedirectPermanent(Url.Content("~/siteadmin/query_custome/list"));
            //try
            //{
            //    //Language
            //    string lang = "tw";

            //    //Model
            //    QueryCustomeService queryCustomeService = new QueryCustomeService();

            //    //string url = "http://localhost:9283/siteadmin/DownloadReport?bid=" + bid;
            //    //string url = "http://iis.youweb.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;
            //    string url = "http://medirep.mornjoy.com.tw/siteadmin/DownloadReport?bid=" + bid;
            //    //string url = "http://iis.24241872.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;

            //    var decodeUserid = Encoding.UTF8.GetString(Convert.FromBase64String(Session["USERID"] as string));
            //    var judgedata =  queryCustomeService.GetData(lang, bid);

            //    if (judgedata == null)
            //    {
            //        return RedirectToAction("Index", "Login");
            //    }
            //    else
            //    {
            //        string judgeUserid = judgedata.user_id.Trim();

            //        if (judgeUserid != decodeUserid)
            //        {
            //            return RedirectToAction("Index", "Login");
            //        }
            //    }

            //    var data = queryCustomeService.GetData_Backend(lang, bid);

            //    using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
            //    {
            //        ObjectConfig oc = new ObjectConfig();

            //        oc.SetPrintBackground(true)
            //             .SetPageUri(url);

            //        byte[] pdf = pechkin.Convert(oc);
            //        return File(pdf, "application/pdf", data.name + "_報告書.pdf");
            //    }
            //}
            //catch (Exception ex)
            //{
            //    return null;
            //}
        }
        public string PrintName(string bid)
        {
            try
            {
                //Language
                string lang = "tw";

                //Model
                QueryCustomeService queryCustomeService = new QueryCustomeService();

                //string url = "http://localhost:9283/siteadmin/DownloadReport?bid=" + bid;
                //string url = "http://iis.youweb.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;
                string url = "http://medirep.mornjoy.com.tw/siteadmin/DownloadReport?bid=" + bid;
                //string url = "http://iis.24241872.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;

                var data = queryCustomeService.GetData_Backend(lang, bid);

                return data.name + "_報告書.pdf";
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult siteadminPrint(string bid)
        {
            try
            {
                //Language
                string lang = "tw";

                //Model
                QueryCustomeService queryCustomeService = new QueryCustomeService();

                //string url = "http://localhost:9283/siteadmin/DownloadReport?bid=" + bid;
                //string url = "http://iis.youweb.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;
                string url = "http://medirep.mornjoy.com.tw/siteadmin/DownloadReport?bid=" + bid;
                //string url = "http://iis.24241872.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;

                var data = queryCustomeService.GetData_Backend(lang, bid);

                using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
                {
                    ObjectConfig oc = new ObjectConfig();

                    oc.SetPrintBackground(true)
                         .SetPageUri(url);

                    byte[] pdf = pechkin.Convert(oc);
                    return File(pdf, "application/pdf", data.name + "_報告書.pdf");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult DownloadReport(string bid)
        {
            //Language
            string lang = "tw";

            //Model
            HomeViewModel homeVM = new HomeViewModel();
            WebDataService webDataService = new WebDataService();
            QueryCustomeService queryCustomeService = new QueryCustomeService();
            AssessTableService assessTablService = new AssessTableService();
            DataService dataService = new DataService();

            ///Content

            //專線
            homeVM.WebData_ViewModel = webDataService.GetWebData(lang);

            //身體構造圖
            var SystemOverview = dataService.GetSystemVOerviewData_Backend(lang, bid);
            homeVM.SystemOverview_Left_ViewModel = SystemOverview["Left"];
            homeVM.SystemOverview_Right_ViewModel = SystemOverview["Right"];

            //目錄

            //使用者資訊
            homeVM.QueryCustome_ViewModel = queryCustomeService.GetData_Backend(lang, bid);

            //基本資訊
            homeVM.BasicAssessment_ViewModel = assessTablService.GetData_Backend(lang, bid);

            //異常清單
            homeVM.Abnormality_ViewModel = dataService.GetAbnormalityData_Backend(lang, bid);

            //理學檢查
            homeVM.Physical_ViewModel = dataService.GetPhysicalData_Backend(lang, bid);

            //檢驗
            homeVM.Examine_ViewModel = dataService.GetExamineData_BackEnd(lang, bid);

            #region -- 超音波檢查 --

            ViewBag.ImagingData = 0;

            //超音波
            homeVM.Ultrasound_ViewModel = dataService.GetUltrasoundData_Backend(lang, bid);
            if (homeVM.Ultrasound_ViewModel != null && homeVM.Ultrasound_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //數位斷層
            homeVM.Digital_ViewModel = dataService.GetDigitalData_Backend(lang, bid);
            if (homeVM.Digital_ViewModel != null && homeVM.Digital_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //胸部能量
            homeVM.ChestDualEnergy_ViewModel = dataService.GetChestDualEnergyData_Backend(lang, bid);
            if (homeVM.ChestDualEnergy_ViewModel != null && homeVM.ChestDualEnergy_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //一般X光
            homeVM.Xray_ViewModel = dataService.GetXrayData_Backend(lang, bid);
            if (homeVM.Xray_ViewModel != null && homeVM.Xray_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //三連片
            homeVM.Triple_ViewModel = dataService.GetTripleData_Backend(lang, bid);
            if (homeVM.Triple_ViewModel != null && homeVM.Triple_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //內視鏡
            homeVM.Endoscope_ViewModel = dataService.GetEndoscopeData_Backend(lang, bid);
            if (homeVM.Endoscope_ViewModel != null && homeVM.Endoscope_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //身體組成分析
            homeVM.Body_ViewModel = dataService.GetBodyData_Backend(lang, bid);
            if (homeVM.Body_ViewModel != null && homeVM.Body_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //心電圖
            homeVM.Electrocardiogram_ViewModel = dataService.GetElectrocardiogramData_Backend(lang, bid);
            if (homeVM.Electrocardiogram_ViewModel != null && homeVM.Electrocardiogram_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //自律神經檢查
            homeVM.Autonomic_ViewModel = dataService.GetAutonomicData_Backend(lang, bid);
            if (homeVM.Autonomic_ViewModel != null && homeVM.Autonomic_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //眼底攝影
            homeVM.Fundus_ViewModel = dataService.GetFundusData_Backend(lang, bid);
            if (homeVM.Fundus_ViewModel != null && homeVM.Fundus_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            #endregion

            //其他檢查
            homeVM.Other_ViewModel = dataService.GetOtherData_Backend(lang, bid);

            return View(homeVM);
        }
    }
}