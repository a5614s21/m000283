﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Web.Repository;

namespace Web.Controllers
{
    public class BaseController : Controller
    {
        protected override void Initialize(RequestContext requestContext)
        {
            base.Initialize(requestContext);
            ViewBag.MetaTitle = "M000283";

            //Language
            string lang = "tw";

            //Model
            WebDataService webDataService = new WebDataService();
            var WData = webDataService.GetWebData(lang);

            ViewBag.title = WData.title;
            ViewBag.keywords = WData.seo_keywords;
            ViewBag.description = WData.seo_description;
            ViewBag.address = WData.address;
            ViewBag.googlemap = WData.googlemapurl;
            ViewBag.phone = WData.phone;
            ViewBag.ext_num = WData.ext_num;
            ViewBag.medical_servicedate = WData.medical_servicedate;
            ViewBag.outpatient_servicedate = WData.outpatient_servicedate;
        }
    }
}