﻿using PagedList;
using Pechkin;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Web.Repository;
using Web.Service;
using Web.ViewModel;
using System.IO;
using Ionic.Zip;
using System.Text;
using System.Collections;
using System.Web.Caching;

namespace Web.Controllers
{
    public class HomeController : BaseController
    {
        private int pageSize = 10;

        //首頁(列表)
        public ActionResult ReportList(int? page)
        {
            //Language
            string lang = "tw";

            //Model
            HomeViewModel homeVM = new HomeViewModel();
            QueryCustomeService queryCustomeService = new QueryCustomeService();
            DataService dataService = new DataService();


            //檢查是否超過登入時效
            bool isLogin = queryCustomeService.UserLoginCheck(Session["ID"]);
            if (!isLogin)
            {
                return RedirectToAction("Index", "Login");
            }

            //Content
            homeVM.QueryCustome_ListViewModel = queryCustomeService.GetDataByUSERID_BIRTHDAY(Session["USERID"].ToString(), Session["BDAY"].ToString());

            var pageNumber = page ?? 1;
            homeVM.QueryCustome_PagedListViewModel = homeVM.QueryCustome_ListViewModel.ToPagedList(pageNumber, pageSize);
            int Allpage = System.Convert.ToInt16(System.Math.Ceiling((double)homeVM.QueryCustome_ListViewModel.Count / pageSize));
            ViewBag.pageView = FunctionService.getPageNum(Url.Content("~/") + "Home/ReportList/", Allpage, pageNumber, homeVM.QueryCustome_ListViewModel.Count, "");

            return View(homeVM);
        }

        public ActionResult Report(string bid)
        {
            //Language
            string lang = "tw";

            //Model
            HomeViewModel homeVM = new HomeViewModel();
            WebDataService webDataService = new WebDataService();
            QueryCustomeService queryCustomeService = new QueryCustomeService();
            AssessTableService assessTableService = new AssessTableService();
            DataService dataService = new DataService();

            //檢查是否超過登入時效
            bool isLogin = queryCustomeService.UserLoginCheck(Session["ID"]);
            if (!isLogin)
            {
                return RedirectToAction("Index", "Login");
            }

            ///Content

            //專線
            homeVM.WebData_ViewModel = webDataService.GetWebData(lang);

            //身體構造圖
            var SystemOverview = dataService.GetSystemVOerviewData(lang, bid);
            homeVM.SystemOverview_Left_ViewModel = SystemOverview["Left"];
            homeVM.SystemOverview_Right_ViewModel = SystemOverview["Right"];

            //目錄

            //使用者資訊
            homeVM.QueryCustome_ViewModel = queryCustomeService.GetData(lang, bid);

            var decodeUserid = Encoding.UTF8.GetString(Convert.FromBase64String(Session["USERID"] as string));

            if (homeVM.QueryCustome_ViewModel == null)
            {
                string judgeUserid = "";

                if (judgeUserid != decodeUserid)
                {
                    return RedirectToAction("Index", "Login");
                }
            }
            else
            {
                string judgeUserid = homeVM.QueryCustome_ViewModel.user_id;

                if (judgeUserid != decodeUserid)
                {
                    return RedirectToAction("Index", "Login");
                }
            }

            //基本資訊
            homeVM.BasicAssessment_ViewModel = assessTableService.GetData(lang, bid);

            //異常清單
            homeVM.Abnormality_ViewModel = dataService.GetAbnormalityData(lang, bid);

            //理學檢查
            homeVM.Physical_ViewModel = dataService.GetPhysicalData(lang, bid);

            //檢驗
            homeVM.Examine_ViewModel = dataService.GetExamineData(lang, bid);

            #region -- 超音波檢查 --

            ViewBag.ImagingData = 0;

            //超音波
            homeVM.Ultrasound_ViewModel = dataService.GetUltrasoundData(lang, bid);
            if (homeVM.Ultrasound_ViewModel != null && homeVM.Ultrasound_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //數位斷層
            homeVM.Digital_ViewModel = dataService.GetDigitalData(lang, bid);
            if (homeVM.Digital_ViewModel != null && homeVM.Digital_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //胸部能量
            homeVM.ChestDualEnergy_ViewModel = dataService.GetChestDualEnergyData(lang, bid);
            if (homeVM.ChestDualEnergy_ViewModel != null && homeVM.ChestDualEnergy_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //一般X光
            homeVM.Xray_ViewModel = dataService.GetXrayData(lang, bid);
            if (homeVM.Xray_ViewModel != null && homeVM.Xray_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //三連片
            homeVM.Triple_ViewModel = dataService.GetTripleData(lang, bid);
            if (homeVM.Triple_ViewModel != null && homeVM.Triple_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //內視鏡
            homeVM.Endoscope_ViewModel = dataService.GetEndoscopeData(lang, bid);
            if (homeVM.Endoscope_ViewModel != null && homeVM.Endoscope_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //身體組成分析
            homeVM.Body_ViewModel = dataService.GetBodyData(lang, bid);
            if (homeVM.Body_ViewModel != null && homeVM.Body_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //心電圖
            homeVM.Electrocardiogram_ViewModel = dataService.GetElectrocardiogramData(lang, bid);
            if (homeVM.Electrocardiogram_ViewModel != null && homeVM.Electrocardiogram_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //自律神經檢查
            homeVM.Autonomic_ViewModel = dataService.GetAutonomicData(lang, bid);
            if (homeVM.Autonomic_ViewModel != null && homeVM.Autonomic_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //眼底攝影
            homeVM.Fundus_ViewModel = dataService.GetFundusData(lang, bid);
            if (homeVM.Fundus_ViewModel != null && homeVM.Fundus_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            #endregion

            //其他檢查
            homeVM.Other_ViewModel = dataService.GetOtherData(lang, bid);

            return View(homeVM);
        }

        public ActionResult DownloadReport(string bid) 
        {
            string lang = "tw";

            //Model
            HomeViewModel homeVM = new HomeViewModel();
            WebDataService webDataService = new WebDataService();
            QueryCustomeService queryCustomeService = new QueryCustomeService();
            AssessTableService assessTableService = new AssessTableService();
            DataService dataService = new DataService();

            //專線
            homeVM.WebData_ViewModel = webDataService.GetWebData(lang);

            //身體構造圖
            var SystemOverview = dataService.GetSystemVOerviewData(lang, bid);
            homeVM.SystemOverview_Left_ViewModel = SystemOverview["Left"];
            homeVM.SystemOverview_Right_ViewModel = SystemOverview["Right"];

            //目錄

            //使用者資訊
            homeVM.QueryCustome_ViewModel = queryCustomeService.GetData(lang, bid);

            //基本資訊
            homeVM.BasicAssessment_ViewModel = assessTableService.GetData(lang, bid);

            //異常清單
            homeVM.Abnormality_ViewModel = dataService.GetAbnormalityData(lang, bid);

            //理學檢查
            homeVM.Physical_ViewModel = dataService.GetPhysicalData(lang, bid);

            //檢驗
            homeVM.Examine_ViewModel = dataService.GetExamineData(lang, bid);

            #region -- 超音波檢查 --

            ViewBag.ImagingData = 0;

            //超音波
            homeVM.Ultrasound_ViewModel = dataService.GetUltrasoundData(lang, bid);
            if (homeVM.Ultrasound_ViewModel != null && homeVM.Ultrasound_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //數位斷層
            homeVM.Digital_ViewModel = dataService.GetDigitalData(lang, bid);
            if (homeVM.Digital_ViewModel != null && homeVM.Digital_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //胸部能量
            homeVM.ChestDualEnergy_ViewModel = dataService.GetChestDualEnergyData(lang, bid);
            if (homeVM.ChestDualEnergy_ViewModel != null && homeVM.ChestDualEnergy_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //一般X光
            homeVM.Xray_ViewModel = dataService.GetXrayData(lang, bid);
            if (homeVM.Xray_ViewModel != null && homeVM.Xray_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //三連片
            homeVM.Triple_ViewModel = dataService.GetTripleData(lang, bid);
            if (homeVM.Triple_ViewModel != null && homeVM.Triple_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //內視鏡
            homeVM.Endoscope_ViewModel = dataService.GetEndoscopeData(lang, bid);
            if (homeVM.Endoscope_ViewModel != null && homeVM.Endoscope_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //身體組成分析
            homeVM.Body_ViewModel = dataService.GetBodyData(lang, bid);
            if (homeVM.Body_ViewModel != null && homeVM.Body_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //心電圖
            homeVM.Electrocardiogram_ViewModel = dataService.GetElectrocardiogramData(lang, bid);
            if (homeVM.Electrocardiogram_ViewModel != null && homeVM.Electrocardiogram_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //自律神經檢查
            homeVM.Autonomic_ViewModel = dataService.GetAutonomicData(lang, bid);
            if (homeVM.Autonomic_ViewModel != null && homeVM.Autonomic_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            //眼底攝影
            homeVM.Fundus_ViewModel = dataService.GetFundusData(lang, bid);
            if (homeVM.Fundus_ViewModel != null && homeVM.Fundus_ViewModel.Count >= 1)
            {
                ViewBag.ImagingData += 1;
            }

            #endregion

            //其他檢查
            homeVM.Other_ViewModel = dataService.GetOtherData(lang, bid);

            return View("Report",homeVM);
        }

        public ActionResult DownloadPdf(string bid)
        {
            try
            {
                //Language
                string lang = "tw";

                //Model
                QueryCustomeService queryCustomeService = new QueryCustomeService();

                //檢查是否超過登入時效
                bool isLogin = queryCustomeService.UserLoginCheck(Session["ID"]);
                //if (!isLogin)
                //{
                //    return RedirectToAction("Index", "Login");
                //}

                //string url = "http://localhost:9283/home/DownloadReport?bid=" + bid;
                //string url = "http://iis.youweb.tw/projects/public/m000283/test/home/DownloadReport?bid=" + bid;
                string url = Request.Url.AbsoluteUri.Replace("DownloadPdf", "DownloadReport");

                var data = queryCustomeService.GetData(lang, bid);

                using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
                {
                    ObjectConfig oc = new ObjectConfig();

                    oc.SetPrintBackground(true)
                         .SetPageUri(url);

                    byte[] pdf = pechkin.Convert(oc);
                    return File(pdf, "application/pdf", data.name + "_報告書.pdf");
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public ActionResult DownloadZip(string bid) 
        {
            try
            {
                var queryCustomeService = new QueryCustomeService();

                //檢查是否超過登入時效
                bool isLogin = queryCustomeService.UserLoginCheck(Session["ID"]);
                //if (!isLogin)
                //{
                //    return RedirectToAction("Index", "Login");
                //}

                var queryCustome = queryCustomeService.GetData("tw", bid);

                if (string.IsNullOrEmpty(queryCustome.image_zip))
                {
                    return this.HttpNotFound();
                }

                return File(
                    Server.MapPath(Server.UrlDecode(queryCustome.image_zip)), 
                    "application/zip", 
                    $"{queryCustome.name}_{queryCustome.user_id}_{queryCustome.checkdate.Replace("/","")}.zip");
            }
            catch (Exception ex) 
            {
                return null;
            }
        }
    }
}