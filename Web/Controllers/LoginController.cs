﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using System.Web.UI;
using Web.Models;
using Web.Repository;
using Web.Service;
using Web.ViewModel;


namespace Web.Controllers
{
    public class LoginController : BaseController
    {
        //登入頁面
        [OutputCache(Location = OutputCacheLocation.None, NoStore = true)]
        public ActionResult Index()
        {
            ViewBag.year = GetYear();
            ViewBag.month = GetMonth();

            return View();
        }

        //登入頁面 POST
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Index(LoginViewModel loginVM, string verification)
        {
            try
            {
                //檢查驗證碼
                if (verification.Equals(Session["_ValCheckCode"].ToString(), StringComparison.OrdinalIgnoreCase))
                {
                    QueryCustomeService queryCustomeService = new QueryCustomeService();

                    //將年月日組合
                    loginVM.Login_PostViewModel.birthday = loginVM.Login_PostViewModel.year.ToString() + "/" + loginVM.Login_PostViewModel.month.ToString("00") + "/" + loginVM.Login_PostViewModel.day.ToString("00");

                    //判斷登入資訊是否正確
                    var checkData = queryCustomeService.UserLoginCheckData(loginVM.Login_PostViewModel);

                    ///測試用 顯示全部資料
                    //if(Model.Login_PostViewModel.usernumber == "admin")
                    //{
                    //    checkData = true;
                    //}

                    //檢查必填欄位與資料正確
                    if (ModelState.IsValid && checkData == true)
                    {
                        //MD5加密
                        var username = FunctionService.md5(loginVM.Login_PostViewModel.usernumber);

                        HttpContext.Session["ID"] = username;
                        HttpContext.Session["USERID"] = Base64Encode(loginVM.Login_PostViewModel.usernumber);
                        HttpContext.Session["BDAY"] = Base64Encode(loginVM.Login_PostViewModel.birthday);

                        return RedirectToAction("ReportList", "Home");
                    }
                    else
                    {
                        ViewBag.Status = "input_error";
                    }
                }
                else
                {
                    ViewBag.Status = "checkcode_error";
                }

                ViewBag.year = GetYear();
                ViewBag.month = GetMonth();

                ViewBag.selectyear = loginVM.Login_PostViewModel.year.ToString();
                ViewBag.selectmonth = loginVM.Login_PostViewModel.month.ToString();
                return View();
            }
            catch (Exception ex)
            {
                return RedirectToAction("Login");
            }
        }

        //登出
        public ActionResult Logout()
        {
            Session.Clear();

            return RedirectToAction("Index", "Login");
        }

        //首次登入
        public ActionResult Phone()
        {           
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Phone(LoginViewModel Model)
        {
            try
            {
                QueryCustomeService queryCustomeService = new QueryCustomeService();
                var check = queryCustomeService.CheckPhone(Model.Phone_PostViewModel.phone);
                if(check == true)
                {
                    var number = CreateRandomCode();
                    HttpContext.Session["PNUMBER"] = number;
                    HttpContext.Session["PHONE"] = Regex.Replace(Model.Phone_PostViewModel.phone, @"[^\w]", "");


                    //設定可更改的時間 (10分鐘)
                    queryCustomeService.EditSetpwDateTime(Model.Phone_PostViewModel.phone);

                    //發送簡訊
                    SmSend(Model.Phone_PostViewModel.phone, number);

                    Model DB = new Model();

                    try
                    {
                        var phone = Model.Phone_PostViewModel.phone;
                        var t = DB.query_custome.Where(x => x.sogi == phone).FirstOrDefault();

                        var smtpdata = DB.smtp_data.SingleOrDefault();

                        //設定smtp主機
                        string smtpAddress = smtpdata.host;
                        //設定Port
                        int portNumber = Int32.Parse(smtpdata.port);
                        bool enableSSL = true;
                        //SSL是否啟用
                        enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                        //填入寄送方email和password
                        string emailFrom = smtpdata.username;
                        string password = smtpdata.password;
                        //收信方email 可以用逗號區分多個收件人
                        string emailTo = t.email;
                        string subject = "【晨悅】驗證碼";

                        string subjectTitle = "";
                        string metaFile = "";
                        subjectTitle = "";
                        metaFile = "Contact_tw.html";

                        string sMessage = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile);
                        sMessage = sMessage.Replace("{$web_title}", "晨悅");
                        sMessage = sMessage.Replace("{$web_url}", "http://" + Request.ServerVariables["Server_Name"]);
                        sMessage = sMessage.Replace("{$use_title}", subjectTitle);

                        sMessage = sMessage.Replace("{$name}", number);

                        using (MailMessage mail = new MailMessage())
                        {
                            mail.From = new MailAddress(emailFrom);
                            mail.To.Add(emailTo);
                            mail.Subject = subject;
                            mail.Body = sMessage;
                            // 若你的內容是HTML格式，則為True
                            mail.IsBodyHtml = true;

                            using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                            {
                                smtp.Credentials = new NetworkCredential(emailFrom, password);
                                smtp.EnableSsl = enableSSL;
                                smtp.Send(mail);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var error = ex;
                    }

                    return RedirectToAction("Verification");
                }
                else
                {
                    var check2 = queryCustomeService.CheckNoPhone(Model.Phone_PostViewModel.phone);
                    if (check2 == true)
                    {
                        TempData["phonealert"] = "已有登入過!";
                    }
                    else
                    {
                        TempData["phonealert"] = "報告書未完成!";
                    }
                }
                
                return RedirectToAction("Index", "Login");
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult Verification()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Verification(LoginViewModel loginVM)
        {
            try
            {
                var s = Session["PNUMBER"].ToString();
                if(loginVM.Verification_PostViewModel.code.ToString() == s)
                {
                    var phone = Session["PHONE"].ToString();
                    QueryCustomeService queryCustomeService = new QueryCustomeService();
                    var check = queryCustomeService.CheckPhone(phone);
                    if(check == true)
                    {
                        queryCustomeService.SetFirst(phone);
                        return RedirectToAction("password");
                    }
                }
                else
                {
                    ViewBag.Error = "code_error";
                }

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Error = "error";
                return View();
            }
        }

        public ActionResult Password()
        {
            try
            {
                var phone = Session["PHONE"].ToString();
                if(!string.IsNullOrEmpty(phone))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Password(LoginViewModel loginVM)
        {
            try
            {
                var phone = Session["PHONE"].ToString();
                if(!string.IsNullOrEmpty(phone))
                {
                    if (loginVM.Password_PostViewModel.password == loginVM.Password_PostViewModel.password_confirm)
                    {
                        QueryCustomeService queryCustomeService = new QueryCustomeService();
                        var check = queryCustomeService.CheckPhone(phone);
                        if (check == true)
                        {
                            var status = queryCustomeService.SetPW(phone, loginVM.Password_PostViewModel);
                            if (status == true)
                            {
                                ViewBag.Status = "success";
                            }
                            else
                            {
                                ViewBag.Status = "error";
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Status = "input_error";
                    }
                }
                else
                {
                    ViewBag.Status = "session_error";
                }

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Status = "error";
                return View();
            }
        }

        public ActionResult Forget()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Forget(LoginViewModel Model)
        {
            try
            {
                QueryCustomeService queryCustomeService = new QueryCustomeService();
                var check = queryCustomeService.CheckPhoneByForget(Model.Forget_PostViewModel.phone);
                if(check == true)
                {
                    var number = CreateRandomCode();
                    HttpContext.Session["PNUMBER"] = number;
                    HttpContext.Session["PHONE"] = Regex.Replace(Model.Forget_PostViewModel.phone, @"[^\w]", "");

                    //設定可更改時間(1小時)
                    queryCustomeService.EditSetpwDateTimeByForget(Model.Forget_PostViewModel.phone);

                    //發送簡訊
                    SmSend(Model.Forget_PostViewModel.phone, number);

                    Model DB = new Model();

                    try
                    {
                        var phone = Model.Forget_PostViewModel.phone;
                        var t = DB.query_custome.Where(x => x.sogi == phone).FirstOrDefault();

                        var smtpdata = DB.smtp_data.SingleOrDefault();

                        //設定smtp主機
                        string smtpAddress = smtpdata.host;
                        //設定Port
                        int portNumber = Int32.Parse(smtpdata.port);
                        bool enableSSL = true;
                        //SSL是否啟用
                        enableSSL = (smtpdata.smtp_auth == "Y" ? true : false);
                        //填入寄送方email和password
                        string emailFrom = smtpdata.username;
                        string password = smtpdata.password;
                        //收信方email 可以用逗號區分多個收件人
                        string emailTo = t.email;
                        string subject = "【晨悅】驗證碼";

                        string subjectTitle = "";
                        string metaFile = "";
                        subjectTitle = "";
                        metaFile = "Contact_tw.html";

                        string sMessage = System.IO.File.ReadAllText(Server.MapPath("~/Content/Mail") + "\\" + metaFile);
                        sMessage = sMessage.Replace("{$web_title}", "晨悅");
                        sMessage = sMessage.Replace("{$web_url}", "http://" + Request.ServerVariables["Server_Name"]);
                        sMessage = sMessage.Replace("{$use_title}", subjectTitle);

                        sMessage = sMessage.Replace("{$name}", number);

                        using (MailMessage mail = new MailMessage())
                        {
                            mail.From = new MailAddress(emailFrom);
                            mail.To.Add(emailTo);
                            mail.Subject = subject;
                            mail.Body = sMessage;
                            // 若你的內容是HTML格式，則為True
                            mail.IsBodyHtml = true;

                            using (SmtpClient smtp = new SmtpClient(smtpAddress, portNumber))
                            {
                                smtp.Credentials = new NetworkCredential(emailFrom, password);
                                smtp.EnableSsl = enableSSL;
                                smtp.Send(mail);
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var error = ex;
                    }

                    return RedirectToAction("Forget_Verification");
                }
                return View();
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        public ActionResult Forget_Verification()
        {
            return View();
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Forget_Verification(LoginViewModel Model)
        {
            try
            {
                var s = Session["PNUMBER"].ToString();
                if(Model.ForgetVerification_PostViewModel.code.ToString() == s)
                {
                    var phone = Session["PHONE"].ToString();
                    QueryCustomeService queryCustomeService = new QueryCustomeService();
                    var check = queryCustomeService.CheckPhoneByForget(phone);
                    if(check == true)
                    {
                        return RedirectToAction("Forget_Password");
                    }
                }
                else
                {
                    ViewBag.Error = "code_error";
                }

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Error = "error";
                return View();
            }
        }

        public ActionResult Forget_Password()
        {
            try
            {
                var phone = Session["PHONE"].ToString();
                if(!string.IsNullOrEmpty(phone))
                {
                    return View();
                }
                else
                {
                    return RedirectToAction("Login");
                }
            }
            catch(Exception ex)
            {
                return View();
            }
        }

        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Forget_Password(LoginViewModel loginVM)
        {
            try
            {
                var phone = Session["PHONE"].ToString();
                if(!string.IsNullOrEmpty(phone))
                {
                    if(loginVM.Password_PostViewModel.password == loginVM.Password_PostViewModel.password_confirm)
                    {
                        QueryCustomeService queryCustomeService = new QueryCustomeService();
                        var check = queryCustomeService.CheckPhoneByForget(phone);
                        if(check == true)
                        {
                            var status = queryCustomeService.SetPWByForget(phone, loginVM.Password_PostViewModel);
                            if(status == true)
                            {
                                ViewBag.Status = "success";
                            }
                            else
                            {
                                ViewBag.Status = "error";
                            }
                        }
                    }
                    else
                    {
                        ViewBag.Status = "input_error";
                    }
                }
                else
                {
                    ViewBag.Status = "session_error";
                }

                return View();
            }
            catch(Exception ex)
            {
                ViewBag.Status = "error";
                return View();
            }
        }

        //取得年份 (100年)
        public List<SelectListItem> GetYear()
        {
            List<SelectListItem> result = new List<SelectListItem>();

            for (int i = 0; i < 100; i++)
            {
                var year = DateTime.Now.Year - i;
                result.Add(new SelectListItem() { Text = year.ToString(), Value = year.ToString() });
            }
            return result;
        }

        //取得月份 (12個月)
        public List<SelectListItem> GetMonth()
        {
            List<SelectListItem> result = new List<SelectListItem>();
            for (int i = 1; i <= 12; i++)
            {
                result.Add(new SelectListItem() { Text = i.ToString(), Value = i.ToString() });
            }
            return result;
        }

        //依照月份取得當月日期
        public ActionResult GetDay(string year, string month)
        {
            try
            {
                List<SelectListItem> selectitemlist = new List<SelectListItem>();
                var LastDay = DateTime.DaysInMonth(int.Parse(year), int.Parse(month)).ToString();

                for (var d = 1; d <= int.Parse(LastDay); d++)
                {
                    selectitemlist.Add(new SelectListItem() { Text = d.ToString(), Value = d.ToString() });
                }

                return Json(new { list = selectitemlist }, JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                var error = ex;
                return Json(new { list = "" }, JsonRequestBehavior.AllowGet);
            }
            
        }

        //加密
        public static string Base64Encode(string AStr)
        {
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(AStr));
        }

        //解密
        public static string Base64Decode(string ABase64)
        {
            return Encoding.UTF8.GetString(Convert.FromBase64String(ABase64));
        }

        public static bool SmSend(string phone, string pnumber)
        {
            StringBuilder reqUrl = new StringBuilder();
            //reqUrl.Append("https://{三竹網域名稱}/b2c/mtk/SmSend?&CharsetURL=UTF-8");
            reqUrl.Append("https://smsapi.mitake.com.tw/api/mtk/SmSend?&CharsetURL=UTF-8");
            //reqUrl.Append("https://smsb2c.mitake.com.tw/b2c/mtk/SmSend?&CharsetURL=UTF-8");

            StringBuilder p = new StringBuilder();
            p.Append("username=82533341");
            p.Append("&password=a2255b2255");
            p.Append("&dstaddr=" + phone);
            p.Append("&smbody=您的手機驗證碼為[" + pnumber + "]。晨悅貼心提醒，請您於10分鐘內進行設定密碼。");

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(new Uri(reqUrl.ToString()));
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            byte[] bs = Encoding.UTF8.GetBytes(p.ToString());
            request.ContentLength = bs.Length;
            request.GetRequestStream().Write(bs, 0, bs.Length);

            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader sr = new StreamReader(response.GetResponseStream());
            string result = sr.ReadToEnd();


            return true;
        }

        public static string CreateRandomCode()
        {
            string allChar = "0,1,2,3,4,5,6,7,8,9";
            string[] allCharArray = allChar.Split(',');
            string randomCode = "";
            int temp = -1;

            Random rand = new Random();
            for(int i = 0; i < 6; i++)
            {
                if(temp != -1)
                {
                    rand = new Random(i * temp * ((int)DateTime.Now.Ticks));
                }
                int t = rand.Next(10);
                if(temp != -1 && temp == t)
                {
                    return CreateRandomCode();
                }
                temp = t;
                randomCode += allCharArray[t];
            }

            return randomCode;
        }
    }
}