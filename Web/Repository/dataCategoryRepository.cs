﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.Repository
{
    public class dataCategoryRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        public static Dictionary<String, Object> colFrom()
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("itemNo", "[{'subject': '項目代碼','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': ''}]");
            main.Add("itemName", "[{'subject': '項目名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': ''}]");
            main.Add("itemSystem", "[{'subject': '所屬系統','type': 'text','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':''}]");
            main.Add("itemCategory", "[{'subject': '所屬分類','type': 'text','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':''}]");
            main.Add("orderindex", "[{'subject': '排序','type': 'text','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':''}]");
            //main.Add("lang", "[{'subject': '語系','type': 'text','default': '','class': 'col-lg-10','required': 'required','notes': '','inherit':''}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();

            //※檔案總類還有：圖片(image/gif,image/jpeg,image/png)，MP4：(video/mp4)

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'Y','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("lang", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            return fromData;
        }

        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("itemNo", "項目代碼");
            re.Add("itemName", "項目名稱");
            re.Add("itemSystem", "所屬系統");
            re.Add("itemCategory", "所屬類別");
            re.Add("orderindex", "排序");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "orderindex");
            re.Add("orderByType", "asc");

            return re;
        }
    }
}