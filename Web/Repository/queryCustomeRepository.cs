﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.Repository
{
    public class queryCustomeRepository
    {
        /// <summary>
        /// 是否使用多語系
        /// </summary>
        /// <returns></returns>
        public static string useLang()
        {
            return "N";
        }

        /// <summary>
        /// 欄位設定
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> colFrom(Dictionary<string, List<string>> data)
        {
            Dictionary<String, Object> fromData = new Dictionary<string, object>();

            #region 主要設定

            Dictionary<String, Object> main = new Dictionary<string, object>();
            main.Add("barcode", "[{'subject': '條碼號碼','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("checkdate", "[{'subject': '體檢日','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("chart", "[{'subject': '病歷號','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("customername", "[{'subject': '公司名稱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("name", "[{'subject': '姓名','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("user_id", "[{'subject': '身分證號碼','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("sex", "[{'subject': '性別','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("birthday", "[{'subject': '出生年月日','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("age", "[{'subject': '年齡','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("sogi", "[{'subject': '行動電話','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");
            main.Add("email", "[{'subject': '電子信箱','type': 'text','defaultVal': '','classVal': 'col-lg-10','required': '','readonly':'readonly','notes': '','useLang':'N'}]");

            #endregion

            #region 多媒體

            Dictionary<String, Object> media = new Dictionary<string, object>();
            //※檔案總類還有：圖片(image/gif, image/jpeg, image/png)，MP4：(video/mp4)

            //超音波檢查
            Dictionary<String, Object> Ultrasound = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Ultrasound"].Count() != 0)
                {
                    foreach (var item in data["Ultrasound"])
                    {
                        switch (item)
                        {
                            case "7010":
                                Ultrasound.Add("pic_7010", "[{'subject': '腹部超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7011":
                                Ultrasound.Add("pic_7011", "[{'subject': '攝護腺超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目3*1 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7012":
                                Ultrasound.Add("pic_7012", "[{'subject': '乳房超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7013":
                                Ultrasound.Add("pic_7013", "[{'subject': '骨盆腔超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目2*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7015":
                                Ultrasound.Add("pic_7015", "[{'subject': '頸動脈超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7014":
                                Ultrasound.Add("pic_7014", "[{'subject': '甲狀腺超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目3*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7016":
                                Ultrasound.Add("pic_7016", "[{'subject': '心臟超音波','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //數位斷層合成掃描
            Dictionary<String, Object> DigitalEnergy = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["DigitalEnergy"].Count() != 0)
                {
                    foreach (var item in data["DigitalEnergy"])
                    {
                        switch (item)
                        {
                            case "7050":
                                DigitalEnergy.Add("pic_7050", "[{'subject': '頭部數位斷層合成攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                            case "7051":
                                DigitalEnergy.Add("pic_7051", "[{'subject': '頸部數位斷層合成攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                            case "7052":
                                DigitalEnergy.Add("pic_7052", "[{'subject': '胸部數位斷層合成攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                            case "7053":
                                DigitalEnergy.Add("pic_7053", "[{'subject': '腹部數位斷層合成攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //胸部能量檢影
            Dictionary<String, Object> ChestDualEnergy = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["ChestDualEnergy"].Count() != 0)
                {
                    foreach (var item in data["ChestDualEnergy"])
                    {
                        switch (item)
                        {
                            case "7049":
                                ChestDualEnergy.Add("pic_7049", "[{'subject': '胸部數位雙能量減影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //一般X光
            Dictionary<String, Object> Xray = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Xray"].Count() != 0)
                {
                    foreach (var item in data["Xray"])
                    {
                        switch (item)
                        {
                            case "7033":
                                Xray.Add("pic_7033", "[{'subject': '胸部CXR','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7034":
                                Xray.Add("pic_7034", "[{'subject': '腹部KUB','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7035":
                                Xray.Add("pic_7035", "[{'subject': '頸椎X光-正面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7036":
                                Xray.Add("pic_7036", "[{'subject': '頸椎X光-側面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7037":
                                Xray.Add("pic_7037", "[{'subject': '胸椎X光-側面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7038":
                                Xray.Add("pic_7038", "[{'subject': '胸椎X光-正面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7039":
                                Xray.Add("pic_7039", "[{'subject': '腰椎X光-正面(L-spine AP)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7040":
                                Xray.Add("pic_7040", "[{'subject': '腰椎X光-側面(L-spine Lat)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7061":
                                Xray.Add("pic_7061", "[{'subject': '左手X光','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7093":
                                Xray.Add("pic_7093", "[{'subject': '胸部CXR-側面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7062":
                                Xray.Add("pic_7062", "[{'subject': '右手X光','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8009":
                                Xray.Add("pic_8009", "[{'subject': '小腿X光(正面)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8010":
                                Xray.Add("pic_8010", "[{'subject': '小腿X光(左腳)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8011":
                                Xray.Add("pic_8011", "[{'subject': '小腿X光(右腳)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8012":
                                Xray.Add("pic_8012", "[{'subject': '膝蓋X光-正面','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8013":
                                Xray.Add("pic_8013", "[{'subject': '膝蓋X光-側面(左)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "8014":
                                Xray.Add("pic_8014", "[{'subject': '膝蓋X光-側面(右)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                        }
                    }
                }
            }

            //三連片
            Dictionary<String, Object> Triple = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Triple"].Count() != 0)
                {
                    foreach (var item in data["Triple"])
                    {
                        switch (item)
                        {
                            case "7054":
                                Triple.Add("pic_7054", "[{'subject': '全脊椎數位X光攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7055":
                                Triple.Add("pic_7055", "[{'subject': '下肢數位X光攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //內視鏡
            Dictionary<String, Object> Endoscope = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Endoscope"].Count() != 0)
                {
                    foreach (var item in data["Endoscope"])
                    {
                        switch (item)
                        {
                            case "7045":
                                Endoscope.Add("pic_7045", "[{'subject': '上消化道內視鏡','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7046":
                                Endoscope.Add("pic_7046", "[{'subject': '下消化道內視鏡','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7029":
                                Endoscope.Add("pic_7029", "[{'subject': '無痛大腸鏡檢查','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7030":
                                Endoscope.Add("pic_7030", "[{'subject': '無痛胃鏡檢查','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7031":
                                Endoscope.Add("pic_7031", "[{'subject': '胃鏡檢查','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7020":
                                Endoscope.Add("pic_7070", "[{'subject': '胃鏡病理報告','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7032":
                                Endoscope.Add("pic_7032", "[{'subject': '大腸鏡檢查','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7063":
                                Endoscope.Add("pic_7063", "[{'subject': '乙狀結腸','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7076":
                                Endoscope.Add("pic_7076", "[{'subject': '大腸鏡病理報告','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //身體組成分析
            Dictionary<String, Object> Body = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Body"].Count() != 0)
                {
                    foreach (var item in data["Body"])
                    {
                        switch (item)
                        {
                            case "7021":
                                Body.Add("pic_7021", "[{'subject': '雙能量X光骨質吸收儀(DXA)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7075":
                                Body.Add("pic_7075", "[{'subject': '雙能量X光骨質吸收儀(DXA)-髖關節','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7091":
                                Body.Add("pic_7091", "[{'subject': '雙能量X光骨質吸收儀(DXA)-腰椎','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7020":
                                Body.Add("pic_7020", "[{'subject': '全身體脂肪分析','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7060":
                                Body.Add("pic_7060", "[{'subject': 'InBody體脂肪機','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //心電圖
            Dictionary<String, Object> Electrocardiogram = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Electrocardiogram"].Count() != 0)
                {
                    foreach (var item in data["Electrocardiogram"])
                    {
                        switch (item)
                        {
                            case "7008":
                                Electrocardiogram.Add("pic_7008", "[{'subject': '靜式心電圖EKG','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7007":
                                Electrocardiogram.Add("pic_7007", "[{'subject': '運動心電圖EKG','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //自律神經檢查
            Dictionary<String, Object> Autonomic = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Autonomic"].Count() != 0)
                {
                    foreach (var item in data["Autonomic"])
                    {
                        switch (item)
                        {
                            case "7042":
                                Autonomic.Add("pic_7042", "[{'subject': '自律神經檢測(HRV)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;

                            case "7041":
                                Autonomic.Add("pic_7041", "[{'subject': '心腦血管彈性分析(APG)','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            //眼底攝影
            Dictionary<String, Object> Fundus = new Dictionary<string, object>();
            if (data != null && data.Count() != 0)
            {
                if (data["Fundus"].Count() != 0)
                {
                    foreach (var item in data["Fundus"])
                    {
                        switch (item)
                        {
                            case "7023":
                                Fundus.Add("pic_7023", "[{'subject': '眼底攝影','type': 'fileUploadByJSON','defaultVal': '','classVal': 'col-lg-10','required': '','notes': '<small class=\"form-text text-muted offset-md-2 col-sm-9\">影像數目4*2 檔案類型:jpg、png;</small>','filetype': 'image/gif,image/jpeg,image/png','multiple': 'Y','useLang':'N'}]");
                                break;
                        }
                    }
                }
            }

            #endregion

            #region 進階

            Dictionary<String, Object> other = new Dictionary<string, object>();

            other.Add("status", "[{'subject': '啟用狀態','type': 'radio','defaultVal': 'N','classVal': 'col-lg-10','required': '','notes': '','data':'啟用/停用','Val':'Y/N','useLang':'N'}]");

            #endregion

            #region 隱藏欄位

            Dictionary<String, Object> hidden = new Dictionary<string, object>();
            hidden.Add("title", "[{'subject': 'title','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            hidden.Add("no", "[{'subject': 'no','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");
            hidden.Add("lang", "[{'subject': 'lang','type': 'hidden','defaultVal': '','classVal': 'col-lg-10','required': 'required','readonly':'','notes': '','useLang':'N'}]");

            #endregion

            fromData.Add("main", main);
            fromData.Add("media", media);
            fromData.Add("other", other);
            fromData.Add("hidden", hidden);

            fromData.Add("ultrasound", Ultrasound);
            fromData.Add("digitaldualenergy", DigitalEnergy);
            fromData.Add("chestdualenergy", ChestDualEnergy);
            fromData.Add("xray", Xray);
            fromData.Add("triple", Triple);
            fromData.Add("endoscope", Endoscope);
            fromData.Add("body", Body);
            fromData.Add("electrocardiogram", Electrocardiogram);
            fromData.Add("autonomic", Autonomic);
            fromData.Add("fundus", Fundus);

            return fromData;
        }

        /// <summary>
        /// 顯示列表
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> dataTableTitle()
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();

            re.Add("guid", "Y");
            re.Add("name", "姓名");
            re.Add("customername", "公司名稱");
            re.Add("sex", "性別");
            re.Add("user_id", "身分證號碼");
            re.Add("checkdate", "體檢日");
            re.Add("modifydate", "異動日期");
            //re.Add("downloadUrl", "檔案路徑");
            re.Add("status", "狀態");
            re.Add("action", "動作");

            return re;
        }

        /// <summary>
        /// 預設排序
        /// </summary>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy()
        {
            Dictionary<String, Object> re = new Dictionary<String, Object>();
            re.Add("orderByKey", "modifydate");
            re.Add("orderByType", "desc");

            return re;
        }
    }
}