﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Controllers;
using Web.Models;
using Web.ViewModel;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Text.RegularExpressions;
using Web.Service;

namespace Web.Repository
{
    public class QueryCustomeService
    {
        private MornjoyContext _context = new MornjoyContext();

        //檢查是否超過登入時間
        public bool UserLoginCheck(object id)
        {
            bool re = false;
            if (id != null)
            {
                re = true;
            }

            return re;
        }

        //取體檢人員名單 By UserID Birthday
        public List<query_custome> GetDataByUSERID_BIRTHDAY(string USERID, string BDAY)
        {
            var userid = LoginController.Base64Decode(USERID);
            var bday = LoginController.Base64Decode(BDAY);

            List<query_custome> result = new List<query_custome>();
            if (userid == "admin")
            {
                result = _context.query_custome.Where(x => x.status == "Y" && x.lang == "tw").ToList();
            }
            else
            {
                result = _context.query_custome.Where(x => x.status == "Y" && x.lang == "tw" && x.user_id == userid && x.birthday == bday).ToList();
            }

            result = result.OrderByDescending(x => x.checkdate).ToList();
            return result;
        }

        //新增體檢人員名單 By API_Data
        public bool InsertQueryCustom(List<QueryCustome> Model)
        {
            try
            {
                var error = 0;
                foreach (var item in Model)
                {
                    //檢查是否重複新增 (檢查Barcode 身分證號碼)
                    var check = _context.query_custome.Where(x => x.status != "D" && x.barcode == item.Barcode && x.user_id == item.ID).Any();
                    if (check == false)
                    {
                        query_custome q = new query_custome();
                        q.guid = Guid.NewGuid().ToString();
                        q.title = item.Name;
                        q.barcode = item.Barcode;
                        q.customername = item.CustomerName;
                        q.user_id = item.ID;
                        q.name = item.Name;
                        q.sex = item.Sex;
                        q.birthday = item.Birthday;
                        q.checkdate = item.CheckDate;
                        q.no = item.No;
                        q.age = item.Age;
                        q.chart = item.Chart;
                        q.sogi = item.Sogi;
                        q.email = item.EMail;
                        q.status = "N";
                        q.modifydate = DateTime.Now;
                        q.create_date = DateTime.Now;
                        q.lang = "tw";
                        _context.Entry(q).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                if (error > 0)
                {
                    return false;
                }
                else
                {
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //檢查登入資料是否正確 By PostViewModel
        public bool UserLoginCheckData(Login_PostViewModel Model)
        {
            try
            {
                Model.password = FunctionService.md5(Model.password);
                var result = _context.query_custome.Where(x => x.status != "D" && x.user_id == Model.usernumber && x.birthday == Model.birthday && x.password == Model.password).Any();
                if (result)
                {
                    return true;
                }

                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //取體檢人員資訊 By Barcode
        public query_custome GetData(string lang, string barcode)
        {
            var result = _context.query_custome.Where(x => x.lang == lang && x.status == "Y" && x.barcode == barcode).SingleOrDefault();
            return result;
        }

        //取體檢人員資訊 By Barcode(後台顯示)
        public query_custome GetData_Backend(string lang, string barcode)
        {
            var result = _context.query_custome.Where(x => x.lang == lang && x.status != "D" && x.barcode == barcode).SingleOrDefault();
            return result;
        }

        //取體檢人員資訊 By Barcode
        public query_custome GetDataBackend(string lang, string barcode)
        {
            var result = _context.query_custome.Where(x => x.lang == lang && x.status != "D" && x.barcode == barcode).SingleOrDefault();
            return result;
        }

        public bool CheckPhone(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhonedata = _context.query_custome.Where(x => x.status != "D" && string.IsNullOrEmpty(x.password)).ToList();

                foreach (var item in AllPhonedata)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        return true;
                    }
                }


                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool CheckNoPhone(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhonedata = _context.query_custome.Where(x => x.status != "D" && !(string.IsNullOrEmpty(x.password))).ToList();

                foreach (var item in AllPhonedata)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if (data == phone)
                    {
                        return true;
                    }
                }


                return false;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool CheckPhoneByForget(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");
                var time = DateTime.Now;
                var AllPhonedata = _context.query_custome.Where(x => x.status != "D" && !string.IsNullOrEmpty(x.password) && x.first == "Y").ToList();
                foreach(var item in AllPhonedata)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        return true;
                    }
                }

                return false;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public void EditSetpwDateTime(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhoneData = _context.query_custome.Where(x => x.status != "D" && string.IsNullOrEmpty(x.password)).ToList();
                foreach(var item in AllPhoneData)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        item.setpw_datetime = DateTime.Now.AddMinutes(10);
                        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                _context.SaveChanges();
            }
            catch(Exception ex)
            {
               
            }
        }

        public void EditSetpwDateTimeByForget(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhoneData = _context.query_custome.Where(x => x.status != "D" && !string.IsNullOrEmpty(x.password) && x.first == "Y").ToList();
                foreach(var item in AllPhoneData)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        item.setpw_datetime = DateTime.Now.AddHours(1);
                        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                _context.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        public void SetFirst(string phone)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhoneData = _context.query_custome.Where(x => x.status != "D" && string.IsNullOrEmpty(x.password)).ToList();
                foreach(var item in AllPhoneData)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        item.setpw_datetime = null;
                        item.first = "Y";

                        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                _context.SaveChanges();
            }
            catch(Exception ex)
            {

            }
        }

        public bool SetPW(string phone, Password_PostViewModel Model)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhoneData = _context.query_custome.Where(x => x.status != "D" && x.first == "Y" && string.IsNullOrEmpty(x.password)).ToList();
                foreach(var item in AllPhoneData)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        item.password = FunctionService.md5(Model.password);
                        item.setpw_datetime = null;

                        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                _context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool SetPWByForget(string phone, Password_PostViewModel Model)
        {
            try
            {
                phone = Regex.Replace(phone, @"[^\w]", "");

                var AllPhoneData = _context.query_custome.Where(x => x.status != "D" && x.first == "Y" && !string.IsNullOrEmpty(x.password)).ToList();
                foreach(var item in AllPhoneData)
                {
                    var data = Regex.Replace(item.sogi, @"[^\w]", "");
                    if(data == phone)
                    {
                        item.password = FunctionService.md5(Model.password);

                        _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                    }
                }
                _context.SaveChanges();

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }

    public class AssessTableService
    {
        private MornjoyContext _context = new MornjoyContext();

        //新增身體評估表 By API_Data
        public bool InsertAssessTable(List<AssessTable> Model)
        {
            try
            {
                var error = 0;
                foreach (var item in Model)
                {
                    //檢查是否重複新增 (檢查Barcode 身分證號碼)
                    var check = _context.assess_table.Where(x => x.status != "D" && x.barcode == item.Barcode && x.user_id == item.ID).Any();
                    if (check == false)
                    {
                        assess_table a = new assess_table();
                        a.guid = Guid.NewGuid().ToString();
                        a.barcode = item.Barcode;
                        a.user_id = item.ID;
                        a.name = item.Name;
                        a.sex = item.Sex;
                        a.birthday = item.Birthday;
                        a.checkdate = item.CheckDate;
                        a.no = item.No;
                        a.age = item.Age;
                        a.chart = item.Chart;
                        a.sogi = item.Sogi;
                        a.email = item.Email;
                        a.chiefcomplaint = item.ChiefComplaint;
                        a.pasthistory = item.PastHistory;
                        a.familyhistory = item.FamilyHistory;
                        a.allergyhistory = item.AllergyHistory;
                        a.vaccinationhistory = item.VaccinationHistory;
                        a.livinghabit = item.LivingHabit;
                        a.status = "Y";
                        a.modifydate = DateTime.Now;
                        a.create_date = DateTime.Now;
                        a.lang = "tw";
                        _context.Entry(a).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                if (error > 0)
                {
                    return false;
                }
                else
                {
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //修改身體評估表狀態 By Barcode Status
        public void ChangeStatusByBarcode(string guid, string Barcode, string status)
        {
            try
            {
                var Data = _context.assess_table.Where(x => x.barcode == Barcode && x.query_guid == guid).ToList();
                foreach (var item in Data)
                {
                    item.status = status;
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        //刪除身體評估表資料 By QueryCustome_Guid
        public void RemoveDataByQueryCustomeGuid(string guid)
        {
            try
            {
                var Q = _context.query_custome.Where(x => x.guid == guid && x.status == "D").SingleOrDefault();

                var Data = _context.assess_table.Where(x => x.barcode == Q.barcode && x.status == "D").ToList();
                foreach (var item in Data)
                {
                    _context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        //取得身體評估表 By Barcode
        public assess_table GetData(string lang, string barcode)
        {
            var result = _context.assess_table.Where(x => x.lang == lang && x.status == "Y" && x.barcode == barcode).SingleOrDefault();
            return result;
        }

        //取得身體評估表 By Barcode(後台顯示)
        public assess_table GetData_Backend(string lang, string barcode)
        {
            var result = _context.assess_table.Where(x => x.lang == lang && x.status != "D" && x.barcode == barcode).SingleOrDefault();
            return result;
        }
    }

    public class GaugeDataService
    {
        private MornjoyContext _context = new MornjoyContext();

        //新增生理量測資料 By API_Data
        public bool InsertGaugeData(List<GaugeData> Model)
        {
            try
            {
                var error = 0;
                foreach (var item in Model)
                {
                    //檢查是否重複新增 (檢查Barcode 體檢日)
                    var check = _context.gauge_data.Where(x => x.status != "D" && x.barcode == item.Barcode && x.checkdate == item.CheckDate).Any();
                    if (check == false)
                    {
                        List<GaugeDataitem> result = new List<GaugeDataitem>();

                        //處理項目
                        foreach (var Dataitem in item.Item)
                        {
                            //取得對應類別
                            var category = _context.data_category.Where(x => x.status == "Y" && x.itemNo == Dataitem.ItemNo).SingleOrDefault();
                            if (category != null)
                            {
                                Dataitem.system_name = category.itemSystem;
                                Dataitem.category_name = category.itemCategory;

                                result.Add(Dataitem);
                            }
                        }

                        //將資料至轉換成JSON
                        var jsondata = JsonConvert.SerializeObject(result);

                        //寫入資料庫
                        gauge_data g = new gauge_data();
                        g.guid = Guid.NewGuid().ToString();
                        g.barcode = item.Barcode;
                        g.old_barcode = item.Old_Barcode;
                        g.checkdate = item.CheckDate;
                        g.content = jsondata;
                        g.status = "Y";
                        g.modifydate = DateTime.Now;
                        g.create_date = DateTime.Now;
                        g.lang = "tw";
                        _context.Entry(g).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                if (error > 0)
                {
                    return false;
                }
                else
                {
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //修改生理量測資料狀態 By Barcode Status
        public void ChangeStatusByBarcode(string guid, string Barcode, string status)
        {
            try
            {
                var Data = _context.gauge_data.Where(x => x.barcode == Barcode && x.query_guid == guid).ToList();
                foreach (var item in Data)
                {
                    item.status = status;
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        //刪除生理量測資料 By QueryCustome_Guid
        public void RemoveDataByQueryCustomeGuid(string guid)
        {
            try
            {
                var Q = _context.query_custome.Where(x => x.guid == guid && x.status == "D").SingleOrDefault();

                var Data = _context.gauge_data.Where(x => x.barcode == Q.barcode && x.status == "D").ToList();
                foreach (var item in Data)
                {
                    _context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }
    }

    public class LabDataService
    {
        private MornjoyContext _context = new MornjoyContext();

        //新增檢驗資料 By API_Data
        public bool InsertLabData(List<LabData> Model)
        {
            try
            {
                var error = 0;
                foreach (var item in Model)
                {
                    //檢查是否重複新增 (檢查Barcode 體檢日)
                    var check = _context.lab_data.Where(x => x.status != "D" && x.barcode == item.Barcode && x.checkdate == item.CheckDate).Any();
                    if (check == false)
                    {
                        List<LabDataitem> result = new List<LabDataitem>();

                        //處理項目
                        foreach (var Dataitem in item.Item)
                        {
                            //取得對應類別
                            var category = _context.data_category.Where(x => x.status == "Y" && x.itemNo == Dataitem.ItemNo).SingleOrDefault();
                            if (category != null)
                            {
                                Dataitem.system_name = category.itemSystem;
                                Dataitem.category_name = category.itemCategory;

                                result.Add(Dataitem);
                            }
                        }

                        //將資料轉換成JSON
                        var jsondata = JsonConvert.SerializeObject(result);

                        //寫入資料庫
                        lab_data l = new lab_data();
                        l.guid = Guid.NewGuid().ToString();
                        l.barcode = item.Barcode;
                        l.old_barcode = item.Old_Barcode;
                        l.checkdate = item.CheckDate;
                        l.content = jsondata;
                        l.status = "Y";
                        l.modifydate = DateTime.Now;
                        l.create_date = DateTime.Now;
                        l.lang = "tw";
                        _context.Entry(l).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                if (error > 0)
                {
                    return false;
                }
                else
                {
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //修改檢驗資料狀態 By Barcode Status
        public void ChangeStatusByBarcode(string guid, string Barcode, string status)
        {
            try
            {
                var Data = _context.lab_data.Where(x => x.barcode == Barcode && x.query_guid == guid).ToList();
                foreach (var item in Data)
                {
                    item.status = status;
                    _context.Entry(item).State = System.Data.Entity.EntityState.Modified;
                }
                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }

        //刪除檢驗資料 By QueryCustome_Guid
        public void RemoveDataByQueryCustomeGuid(string guid)
        {
            try
            {
                var Q = _context.query_custome.Where(x => x.guid == guid && x.status == "D").SingleOrDefault();

                var Data = _context.lab_data.Where(x => x.barcode == Q.barcode && x.status == "D").ToList();
                foreach (var item in Data)
                {
                    _context.Entry(item).State = System.Data.Entity.EntityState.Deleted;
                }

                _context.SaveChanges();
            }
            catch (Exception ex)
            {
                var error = ex;
            }
        }
    }

    public class DataService
    {
        private MornjoyContext _context = new MornjoyContext();

        public Dictionary<string, List<string>> GetPic(string lang, string barcode, string itemNo)
        {
            try
            {
                var QueryCustome = _context.query_custome.Where(x => x.lang == lang && x.barcode == barcode && x.status != "D").SingleOrDefault();
                if (QueryCustome != null && !string.IsNullOrEmpty(QueryCustome.pic) && !string.IsNullOrEmpty(QueryCustome.pic_alt))
                {
                    Dictionary<string, string> pic = JsonConvert.DeserializeObject<Dictionary<string, string>>(QueryCustome.pic);
                    Dictionary<string, string> pic_alt = JsonConvert.DeserializeObject<Dictionary<string, string>>(QueryCustome.pic_alt);

                    var p = pic.ContainsKey("pic_" + itemNo) ? pic["pic_" + itemNo] : null;
                    var a = pic_alt.ContainsKey("pic_" + itemNo + "_alt") ? pic_alt["pic_" + itemNo + "_alt"] : null;

                    if (!string.IsNullOrEmpty(p))
                    {
                        var result_pic = p.Split(',').ToList();
                        var result_picalt = a.Split('§').ToList();

                        List<string> jPic = new List<string>();
                        List<string> jPicAlt = new List<string>();

                        foreach (var item1 in result_pic)
                        {
                            var jPicSplit = item1.Split('/');
                            string index_urltemp = "";
                            int deleteindex = 0;

                            for (var i = 0; i < jPicSplit.Length; i++)
                            {
                                if (jPicSplit[i] == "Content")
                                {
                                    deleteindex = i;
                                }
                            }

                            for (var j = 0; j < deleteindex; j++)
                            {
                                jPicSplit[j] = "";
                            }

                            for (var k = 0; k < jPicSplit.Length; k++)
                            {
                                if (jPicSplit[k] != "")
                                {
                                    index_urltemp = index_urltemp + "/" + jPicSplit[k];
                                }
                            }

                            jPic.Add(index_urltemp);
                        }

                        Dictionary<string, List<string>> result = new Dictionary<string, List<string>>
                        {
                            //{ "pic", result_pic },
                            { "pic", jPic },
                            { "pic_alt", result_picalt }
                        };

                        return result;
                    }
                }

                return null;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public Dictionary<int, List<string>> PicGroup(List<string> data, int number)
        {
            try
            {
                if (data != null)
                {
                    var num = 0;
                    var g = data.GroupBy(x => num++ / number).ToList();

                    Dictionary<int, List<string>> result = new Dictionary<int, List<string>>();
                    foreach (var item in g)
                    {
                        result.Add(item.Key, item.ToList());
                    }

                    return result;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int GetPicNumber(int? pic_count)
        {
            try
            {
                var result = 0;
                if (pic_count != null)
                {
                    var count = pic_count;
                    if (count <= 1)
                    {
                        result = 1;
                    }
                    else if (count == 2)
                    {
                        result = 2;
                    }
                    else if (count == 3)
                    {
                        result = 3;
                    }
                    else if (count == 4)
                    {
                        result = 4;
                    }
                    else if (count == 6)
                    {
                        result = 6;
                    }
                    else
                    {
                        result = 8;
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                return 8;
            }
        }

        //取得身體構造圖資訊
        public Dictionary<string, Dictionary<string, int>> GetSystemVOerviewData(string lang, string barcode)
        {
            try
            {
                Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                Dictionary<string, int> left = new Dictionary<string, int>();
                left.Add("神經系統", 0);
                left.Add("頭頸部與心血管系統", 0);
                left.Add("胸腔與呼吸系統", 0);
                left.Add("腹部與肝膽腸胃系統", 0);
                left.Add("泌尿生殖系統", 0);
                left.Add("肌肉骨骼系統", 0);

                Dictionary<string, int> right = new Dictionary<string, int>();
                right.Add("一般體格", 0);
                right.Add("血液", 0);
                right.Add("免疫系統", 0);
                right.Add("重金屬檢查", 0);
                right.Add("基因檢查", 0);
                right.Add("新陳代謝", 0);
                right.Add("醫生問診", 0);

                foreach (var item in GaugeData_json)
                {
                    if (left.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        left[item.system_name] += 1;
                    }
                    else if (right.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        right[item.system_name] += 1;
                    }
                }

                foreach (var item in LabData_json)
                {
                    if (left.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        left[item.system_name] += 1;
                    }
                    else if (right.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        right[item.system_name] += 1;
                    }
                }

                result.Add("Left", left);
                result.Add("Right", right);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得身體構造圖資訊(後台顯示)
        public Dictionary<string, Dictionary<string, int>> GetSystemVOerviewData_Backend(string lang, string barcode)
        {
            try
            {
                Dictionary<string, Dictionary<string, int>> result = new Dictionary<string, Dictionary<string, int>>();

                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                Dictionary<string, int> left = new Dictionary<string, int>();
                left.Add("神經系統", 0);
                left.Add("頭頸部與心血管系統", 0);
                left.Add("胸腔與呼吸系統", 0);
                left.Add("腹部與肝膽腸胃系統", 0);
                left.Add("泌尿生殖系統", 0);
                left.Add("肌肉骨骼系統", 0);

                Dictionary<string, int> right = new Dictionary<string, int>();
                right.Add("一般體格", 0);
                right.Add("血液", 0);
                right.Add("免疫系統", 0);
                right.Add("重金屬檢查", 0);
                right.Add("基因檢查", 0);
                right.Add("新陳代謝", 0);
                right.Add("醫生問診", 0);

                foreach (var item in GaugeData_json)
                {
                    if (left.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        left[item.system_name] += 1;
                    }
                    else if (right.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        right[item.system_name] += 1;
                    }
                }

                foreach (var item in LabData_json)
                {
                    if (left.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        left[item.system_name] += 1;
                    }
                    else if (right.ContainsKey(item.system_name) && item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        right[item.system_name] += 1;
                    }
                }

                result.Add("Left", left);
                result.Add("Right", right);

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得異常清單
        public List<Abnormality> GetAbnormalityData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Abnormality> result = new List<Abnormality>();

                //檢查異常生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Abnormality r = new Abnormality();
                    if (item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.suggest = item.Suggest;
                        r.e_suggest = item.ESuggest;

                        r.orderindex = int.Parse(item.OrderIndex);

                        result.Add(r);
                    }
                }

                //檢查異常檢驗資料
                foreach (var item in LabData_json)
                {
                    Abnormality r = new Abnormality();
                    if (item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.suggest = item.Suggest;
                        r.e_suggest = item.ESuggest;
                        r.orderindex = int.Parse(item.OrderIndex);
                        result.Add(r);
                    }
                }

                //依照排序
                result = result.OrderBy(x => x.orderindex).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得異常清單(後台顯示)
        public List<Abnormality> GetAbnormalityData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Abnormality> result = new List<Abnormality>();

                //檢查異常生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Abnormality r = new Abnormality();
                    if (item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.suggest = item.Suggest;
                        r.e_suggest = item.ESuggest;

                        r.orderindex = int.Parse(item.OrderIndex);

                        result.Add(r);
                    }
                }

                //檢查異常檢驗資料
                foreach (var item in LabData_json)
                {
                    Abnormality r = new Abnormality();
                    if (item.Err == "true" && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.suggest = item.Suggest;
                        r.e_suggest = item.ESuggest;
                        r.orderindex = int.Parse(item.OrderIndex);
                        result.Add(r);
                    }
                }

                //依照排序
                result = result.OrderBy(x => x.orderindex).ToList();

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得理學檢查
        public Dictionary<string, List<Physical>> GetPhysicalData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                Dictionary<string, List<Physical>> result = new Dictionary<string, List<Physical>>();
                List<Physical> body = new List<Physical>();
                List<Physical> dr = new List<Physical>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Physical r = new Physical();
                    if ((item.CheckCategory == "一般體格" || item.category_name == "一般體格") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;

                        body.Add(r);
                    }
                    else if ((item.CheckCategory == "醫生問診" || item.category_name == "醫生問診") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        dr.Add(r);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Physical r = new Physical();
                    if ((item.CheckCategory == "一般體格" || item.category_name == "一般體格") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        body.Add(r);
                    }
                    else if ((item.CheckCategory == "醫生問診" || item.category_name == "醫生問診") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        dr.Add(r);
                    }
                }

                body = body.OrderBy(x => x.orderindex).ToList();
                dr = dr.OrderBy(x => x.orderindex).ToList();

                result.Add("一般體格", body);
                result.Add("醫生問診", dr);

                if (body.Count == 0 && dr.Count == 0)
                {
                    return null;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得理學檢查(後台顯示)
        public Dictionary<string, List<Physical>> GetPhysicalData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                Dictionary<string, List<Physical>> result = new Dictionary<string, List<Physical>>();
                List<Physical> body = new List<Physical>();
                List<Physical> dr = new List<Physical>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Physical r = new Physical();
                    if ((item.CheckCategory == "一般體格" || item.category_name == "一般體格") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;

                        body.Add(r);
                    }
                    else if ((item.CheckCategory == "醫生問診" || item.category_name == "醫生問診") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        dr.Add(r);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Physical r = new Physical();
                    if ((item.CheckCategory == "一般體格" || item.category_name == "一般體格") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        body.Add(r);
                    }
                    else if ((item.CheckCategory == "醫生問診" || item.category_name == "醫生問診") && !string.IsNullOrEmpty(item.Value))
                    {
                        r.name = item.ItemName;
                        r.value = item.Value;
                        r.reference = item.Ref;
                        r.orderindex = int.Parse(item.OrderIndex);
                        r.err = item.Err;
                        dr.Add(r);
                    }
                }

                body = body.OrderBy(x => x.orderindex).ToList();
                dr = dr.OrderBy(x => x.orderindex).ToList();

                result.Add("一般體格", body);
                result.Add("醫生問診", dr);

                if (body.Count == 0 && dr.Count == 0)
                {
                    return null;
                }
                else
                {
                    return result;
                }
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得檢驗資料
        public Dictionary<string, List<Examine>> GetExamineData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Examine> r = new List<Examine>();
                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Examine e = new Examine();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Examine e = new Examine();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                Dictionary<string, List<Examine>> result = r.OrderBy(x => x.orderindex).GroupBy(x => x.category).ToDictionary(x => x.Key, x => x.ToList());

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得檢驗資料(後台顯示用)
        public Dictionary<string, List<Examine>> GetExamineData_BackEnd(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Examine> r = new List<Examine>();
                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Examine e = new Examine();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Examine e = new Examine();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckExamineData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                Dictionary<string, List<Examine>> result = r.OrderBy(x => x.orderindex).GroupBy(x => x.category).ToDictionary(x => x.Key, x => x.ToList());

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Examine CheckExamineData(string category, dynamic item)
        {
            try
            {
                dynamic result = null;
                switch (category)
                {
                    case "尿液常規檢查":
                    case "糞便檢查":
                    case "血液檢查":
                    case "生化檢查":
                    case "肝炎檢查":
                    case "腫瘤標記":
                    case "甲狀腺檢查":
                    case "血清免疫檢查":
                    case "賀爾蒙檢查":
                    case "基因檢查":
                    case "重金屬檢查":
                    case "功能醫學":
                        Examine e = new Examine();
                        e.name = item.ItemName;
                        e.value = item.Value;
                        e.reference = item.Ref;
                        e.orderindex = int.Parse(item.OrderIndex);
                        e.category = category;
                        e.err = item.Err;
                        result = e;
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得超音波資料
        public List<Imaging> GetUltrasoundData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging u = new Imaging();
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 0;
                        //switch (int.Parse(item.ItemNo))
                        //{
                        //    //4 * 2
                        //    case 7010:
                        //    case 7012:
                        //    case 7015:
                        //    case 7016:
                        //        number = 8;
                        //        break;
                        //    //3 * 2
                        //    case 7014:
                        //        number = 6;
                        //        break;
                        //    //2 * 2
                        //    case 7013:
                        //        number = 4;
                        //        break;
                        //    //3 * 1
                        //    case 7011:
                        //        number = 3;
                        //        break;
                        //}

                        u.name = item.ItemName;
                        u.value = item.Value;
                        u.orderindex = int.Parse(item.OrderIndex);
                        u.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            u.height = pic_Width_Height["Hight"];
                            u.width = pic_Width_Height["Width"];
                            u.pic = p;
                            u.pic_alt = pa;
                        }
                        result.Add(u);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging u = new Imaging();
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 0;
                        //switch (int.Parse(item.ItemNo))
                        //{
                        //    //4 * 2
                        //    case 7010:
                        //    case 7012:
                        //    case 7015:
                        //    case 7016:
                        //        number = 8;
                        //        break;
                        //    //3 * 2
                        //    case 7014:
                        //        number = 6;
                        //        break;
                        //    //2 * 2
                        //    case 7013:
                        //        number = 4;
                        //        break;
                        //    //3 * 1
                        //    case 7011:
                        //        number = 3;
                        //        break;
                        //}

                        u.name = item.ItemName;
                        u.value = item.Value;
                        u.orderindex = int.Parse(item.OrderIndex);
                        u.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            u.height = pic_Width_Height["Hight"];
                            u.width = pic_Width_Height["Width"];
                            u.pic = p;
                            u.pic_alt = pa;
                        }
                        result.Add(u);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得超音波資料(後台顯示用)
        public List<Imaging> GetUltrasoundData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging u = new Imaging();
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 0;
                        //switch (int.Parse(item.ItemNo))
                        //{
                        //    //4 * 2
                        //    case 7010:
                        //    case 7012:
                        //    case 7015:
                        //    case 7016:
                        //        number = 8;
                        //        break;
                        //    //3 * 2
                        //    case 7014:
                        //        number = 6;
                        //        break;
                        //    //2 * 2
                        //    case 7013:
                        //        number = 4;
                        //        break;
                        //    //3 * 1
                        //    case 7011:
                        //        number = 3;
                        //        break;
                        //}

                        u.name = item.ItemName;
                        u.value = item.Value;
                        u.orderindex = int.Parse(item.OrderIndex);
                        u.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            u.height = pic_Width_Height["Hight"];
                            u.width = pic_Width_Height["Width"];
                            u.pic = p;
                            u.pic_alt = pa;
                        }
                        result.Add(u);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging u = new Imaging();
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 0;
                        //switch (int.Parse(item.ItemNo))
                        //{
                        //    //4 * 2
                        //    case 7010:
                        //    case 7012:
                        //    case 7015:
                        //    case 7016:
                        //        number = 8;
                        //        break;
                        //    //3 * 2
                        //    case 7014:
                        //        number = 6;
                        //        break;
                        //    //2 * 2
                        //    case 7013:
                        //        number = 4;
                        //        break;
                        //    //3 * 1
                        //    case 7011:
                        //        number = 3;
                        //        break;
                        //}

                        u.name = item.ItemName;
                        u.value = item.Value;
                        u.orderindex = int.Parse(item.OrderIndex);
                        u.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            u.height = pic_Width_Height["Hight"];
                            u.width = pic_Width_Height["Width"];
                            u.pic = p;
                            u.pic_alt = pa;
                        }
                        result.Add(u);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得數位斷層資料
        public List<Digital> GetDigitalData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Digital> result = new List<Digital>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Digital d = new Digital();
                    if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        d.name = item.ItemName;
                        d.value = item.Value;
                        d.orderindex = int.Parse(item.OrderIndex);
                        d.itemno = int.Parse(item.ItemNo);


                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            d.height = pic_Width_Height["Hight"];
                            d.width = pic_Width_Height["Width"];
                            d.pic = p;
                            d.pic_alt = pa;
                        }

                        result.Add(d);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Digital d = new Digital();
                    if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        d.name = item.ItemName;
                        d.value = item.Value;
                        d.orderindex = int.Parse(item.OrderIndex);
                        d.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            d.height = pic_Width_Height["Hight"];
                            d.width = pic_Width_Height["Width"];
                            d.pic = p;
                            d.pic_alt = pa;
                        }

                        result.Add(d);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得數位斷層資料
        public List<Digital> GetDigitalData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Digital> result = new List<Digital>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Digital d = new Digital();
                    if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        d.name = item.ItemName;
                        d.value = item.Value;
                        d.orderindex = int.Parse(item.OrderIndex);
                        d.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            d.height = pic_Width_Height["Hight"];
                            d.width = pic_Width_Height["Width"];
                            d.pic = p;
                            d.pic_alt = pa;
                        }

                        result.Add(d);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Digital d = new Digital();
                    if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        d.name = item.ItemName;
                        d.value = item.Value;
                        d.orderindex = int.Parse(item.OrderIndex);
                        d.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            d.height = pic_Width_Height["Hight"];
                            d.width = pic_Width_Height["Width"];
                            d.pic = p;
                            d.pic_alt = pa;
                        }

                        result.Add(d);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得胸部能量資料
        public List<Imaging> GetChestDualEnergyData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging c = new Imaging();
                    if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 3;

                        c.name = item.ItemName;
                        c.value = item.Value;
                        c.orderindex = int.Parse(item.OrderIndex);
                        c.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            c.height = pic_Width_Height["Hight"];
                            c.width = pic_Width_Height["Width"];
                            c.pic = p;
                            c.pic_alt = pa;
                        }
                        result.Add(c);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging c = new Imaging();
                    if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 3;

                        c.name = item.ItemName;
                        c.value = item.Value;
                        c.orderindex = int.Parse(item.OrderIndex);
                        c.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            c.height = pic_Width_Height["Hight"];
                            c.width = pic_Width_Height["Width"];
                            c.pic = p;
                            c.pic_alt = pa;
                        }
                        result.Add(c);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得胸部能量資料
        public List<Imaging> GetChestDualEnergyData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging c = new Imaging();
                    if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 3;

                        c.name = item.ItemName;
                        c.value = item.Value;
                        c.orderindex = int.Parse(item.OrderIndex);
                        c.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            c.height = pic_Width_Height["Hight"];
                            c.width = pic_Width_Height["Width"];
                            c.pic = p;
                            c.pic_alt = pa;
                        }
                        result.Add(c);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging c = new Imaging();
                    if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 3;

                        c.name = item.ItemName;
                        c.value = item.Value;
                        c.orderindex = int.Parse(item.OrderIndex);
                        c.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);

                        var number = GetPicNumber(picalt?["pic"].Count);

                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            c.height = pic_Width_Height["Hight"];
                            c.width = pic_Width_Height["Width"];
                            c.pic = p;
                            c.pic_alt = pa;
                        }
                        result.Add(c);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得X光資料
        public List<Imaging> GetXrayData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);
                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);
                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得X光資料
        public List<Imaging> GetXrayData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);
                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);
                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得三連片資料
        public List<Imaging> GetTripleData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得三連片資料
        public List<Imaging> GetTripleData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得內視鏡資料
        public List<Imaging> GetEndoscopeData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 8;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 8;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得內視鏡資料
        public List<Imaging> GetEndoscopeData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 8;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 8;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得身體組成分析
        public List<Imaging> GetBodyData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得身體組成分析
        public List<Imaging> GetBodyData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得心電圖資料
        public List<Imaging> GetElectrocardiogramData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }

                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }

                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得心電圖資料
        public List<Imaging> GetElectrocardiogramData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }

                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);
                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }

                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得自律神經檢查
        public List<Imaging> GetAutonomicData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得自律神經檢查
        public List<Imaging> GetAutonomicData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 1;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得眼底攝影資料
        public List<Imaging> GetFundusData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 2;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 2;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得眼底攝影資料
        public List<Imaging> GetFundusData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Imaging> result = new List<Imaging>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 2;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Imaging x = new Imaging();
                    if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        //var number = 2;

                        x.name = item.ItemName;
                        x.value = item.Value;
                        x.orderindex = int.Parse(item.OrderIndex);
                        x.itemno = int.Parse(item.ItemNo);

                        //取得該項目的圖片
                        var picalt = GetPic(lang, barcode, item.ItemNo);
                        var number = GetPicNumber(picalt?["pic"].Count);
                        if (picalt != null)
                        {
                            var p = PicGroup(picalt?["pic"], number);
                            var pa = PicGroup(picalt?["pic_alt"], number);

                            var pic = picalt["pic"];
                            var pic_number = pic.Count();
                            var pic_Width_Height = PicWidthHeight(pic_number);

                            x.height = pic_Width_Height["Hight"];
                            x.width = pic_Width_Height["Width"];
                            x.pic = p;
                            x.pic_alt = pa;
                        }
                        result.Add(x);
                    }
                }

                result = result.OrderBy(x => x.orderindex).ToList();
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得其他資料
        public Dictionary<string, List<Other>> GetOtherData(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Other> r = new List<Other>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Other e = new Other();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Other e = new Other();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                Dictionary<string, List<Other>> result = r.OrderBy(x => x.orderindex).GroupBy(x => x.category).ToDictionary(x => x.Key, x => x.ToList());

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得其他資料
        public Dictionary<string, List<Other>> GetOtherData_Backend(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status != "D" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<Other> r = new List<Other>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    Other e = new Other();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    Other e = new Other();
                    if (!string.IsNullOrEmpty(item.CheckCategory) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.CheckCategory, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                    else if (!string.IsNullOrEmpty(item.category_name) && !string.IsNullOrEmpty(item.Value))
                    {
                        var data = CheckOtherData(item.category_name, item);
                        if (data != null)
                        {
                            r.Add(data);
                        }
                    }
                }

                Dictionary<string, List<Other>> result = r.OrderBy(x => x.orderindex).GroupBy(x => x.category).ToDictionary(x => x.Key, x => x.ToList());

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Other CheckOtherData(string category, dynamic item)
        {
            try
            {
                dynamic result = null;
                switch (category)
                {
                    case "肺功能檢查":
                    case "抹片檢查":
                    case "精液檢查":
                        Other e = new Other();
                        e.name = item.ItemName;
                        e.value = item.Value;
                        e.reference = item.Ref;
                        e.orderindex = int.Parse(item.OrderIndex);
                        e.category = category;
                        e.itemno = int.Parse(item.ItemNo);
                        result = e;
                        break;
                }
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, List<string>> GetImagingDataitemNo(string lang, string barcode)
        {
            try
            {
                //取得資料
                gauge_data GaugeData = new gauge_data();
                GaugeData = _context.gauge_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                lab_data LabData = new lab_data();
                LabData = _context.lab_data.Where(x => x.status == "Y" && x.lang == lang && x.barcode == barcode && x.old_barcode == barcode).SingleOrDefault();

                //將資料轉換成JSON
                var GaugeData_json = JsonConvert.DeserializeObject<List<GaugeDataitem>>(GaugeData.content);
                var LabData_json = JsonConvert.DeserializeObject<List<LabDataitem>>(LabData.content);

                List<string> Ultrasound = new List<string>();
                List<string> DigitalEnergy = new List<string>();
                List<string> ChestDualEnergy = new List<string>();
                List<string> Xray = new List<string>();
                List<string> Triple = new List<string>();
                List<string> Endoscope = new List<string>();
                List<string> Body = new List<string>();
                List<string> Electrocardiogram = new List<string>();
                List<string> Autonomic = new List<string>();
                List<string> Fundus = new List<string>();

                //處理生理量測資料
                foreach (var item in GaugeData_json)
                {
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        Ultrasound.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        DigitalEnergy.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        ChestDualEnergy.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        Xray.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        Triple.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        Endoscope.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        Body.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        Electrocardiogram.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        Autonomic.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        Fundus.Add(item.ItemNo);
                    }
                }

                //處理檢驗資料
                foreach (var item in LabData_json)
                {
                    if ((item.CheckCategory == "超音波檢查" || item.category_name == "超音波檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        Ultrasound.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "數位斷層合成掃描" || item.category_name == "數位斷層合成掃描") && !string.IsNullOrEmpty(item.Value))
                    {
                        DigitalEnergy.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "胸部能量減影" || item.category_name == "胸部能量減影") && !string.IsNullOrEmpty(item.Value))
                    {
                        ChestDualEnergy.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "一般X光" || item.category_name == "一般X光") && !string.IsNullOrEmpty(item.Value))
                    {
                        Xray.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "三連片" || item.category_name == "三連片") && !string.IsNullOrEmpty(item.Value))
                    {
                        Triple.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "內視鏡" || item.category_name == "內視鏡") && !string.IsNullOrEmpty(item.Value))
                    {
                        Endoscope.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "身體組成分析" || item.category_name == "身體組成分析") && !string.IsNullOrEmpty(item.Value))
                    {
                        Body.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "心電圖" || item.category_name == "心電圖") && !string.IsNullOrEmpty(item.Value))
                    {
                        Electrocardiogram.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "自律神經檢查" || item.category_name == "自律神經檢查") && !string.IsNullOrEmpty(item.Value))
                    {
                        Autonomic.Add(item.ItemNo);
                    }
                    else if ((item.CheckCategory == "眼底攝影" || item.category_name == "眼底攝影") && !string.IsNullOrEmpty(item.Value))
                    {
                        Fundus.Add(item.ItemNo);
                    }
                }

                Dictionary<string, List<string>> result = new Dictionary<string, List<string>>();
                result.Add("Ultrasound", Ultrasound.OrderBy(x => int.Parse(x)).ToList());
                result.Add("DigitalEnergy", DigitalEnergy.OrderBy(x => int.Parse(x)).ToList());
                result.Add("ChestDualEnergy", ChestDualEnergy.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Xray", Xray.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Triple", Triple.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Endoscope", Endoscope.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Body", Body.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Electrocardiogram", Electrocardiogram.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Autonomic", Autonomic.OrderBy(x => int.Parse(x)).ToList());
                result.Add("Fundus", Fundus.OrderBy(x => int.Parse(x)).ToList());

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public Dictionary<string, string> PicWidthHeight(int pic_number)
        {
            try
            {
                Dictionary<string, string> result = new Dictionary<string, string>();

                if (pic_number <= 1)
                {
                    result.Add("Width", "960px;");
                    result.Add("Hight", "");
                }
                else if (pic_number == 2)
                {
                    result.Add("Width", "960px;");
                    result.Add("Hight", "");
                }
                else if (pic_number == 3)
                {
                    result.Add("Width", "600px");
                    result.Add("Hight", "");
                }
                else if (pic_number >= 4 && pic_number <= 6)
                {
                    result.Add("Width", "480px");
                    result.Add("Hight", "");
                }
                else if (pic_number >= 7 && pic_number <= 8)
                {
                    result.Add("Width", "480px");
                    result.Add("Hight", "");
                }
                else if (pic_number > 8)
                {
                    result.Add("Width", "480px");
                    result.Add("Hight", "");
                }
                else
                {
                    return null;
                }

                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        
    }
}