//線上使用者/單次頁數/停留時間/跳出率
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/nowUser",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#nowUser").text(json.nowUser);
            $("#pageviewsPerSession").text(json.pageviewsPerSession);
            $("#avgTimeOnPage").text(json.avgTimeOnPage);
            $("#exitRate").text(json.exitRate);
        }
    }
});

//瀏覽器使用率
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/browser",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#chrome").text(json[0].GoogleChrome + "%");
            $("#firefox").text(json[1].MozilaFirefox + "%");
            $("#ie").text(json[2].InterentExplorer + "%");
            $("#safari").text(json[3].Safari + "%");
        }
    }
});

//流量來源
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/organicSearches",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#organic").text(json.organic + " 訪問");
            $("#direct").text(json.direct + " 訪問");
            $("#referral").text(json.referral + " 訪問");
            $("#organicPer").text(json.organicPer + " %  搜索");
            $("#directPer").text(json.directPer + " %  直接");
            $("#referralPer").text(json.referralPer + " %  推薦");

            //圓餅圖
            AmCharts.makeChart("analyticsTraffic", {
                "type": "pie",
                "theme": "light",
                "dataProvider": [{
                    "country": "搜尋",
                    "litres": json.organic
                },
                {
                    "country": "直接",
                    "litres": json.direct
                },
                {
                    "country": "推薦",
                    "litres": json.referral
                }],
                "titleField": "country",
                "valueField": "litres",
                "balloonText": "[[title]]<br><span style='font-size:14px'><b>[[value]]</b> ([[percents]]%)</span>",
                "innerRadius": "0%",
                "labelRadius": 5,
                "radius": "45%",
                "labelText": "",
            });
        }
    }
});

//流量來源(每日流量 波浪圖)
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/organicSearchesList",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            //String to Json or null
            var traffic_organic_data = "";
            if (json.traffic_organic !== null && json.traffic_organic !== "") {
                traffic_organic_data = $.parseJSON(json.traffic_organic);
            }
            else {
                traffic_organic_data = null;
            }

            var traffic_direct_data = "";
            if (json.traffic_direct !== null && json.traffic_direct !== "") {
                traffic_direct_data = $.parseJSON(json.traffic_direct);
            }
            else {
                traffic_direct_data = null;
            }

            var traffic_referral_data = "";
            if (json.traffic_referral !== null && json.traffic_referral !== "") {
                traffic_referral_data = $.parseJSON(json.traffic_referral);
            }
            else {
                traffic_referral_data = null;
            }

            //"搜索"波浪圖
            AmCharts.makeChart("traffic1", {
                "type": "serial",
                "theme": "light",
                "dataProvider": traffic_organic_data,
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "zeroGridAlpha": 0,
                }],
                "categoryAxis": {
                    "dashLength": 0,
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                },
                "balloon": {
                    "shadowAlpha": 0,
                },
                "marginTop": 0,
                "marginRight": 0,
                "marginLeft": 0,
                "marginBottom": 0,
                "autoMargins": false,
                "startDuration": 1,
                "graphs": [{
                    "id": "g1",
                    "balloon": {
                        "drop": false,
                        "adjustBorderColor": false,
                        "color": "#ffffff",
                    },
                    "fillAlphas": 0.2,
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "hideBulletsCount": 5,
                    "lineThickness": 1,
                    "valueField": "value",
                }],
                "chartCursor": {
                    "valueLineEnabled": false,
                    "categoryBalloonEnabled": false,
                    "cursorColor": "#67b7dc",
                    "pan": true,
                },

                "categoryField": "date",

                "export": {
                    "enabled": false
                },
            });
            //"直接"波浪圖
            AmCharts.makeChart("traffic2", {
                "type": "serial",
                "theme": "light",
                "dataProvider": traffic_direct_data,
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "zeroGridAlpha": 0,
                }],
                "categoryAxis": {
                    "dashLength": 0,
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                },
                "balloon": {
                    "shadowAlpha": 0,
                },
                "marginTop": 0,
                "marginRight": 0,
                "marginLeft": 0,
                "marginBottom": 0,
                "autoMargins": false,
                "startDuration": 1,
                "graphs": [{
                    "id": "g1",
                    "balloon": {
                        "drop": false,
                        "adjustBorderColor": false,
                        "color": "#ffffff",
                    },
                    "fillAlphas": 0.2,
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "hideBulletsCount": 5,
                    "lineThickness": 1,
                    "valueField": "value",
                }],
                "chartCursor": {
                    "valueLineEnabled": false,
                    "categoryBalloonEnabled": false,
                    "cursorColor": "#67b7dc",
                    "pan": true,
                },

                "categoryField": "date",

                "export": {
                    "enabled": false
                },
            });
            //"推薦"波浪圖
            AmCharts.makeChart("traffic3", {
                "type": "serial",
                "theme": "light",
                "dataProvider": traffic_referral_data,
                "valueAxes": [{
                    "id": "v1",
                    "axisAlpha": 0,
                    "gridAlpha": 0,
                    "zeroGridAlpha": 0,
                }],
                "categoryAxis": {
                    "dashLength": 0,
                    "gridAlpha": 0,
                    "axisAlpha": 0,
                },
                "balloon": {
                    "shadowAlpha": 0,
                },
                "marginTop": 0,
                "marginRight": 0,
                "marginLeft": 0,
                "marginBottom": 0,
                "autoMargins": false,
                "startDuration": 1,
                "graphs": [{
                    "id": "g1",
                    "balloon": {
                        "drop": false,
                        "adjustBorderColor": false,
                        "color": "#ffffff",
                    },
                    "fillAlphas": 0.2,
                    "bullet": "round",
                    "bulletBorderAlpha": 1,
                    "bulletColor": "#FFFFFF",
                    "hideBulletsCount": 5,
                    "lineThickness": 1,
                    "valueField": "value",
                }],
                "chartCursor": {
                    "valueLineEnabled": false,
                    "categoryBalloonEnabled": false,
                    "cursorColor": "#67b7dc",
                    "pan": true,
                },

                "categoryField": "date",

                "export": {
                    "enabled": false
                },
            });
        }
    }
});

//年度數量
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/year",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            var chart = AmCharts.makeChart("analyticsNetflow", {
                "theme": "light",
                "type": "serial",
                //"theme": "light",
                "dataProvider": json,
                "valueAxes": [{
                    "gridColor": "#878787",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "color": "#878787"
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits",
                    "color": "#878787"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "age",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20,
                    "color": "#878787"
                },
                "export": {
                    "enabled": false
                }
            });
        }
    },
    complete: function () { //生成分頁條
    },
    error: function () {
        //alert("讀取錯誤!");
    }
});

//熱門頁面
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/hotpages",
    dataType: 'json',
    success: function (json) {
        var toAppend = '';
        if (json !== "Error") {
            $.each(json, function (index, value) {
                toAppend += '<tr>';
                toAppend += '<td class="text-center">' + index + '</td>';
                toAppend += '<td><a class="text-primary text-nowrap ellipsis" href="' + value[0] + '" target="_blank">' + value[0] + '</a></td>';
                toAppend += '<td class="text-center">' + value[1] + '</td>';
                toAppend += '<td class="text-center">' + value[2] + '</td>';
                toAppend += '</tr>';
            });
            $("#HotPages").empty();
            $("#HotPages").append(toAppend);
        }
    }
});

//關鍵字
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/keyword",
    dataType: 'json',
    success: function (json) {
        var toAppend = '';
        if (json !== "Error") {
            var num = 1;
            $.each(json, function (index, value) {
                toAppend += '<tr>';
                toAppend += '<th class="text-center" scope="row">' + num + ' </th>';
                toAppend += '<td>' + index + '</td>';
                toAppend += '<td>' + value + '</td>';
                toAppend += '</tr>';
                num++;
            });
            $("#keywordAppend").empty();
            $("#keywordAppend").append(toAppend);
        }
    }
});

//年齡層
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/age",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            var chart = AmCharts.makeChart("analyticsAge", {
                "theme": "light",
                "type": "serial",
                "dataProvider": json,
                "valueAxes": [{
                    "gridColor": "#878787",
                    "gridAlpha": 0.2,
                    "dashLength": 0,
                    "color": "#878787"
                }],
                "gridAboveGraphs": true,
                "startDuration": 1,
                "graphs": [{
                    "balloonText": "[[category]]: <b>[[value]]</b>",
                    "fillAlphas": 0.8,
                    "lineAlpha": 0.2,
                    "type": "column",
                    "valueField": "visits",
                    "color": "#878787"
                }],
                "chartCursor": {
                    "categoryBalloonEnabled": false,
                    "cursorAlpha": 0,
                    "zoomable": false
                },
                "categoryField": "age",
                "categoryAxis": {
                    "gridPosition": "start",
                    "gridAlpha": 0,
                    "tickPosition": "start",
                    "tickLength": 20,
                    "color": "#878787"
                },
                "export": {
                    "enabled": false
                }
            });
        }
    },
    complete: function () { //生成分頁條
    },
    error: function () {
        //alert("讀取錯誤!");
    }
});

//總瀏覽次數
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/totalPathViews",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#totalPathViews").text(json.PathViews);
        }
    }
});

//總流量
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/totalOrganicSearches",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#totalOrganicSearches").text(json.TotalOrganicSearches);
        }
    }
});

//今日參觀
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/todayOrganicSearches",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            $("#TodayOrganicSearches").text(json.TodayOrganicSearches);
        }
    }
});

//客服信函
$.ajax({
    type: 'POST',
    url: $('#siteadminArea').val() + "/analytics/totalContact",
    dataType: 'json',
    success: function (json) {
        if (json !== "Error") {
            console.log(json);
        }
    }
});