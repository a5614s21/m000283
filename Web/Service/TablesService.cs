﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;
using Web.Repository;
using Newtonsoft.Json;
using System.IO;
using System.Text.RegularExpressions;
using System.Collections.Specialized;
using Newtonsoft.Json.Linq;
using System.Linq.Dynamic;
using System.Data.Entity;
using Web.Service;
using System.Web.Mvc;
using System.Configuration;
using System.Linq.Expressions;
using System.Web.Script.Serialization;
using Web.ViewModel;

namespace Web.Service
{
    public class TablesService : Controller
    {
        /// <summary>
        /// 取得列表資料提供給DataTable
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> getListData(string tables, NameValueCollection requests, string urlRoot, string keyword)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            Dictionary<String, Object> list = new Dictionary<String, Object>();
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            int skip = int.Parse(requests["iDisplayStart"].ToString());
            int take = int.Parse(requests["iDisplayLength"].ToString());
            if (take == -1)
            {
                take = 9999;
            }
            int iTotalDisplayRecords = 0;
            dynamic listData = null;
            dynamic sSearch = null;

            if (requests["sSearch"].ToString() != "")
            {
                sSearch = Newtonsoft.Json.Linq.JArray.Parse(requests["sSearch"].ToString());
                sSearch = sSearch[0];
            }

            string orderByKey = requests["mDataProp_" + requests["iSortCol_0"]].ToString();
            string orderByType = requests["sSortDir_0"].ToString();
            string orderBy = formatOrderByKey(tables, orderByKey) + " " + orderByType.ToUpper();

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    if (model != null)
                    {
                        var data = model.Repository<query_custome>();
                        var models = data.ReadsWhere(x => x.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.customername.Contains(keywords) || m.name.Contains(keywords) || m.user_id.Contains(keywords) || m.checkdate.Contains(keywords));
                            }

                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        else if (!string.IsNullOrEmpty(keyword))
                        {
                            keyword = HttpUtility.UrlDecode(keyword);
                            models = models.Where(m => m.customername.Contains(keyword) || m.name.Contains(keyword) || m.user_id.Contains(keyword) || m.checkdate.Contains(keyword));
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #endregion

                case "data_category":
                    if (model != null)
                    {
                        var data = model.Repository<data_category>();
                        var models = data.ReadsWhere(x => x.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.itemNo.Contains(keywords) || m.itemName.Contains(keywords) || m.itemSystem.Contains(keywords) || m.itemCategory.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }
                        else if (!string.IsNullOrEmpty(keyword))
                        {
                            keyword = HttpUtility.UrlDecode(keyword);
                            models = models.Where(m => m.itemNo.Contains(keyword) || m.itemName.Contains(keyword) || m.itemSystem.Contains(keyword) || m.itemCategory.Contains(keyword));
                        }
                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                #region -- Default --

                //聯絡我們
                case "contacts":
                    if (model != null)
                    {
                        var data = model.Repository<contacts>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (model != null)
                    {
                        var data = model.Repository<news_category>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //最新消息
                case "news":
                    if (model != null)
                    {
                        var data = model.Repository<news>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.lang == defLang);

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.category.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //群組管理
                case "roles":
                    if (model != null)
                    {
                        var data = model.Repository<roles>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //使用者
                case "user":
                    if (model != null)
                    {
                        var data = model.Repository<user>();

                        var models = data.ReadsWhere(m => m.status != "D").Where(m => m.username != "sysadmin");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.username.Contains(keywords) || m.name.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }

                            if (sSearch["category"].ToString() != "")
                            {
                                string category = sSearch["category"].ToString();
                                models = models.Where(m => m.role_guid.Contains(category));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //防火牆
                case "firewalls":
                    if (model != null)
                    {
                        var data = model.Repository<firewalls>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                            if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //系統日誌
                case "system_log":
                    if (model != null)
                    {
                        var data = model.Repository<system_log>();

                        var models = data.ReadsWhere(m => m.status != "D");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords) || m.ip.Contains(keywords) || m.notes.Contains(keywords));
                            }
                            /*if (sSearch["status"].ToString() != "")
                            {
                                string status = sSearch["status"].ToString();
                                models = models.Where(m => m.status == status);
                            }*/
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                //資源回收桶
                case "ashcan":
                    if (model != null)
                    {
                        var data = model.Repository<ashcan>();

                        var models = data.ReadsWhere(m => m.from_guid != "");

                        if (requests["sSearch"].ToString() != "")
                        {
                            if (sSearch["keyword"].ToString() != "")
                            {
                                string keywords = sSearch["keyword"].ToString();
                                models = models.Where(m => m.title.Contains(keywords));
                            }
                        }

                        iTotalDisplayRecords = models.ToList().Count;
                        listData = models.OrderBy(orderBy).ToList().Skip(skip).Take(take);
                    }
                    break;

                    #endregion
            }

            list.Add("data", dataTableListData(listData, tables, urlRoot, requests));
            list.Add("iTotalDisplayRecords", iTotalDisplayRecords);

            return list;
        }

        /// <summary>
        /// 調整DataTable資料輸出
        /// </summary>
        /// <param name="list"></param>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static List<Object> dataTableListData(dynamic list, string tables, string urlRoot, NameValueCollection requests)
        {
            System.Web.HttpContext context = System.Web.HttpContext.Current;
            Model DB = new Model();

            List<Object> re = new List<Object>();

            Dictionary<String, Object> dataTableRow = dataTableTitle(tables);//資料欄位

            string json = JsonConvert.SerializeObject(list, Formatting.Indented);//轉型(model to Json String)

            var Json1 = Newtonsoft.Json.Linq.JArray.Parse(json);//第一層列表轉json陣列
            string picurlRoot = urlRoot;
            if (urlRoot == "/")
            {
                urlRoot = "";
            }

            //取得選單一些基本資訊

            IQueryable<system_menu> system_menu_search = DB.system_menu.Where(m => m.tables == tables);
            if (requests["category"] != null && requests["category"].ToString() != "")
            {
                string category = requests["category"].ToString();
                string act_path = "list/" + category;
                system_menu_search = system_menu_search.Where(m => m.act_path == act_path);
            }

            var system_menu = system_menu_search.FirstOrDefault();
            if (system_menu == null)
            {
                system_menu = DB.system_menu.Where(m => m.tables == tables).FirstOrDefault();
            }

            int i = 0;
            foreach (var dataList in Json1)
            {
                Dictionary<String, Object> rowData = new Dictionary<string, object>();

                string thisGuid = dataList["guid"].ToString();

                rowData.Add("DT_RowId", "row_" + thisGuid);//欄位ID

                foreach (KeyValuePair<string, object> dataItem in dataTableRow)
                {
                    switch (dataItem.Key.ToString())
                    {
                        #region 鍵值

                        case "guid":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/checkbox.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                //是否瀏覽
                                if (tables == "query_custome")
                                {
                                    readText = readText.Replace("{$barcode}", dataList["barcode"].ToString());
                                }

                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "icon":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "style=\"background-color:#333;\"");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 圖片

                        case "list_pic":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/pic.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$value}", picurlRoot + dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$backColor}", "");
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 內容摘要

                        case "info":

                            if (thisGuid != null && thisGuid != "")
                            {
                                if (tables != "shareholders")
                                {
                                    string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/info.cshtml");
                                    string readText = System.IO.File.ReadAllText(path);

                                    readText = readText.Replace("{$value}", dataList["title"].ToString());

                                    //置頂
                                    string sticky = "";
                                    if (dataList["sticky"] != null && dataList["sticky"].ToString() != "")
                                    {
                                        if (dataList["sticky"].ToString() == "Y")
                                        {
                                            sticky = "<span class=\"badge badge-pill badge-warning ml-2\">置頂</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$sticky}", sticky);

                                    //今日異動
                                    string modifydate = "";
                                    if (dataList["modifydate"] != null && dataList["modifydate"].ToString() != "")
                                    {
                                        string date = DateTime.Parse(dataList["modifydate"].ToString()).ToString("yyyy-MM-dd");
                                        if (date == DateTime.Now.ToString("yyyy-MM-dd"))
                                        {
                                            modifydate = "<span class=\"badge badge-pill badge-info ml-2\">今日異動</span>";
                                        }
                                    }
                                    readText = readText.Replace("{$modifydate}", modifydate);

                                    //簡述
                                    string content = "";

                                    if (dataList["content"] != null && dataList["content"].ToString() != "")
                                    {
                                        content = Regex.Replace(dataList["content"].ToString(), "<.*?>", String.Empty);

                                        if (content.Length > 50)
                                        {
                                            content = content.Substring(0, 50) + "...";
                                        }
                                        content = "<p class=\"m-0 p-0 text-pre-line\"><small>" + content + "</small></p>";
                                    }
                                    readText = readText.Replace("{$content}", content);

                                    string notes = "";//

                                    readText = readText.Replace("{$notes}", notes);

                                    //連結
                                    string addId = "";
                                    //dataTableCategory
                                    if (context.Session["dataTableCategory"] != null)
                                    {
                                        addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                    }
                                    readText = readText.Replace("{$url}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                                else
                                {
                                    string readText = "<div>" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                    rowData.Add(dataItem.Key.ToString(), readText);
                                }
                            }
                            break;

                        #endregion

                        #region 分類或其他上層對應

                        case "category":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "news")
                                {
                                    string categoryID = dataList["category"].ToString();
                                    var category = DB.news_category.Where(m => m.guid == categoryID).FirstOrDefault();
                                    readText = category.title;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 複選分類

                        case "pluralCategory":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";
                                if (tables == "user")
                                {
                                    string[] categoryArr = dataList["role_guid"].ToString().Split(',');
                                    for (int s = 0; s < categoryArr.Length; s++)
                                    {
                                        string categoryID = categoryArr[s].ToString();
                                        var category = DB.roles.Where(m => m.guid == categoryID).FirstOrDefault();
                                        if (category != null)
                                        {
                                            readText += category.title + "<br>";
                                        }
                                    }
                                }

                                readText = "<div>" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 審核者

                        case "user":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string readText = "";

                                if (dataList["user_guid"] != null && dataList["user_guid"].ToString() != "")
                                {
                                    string categoryID = dataList["user_guid"].ToString();
                                    var category = DB.user.Where(m => m.guid == categoryID).FirstOrDefault();
                                    if (category != null)
                                    {
                                        readText = category.name;
                                    }
                                    else
                                    {
                                        readText = "查無審核者!";
                                    }
                                }
                                else
                                {
                                    readText = "未指定";
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 瀏覽次數

                        case "view":

                            /*  if (thisGuid != null && thisGuid != "")
                              {
                                  int record_log_qty = DB.record_log.Where(m => m.from == tables).Where(m => m.from_guid == thisGuid).Where(m => m.types == "view").Count();
                                  string readText = "<div class=\"text-center\">"+ record_log_qty.ToString() + "</div>";
                                  rowData.Add(dataItem.Key.ToString(), readText);
                              }*/
                            break;

                        #endregion

                        #region 日期

                        case "date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期

                        case "startdate":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 發布日期2

                        case "create_date":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string dates = "";
                                if (dataList[dataItem.Key.ToString()].ToString() != "")
                                {
                                    dates = DateTime.Parse(dataList[dataItem.Key.ToString()].ToString()).ToString("yyyy-MM-dd");
                                }
                                string readText = "<div class=\"text-center\">" + dates + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 狀態

                        case "status":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/status.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "啟用";
                                string status = dataList[dataItem.Key.ToString()].ToString();

                                if (status == "N")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);
                                readText = readText.Replace("{$status}", status);
                                readText = readText.Replace("{$guid}", thisGuid);
                                if (tables == "user" || tables == "project_users")
                                {
                                    readText = readText.Replace("{$title}", dataList["name"].ToString());
                                }
                                else if (tables == "data_category")
                                {
                                    readText = readText.Replace("{$title}", dataList["itemName"].ToString());
                                }
                                else
                                {
                                    readText = readText.Replace("{$title}", dataList["title"].ToString());
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 討論區狀態

                        case "forum_status":

                            if (dataList["status"] != null && dataList["status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "待分派";
                                string status = dataList["status"].ToString();

                                if (status == "disable")
                                {
                                    style = "secondary";
                                    statusSubject = "停用";
                                }
                                if (status == "stagnate")
                                {
                                    style = "dark";
                                    statusSubject = "後續評估";
                                }
                                if (status == "success")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }
                                if (status == "warning")
                                {
                                    style = "warning";
                                    statusSubject = "處理中";
                                }
                                if (status == "end")
                                {
                                    style = "dark";
                                    statusSubject = "已解決";
                                }
                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 回覆狀態

                        case "re_status":

                            if (dataList["re_status"] != null && dataList["re_status"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "danger";
                                string statusSubject = "尚未回覆";
                                string status = dataList["re_status"].ToString();

                                if (status == "Y")
                                {
                                    style = "success";
                                    statusSubject = "已回覆";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            else
                            {
                                rowData.Add(dataItem.Key.ToString(), "");
                            }
                            break;

                        #endregion

                        #region 角色

                        case "role":

                            if (dataList["role"] != null && dataList["role"].ToString() != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/statusNoClick.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                string style = "primary";
                                string statusSubject = "回覆者";
                                string status = dataList["role"].ToString();

                                if (status == "Tongren")
                                {
                                    style = "secondary";
                                    statusSubject = "同仁";
                                }
                                if (status == "Asker")
                                {
                                    style = "danger";
                                    statusSubject = "提問者";
                                }

                                readText = readText.Replace("{$style}", style);
                                readText = readText.Replace("{$statusSubject}", statusSubject);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 屬性

                        case "type":

                            if (dataList[dataItem.Key.ToString()] != null && dataList[dataItem.Key.ToString()].ToString() != "")
                            {
                                string readText = "";
                                if (tables == "holiday")
                                {
                                    string type = "休假";
                                    if (dataList[dataItem.Key.ToString()].ToString() == "duty")
                                    {
                                        type = "補上班";
                                    }
                                    readText = type;
                                }

                                readText = "<div class=\"\">" + readText + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 排序

                        case "sortIndex":

                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/sortIndex.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                readText = readText.Replace("{$value}", dataList[dataItem.Key.ToString()].ToString());
                                readText = readText.Replace("{$guid}", thisGuid);

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 成員數

                        case "user_qty":
                            if (dataList["guid"] != null)
                            {
                                string role_guid = dataList["guid"].ToString();
                                var data = DB.user.Where(m => m.role_guid.Contains(role_guid)).Where(m => m.username != "sysadmin").Count();
                                string readText = "<div>" + data + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        #region 來源資料表名稱

                        case "tables":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string fromTable = dataList[dataItem.Key.ToString()].ToString();
                                var data = DB.system_menu.Where(m => m.tables == fromTable).Select(a => new { title = a.title }).FirstOrDefault();
                                rowData.Add(dataItem.Key.ToString(), data.title);
                            }
                            break;

                        #endregion

                        #region 動作

                        case "action":

                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/action.cshtml");
                                string readText = System.IO.File.ReadAllText(path);

                                readText = readText.Replace("{$guid}", thisGuid);

                                //是否瀏覽
                                if (tables == "query_custome")
                                {
                                    readText = readText.Replace("{$viewHide}", "");
                                    readText = readText.Replace("{$viewUrl}", urlRoot + "/siteadmin/Report?bid=" + dataList["barcode"].ToString());

                                    readText = readText.Replace("{$printHide}", "");
                                    readText = readText.Replace("{$printUrl}", urlRoot + "/siteadmin/siteadminPrint?bid=" + dataList["barcode"].ToString());

                                    readText = readText.Replace("{$downloadHide}", "");
                                    readText = readText.Replace("{$downloadUrl}", urlRoot + "/siteadmin/Print?bid=" + dataList["barcode"].ToString() + "&userid=" + dataList["user_id"].ToString());
                                }
                                else
                                {
                                    readText = readText.Replace("{$viewHide}", "style=\"display:none;\"");
                                    readText = readText.Replace("{$printHide}", "style=\"display:none;\"");
                                    readText = readText.Replace("{$downloadHide}", "style=\"display:none;\"");
                                }

                                //是否修改
                                if (system_menu.can_edit == "Y")
                                {
                                    readText = readText.Replace("{$editHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$editHide}", "style=\"display:none;\"");
                                }
                                //是否可刪除
                                dynamic permissions = context.Session["permissions"];//取得權限紀錄
                                if (system_menu.can_del == "Y" && permissions[system_menu.guid] == "F")
                                {
                                    readText = readText.Replace("{$delHide}", "");
                                }
                                else
                                {
                                    readText = readText.Replace("{$delHide}", "style=\"display:none;\"");
                                }

                                //還原(限定資源回收桶)
                                if (tables != "ashcan")
                                {
                                    readText = readText.Replace("{$reHide}", "style=\"display:none;\"");
                                }
                                else
                                {
                                    readText = readText.Replace("{$reHide}", "");
                                }

                                string addId = "";
                                //dataTableCategory
                                if (context.Session["dataTableCategory"] != null)
                                {
                                    addId = "?c=" + context.Session["dataTableCategory"].ToString();
                                }

                                //連結
                                readText = readText.Replace("{$editUrl}", urlRoot + "/siteadmin/" + tables + "/edit/" + thisGuid + addId);

                                if (system_menu.index_view_url != null && system_menu.index_view_url.ToString() != "")
                                {
                                    readText = readText.Replace("{$index_view_url}", "<a href=\"" + urlRoot + system_menu.index_view_url.ToString() + thisGuid + "\" target=\"_blank\" class=\"btn btn-outline-default btn-sm\" role=\"button\" aria-pressed=\"true\" title=\"瀏覽前端\"><i class=\"icon-eye3\"></i><span class=\"text-hide\">瀏覽</span></a>");
                                }
                                else
                                {
                                    readText = readText.Replace("{$index_view_url}", "");
                                }

                                readText = readText.Replace("{$title}", tableTitleKey(tables, dataList));

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }

                            break;

                        #endregion

                        #region 按鈕

                        case "button":
                            if (thisGuid != null && thisGuid != "")
                            {
                                string path = System.Web.HttpContext.Current.Server.MapPath("~/Views/Siteadmin/DataTable/button.cshtml");
                                string readText = System.IO.File.ReadAllText(path);
                                switch (tables)
                                {
                                    case "cases_city":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/cases_city_history/list/" + thisGuid);
                                        break;

                                    case "progress_cases":
                                        readText = readText.Replace("{$url}", urlRoot + "/siteadmin/progress_info/list/" + thisGuid);
                                        break;
                                }

                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;

                        #endregion

                        default:
                            if (dataList[dataItem.Key.ToString()] != null)
                            {
                                string readText = "<div >" + dataList[dataItem.Key.ToString()].ToString() + "</div>";
                                rowData.Add(dataItem.Key.ToString(), readText);
                            }
                            break;
                    }
                }

                re.Add(rowData);
            }

            return re;
        }

        /// <summary>
        /// 是否使用語系
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string useLang(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    re = queryCustomeRepository.useLang();
                    break;

                #endregion

                case "data_category":
                    re = dataCategoryRepository.useLang();
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    re = systemLogRepository.useLang();
                    break;

                //防火牆
                case "firewalls":
                    re = firewallsRepository.useLang();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.useLang();
                    break;

                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.useLang();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.useLang();
                    break;
                //使用者
                case "user":
                    re = userRepository.useLang();
                    break;

                //網站基本資料
                case "web_data":
                    re = webDataRepository.useLang();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.useLang();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.useLang();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得DataTable欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic dataTableTitle(string tables)
        {
            dynamic re = null;

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    re = queryCustomeRepository.dataTableTitle();
                    break;

                #endregion

                case "data_category":
                    re = dataCategoryRepository.dataTableTitle();
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    re = systemLogRepository.dataTableTitle();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.dataTableTitle();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.dataTableTitle();
                    break;

                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.dataTableTitle();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.dataTableTitle();
                    break;

                //使用者
                case "user":
                    re = userRepository.dataTableTitle();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.dataTableTitle();
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得列表說明
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic listMessage(string tables)
        {
            dynamic re = null;
            switch (tables)
            {
                //防火牆
                case "firewalls":
                    re = firewallsRepository.listMessage();
                    break;

                default:
                    re = "";
                    break;
            }

            return re;
        }

        /// <summary>
        /// 欄位資料
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getColData(string tables, dynamic data, string PageType)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    Dictionary<string, List<string>> ImaDataItemno = new Dictionary<string, List<string>>();
                    if (PageType == "edit")
                    {
                        DataService Data = new DataService();
                        var b = data["barcode"];
                        var barcode = b.Value.ToString();
                        ImaDataItemno = Data.GetImagingDataitemNo("tw", barcode);
                    }

                    re = queryCustomeRepository.colFrom(ImaDataItemno);
                    break;

                #endregion

                case "data_category":
                    re = dataCategoryRepository.colFrom();
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    re = systemLogRepository.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.colFrom();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.colFrom();
                    break;

                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.colFrom();
                    break;
                //使用者
                case "user":
                    re = userRepository.colFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = webDataRepository.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = systemDataRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.colFrom();
                    break;

                    #endregion
            }

            return re;
        }
        public static dynamic getColData2(string tables, dynamic data, string PageType)
        {
            dynamic re = null;
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    Dictionary<string, List<string>> ImaDataItemno = new Dictionary<string, List<string>>();
                    if (PageType == "edit")
                    {
                        DataService Data = new DataService();
                        var b = data["barcode"];
                        var barcode = b.Value.ToString();
                        ImaDataItemno = Data.GetImagingDataitemNo("tw", barcode);
                    }

                    re = ImaDataItemno;
                    break;

                #endregion

                case "data_category":
                    re = dataCategoryRepository.colFrom();
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    re = systemLogRepository.colFrom();
                    break;
                //防火牆
                case "firewalls":
                    re = firewallsRepository.colFrom();
                    break;

                //最新消息
                case "news":
                    re = newsRepository.colFrom();
                    break;

                //最新消息分類
                case "news_category":
                    re = newsCategoryRepository.colFrom();
                    break;

                //群組管理
                case "roles":
                    re = rolesRepository.colFrom();
                    break;
                //使用者
                case "user":
                    re = userRepository.colFrom();
                    break;

                //網站基本資料
                case "web_data":
                    re = webDataRepository.colFrom();
                    break;
                //SMTP資料
                case "smtp_data":
                    re = smtpDataRepository.colFrom();
                    break;

                //系統參數
                case "system_data":
                    re = systemDataRepository.colFrom();
                    break;

                //資源回收桶
                case "ashcan":
                    re = ashcanRepository.colFrom();
                    break;

                    #endregion
            }

            return re;
        }


        /// <summary>
        /// 取得資料庫資料
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getPresetData(string tables, string guid)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            string re = "";

            Dictionary<String, Object> reValData = new Dictionary<String, Object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<query_custome>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #endregion

                case "data_category":
                    if (!string.IsNullOrEmpty(guid))
                    {
                        var data = model.Repository<data_category>();
                        var tempData = data.ReadsWhere(x => x.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_log>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //防火牆
                case "firewalls":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<firewalls>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;
                //聯絡我們
                case "contacts":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<contacts>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息
                case "news":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);

                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //最新消息分類
                case "news_category":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<news_category>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //群組管理
                case "roles":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<roles>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //使用者
                case "user":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<user>();
                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }

                    break;

                //網站基本資料
                case "web_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<web_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //系統參數
                case "system_data":

                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<system_data>();
                        Guid newGuid = Guid.Parse(guid);
                        var tempData = data.ReadsWhere(m => m.guid == newGuid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    if (guid != null && guid != "")
                    {
                        var data = model.Repository<smtp_data>();

                        var tempData = data.ReadsWhere(m => m.guid == guid);
                        re = JsonConvert.SerializeObject(tempData, Formatting.Indented);
                    }
                    break;

                    #endregion
            }

            return re;
        }

        /// <summary>
        /// 取得分類
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static dynamic getCategory(string tables, dynamic data = null)
        {
            Model DB = new Model();
            dynamic re = null;
            string defLang = ConfigurationManager.ConnectionStrings["defaultLanguage"].ConnectionString;//預設語系

            switch (tables)
            {
                //最新消息分類
                case "news_category":
                    if (tables != null && tables != "")
                    {
                        re = DB.news_category.Where(m => m.status == "Y").Where(m => m.lang == defLang).ToList();
                    }
                    break;
                //群組
                case "roles":
                    if (tables != null && tables != "")
                    {
                        re = DB.roles.Where(m => m.status == "Y").ToList();
                    }
                    break;

                //使用者
                case "user":
                    if (tables != null && tables != "")
                    {
                        re = DB.user.Where(m => m.status == "Y").ToList();
                    }
                    break;
            }

            return re;
        }

        /// <summary>
        /// 回傳標題
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="guid"></param>
        /// <returns></returns>
        public static string getDataTitle(string tables, string guid)
        {
            Model DB = new Model();
            string re = "";
            /* switch (tables)
            {
            }*/

            return re;
        }

        /// <summary>
        /// 複選選單選取判斷用
        /// </summary>
        /// <param name="tabels"></param>
        /// <param name="user_guid"></param>
        /// <param name="forum_guid"></param>
        /// <returns></returns>
        public static string selectMultipleSelected(string tabels, string user_guid, string forum_guid)
        {
            Model DB = new Model();

            string selected = "";
            /*
            switch(tabels)
            {
                case "forum_message":

                    int data = DB.forum_message.Where(m=>m.user_guid == user_guid).Where(m=>m.forum_guid == forum_guid).Select(a => new { guid = a.guid }).Count();
                    if(data > 0)
                    {
                        selected = " selected";
                    }
                    break;
            }
            */

            return selected;
        }

        /// <summary>
        /// 新增 修改
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="form"></param>
        /// <param name="Field"></param>
        /// <returns></returns>
        public static string saveData(string tables, string form, string Field, string guid, string actType, Dictionary<String, Object> FormObj = null)
        {
            Model DB = new Model();
            EFUnitOfWork model = new EFUnitOfWork(DB);
            dynamic Model = null;//目前Model
            dynamic FromData = null;//表單資訊
            string lang = "";

            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":

                    var picJson = "";
                    var picaltJson = "";
                    if (actType != "add")
                    {
                        var pic = JsonConvert.DeserializeObject<pic_ViewModel>(form);
                        picJson = JsonConvert.SerializeObject(pic);

                        var picalt = JsonConvert.DeserializeObject<picalt_ViewModel>(form);
                        picaltJson = JsonConvert.SerializeObject(picalt);
                    }

                    FromData = JsonConvert.DeserializeObject<query_custome>(form);
                    if (actType != "add")
                    {
                        FromData.pic = picJson;
                        FromData.pic_alt = picaltJson;
                    }
                    Model = model.Repository<query_custome>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.query_custome.Where(x => x.guid == guid).FirstOrDefault();
                    }
                    break;

                #endregion

                case "data_category":
                    FromData = JsonConvert.DeserializeObject<data_category>(form);
                    Model = model.Repository<data_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.data_category.Where(x => x.guid == guid).FirstOrDefault();
                    }
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":

                    FromData = JsonConvert.DeserializeObject<system_log>(form);
                    Model = model.Repository<system_log>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.system_log.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //防火牆
                case "firewalls":

                    FromData = JsonConvert.DeserializeObject<firewalls>(form);
                    Model = model.Repository<firewalls>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.firewalls.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;
                //最新消息
                case "news":
                    FromData = JsonConvert.DeserializeObject<news>(form);
                    Model = model.Repository<news>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }
                    break;
                //最新消息分類
                case "news_category":

                    FromData = JsonConvert.DeserializeObject<news_category>(form);
                    Model = model.Repository<news_category>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        lang = FromData.lang;
                        Model = DB.news_category.Where(m => m.guid == guid).Where(m => m.lang == lang).FirstOrDefault();
                    }

                    break;
                //群組管理
                case "roles":

                    FromData = JsonConvert.DeserializeObject<roles>(form);
                    Model = model.Repository<roles>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.roles.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //網站基本資料
                case "web_data":
                    FromData = JsonConvert.DeserializeObject<web_data>(form);
                    Model = model.Repository<web_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.web_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //SMTP資料
                case "smtp_data":
                    FromData = JsonConvert.DeserializeObject<smtp_data>(form);
                    Model = model.Repository<smtp_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.smtp_data.Where(m => m.guid == guid).FirstOrDefault();
                    }
                    break;

                //資源回收桶
                case "ashcan":

                    FromData = JsonConvert.DeserializeObject<ashcan>(form);
                    Model = model.Repository<ashcan>();
                    if (Field != "")
                    {
                        Guid NewGuid = Guid.Parse(FromData.guid.ToString());
                        Model = DB.ashcan.Where(m => m.guid == NewGuid).FirstOrDefault();
                    }

                    break;
                //使用者
                case "user":

                    FromData = JsonConvert.DeserializeObject<user>(form);
                    Model = model.Repository<user>();

                    if (FromData.password != null && FromData.password != "")
                    {
                        FromData.password = FunctionService.md5(FromData.password);
                    }
                    //無輸入密碼進入修改情況下
                    if (FromData.password == "" && FromData.guid != "")
                    {
                        FromData.password = FormObj["defPassword"].ToString();
                    }

                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Model = DB.user.Where(m => m.guid == guid).FirstOrDefault();
                    }

                    break;

                //系統參數
                case "system_data":

                    FromData = JsonConvert.DeserializeObject<system_data>(form);
                    Model = model.Repository<system_data>();
                    if (Field != "")
                    {
                        guid = FromData.guid;
                        Guid newGuid = Guid.Parse(guid);
                        Model = DB.system_data.Where(m => m.guid == newGuid).FirstOrDefault();
                    }

                    break;

                    #endregion
            }

            FromData.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

            if (actType == "add")
            {
                FromData.guid = guid;
                FromData.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Model.Create(FromData);
                Model.SaveChanges();
            }
            else
            {
                if (Field == "")
                {
                    Model.Update(FromData);
                    Model.SaveChanges();
                }
                else
                {
                    //單一欄位修改
                    switch (Field)
                    {
                        case "status":
                            Model.status = FromData.status;

                            //修改其餘相關資料表的狀態
                            if (tables == "query_custome")
                            {
                                AssessTableService AssessTable = new AssessTableService();
                                GaugeDataService GaugeData = new GaugeDataService();
                                LabDataService LabData = new LabDataService();

                                AssessTable.ChangeStatusByBarcode(Model.guid, Model.barcode, Model.status);
                                GaugeData.ChangeStatusByBarcode(Model.guid, Model.barcode, Model.status);
                                LabData.ChangeStatusByBarcode(Model.guid, Model.barcode, Model.status);
                            }
                            break;

                        case "sortIndex":
                            Model.sortIndex = FromData.sortIndex;
                            break;

                        case "logindate":
                            Model.logindate = FromData.logindate;
                            break;
                    }

                    DB.SaveChanges();
                }
            }

            if (FormObj != null)
            {
                #region 權限

                if (FormObj.ContainsKey("permissions") && FormObj["permissions"] != null && FormObj["permissions"].ToString() != "")
                {
                    NameValueCollection permissions = FunctionService.reSubmitFormDataJson(FormObj["permissions"].ToString());

                    role_permissions role_permissions_add = new role_permissions();

                    var delData = DB.role_permissions.Where(m => m.role_guid == guid).ToList();

                    foreach (var item in delData)
                    {
                        DB.role_permissions.Remove((role_permissions)item);
                    }

                    foreach (string key in permissions)
                    {
                        //Guid newGuid = Guid.NewGuid();
                        role_permissions_add = new role_permissions();
                        role_permissions_add.guid = Guid.NewGuid();
                        role_permissions_add.role_guid = guid;
                        role_permissions_add.permissions_guid = key.ToString();
                        role_permissions_add.permissions_status = permissions[key].ToString();
                        DB.role_permissions.Add(role_permissions_add);
                        DB.SaveChanges();
                    }
                }

                #endregion
            }

            return guid;
        }

        /// <summary>
        /// 取得預設排序
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static Dictionary<String, Object> defaultOrderBy(string tables)
        {
            Dictionary<String, Object> re = new Dictionary<string, object>();
            //>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>設定區<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
            switch (tables)
            {
                #region -- 內容管理 --

                //報告書列表清單
                case "query_custome":
                    re.Clear();
                    re = queryCustomeRepository.defaultOrderBy();
                    break;

                #endregion

                case "data_category":
                    re = dataCategoryRepository.defaultOrderBy();
                    break;

                #region -- Defalut --

                //系統日誌
                case "system_log":
                    re.Clear();
                    re = systemLogRepository.defaultOrderBy();
                    break;
                //防火牆
                case "firewalls":
                    re.Clear();
                    re = firewallsRepository.defaultOrderBy();
                    break;

                    #endregion
            }
            return re;
        }

        /// <summary>
        /// 格式化排序欄位
        /// </summary>
        /// <param name="tables"></param>
        /// <param name="orderByKey"></param>
        /// <returns></returns>
        public static string formatOrderByKey(string tables, string orderByKey)
        {
            string re = "";
            switch (orderByKey)
            {
                case "info":
                    re = "title";
                    break;

                case "view_info":
                    re = "nums";
                    break;

                case "forum_status":
                    re = "status";
                    break;

                case "action":
                    re = "guid";
                    break;

                case "user":
                    re = "user_guid";
                    break;

                case "category":
                    re = "category";
                    if (tables == "package_content")
                    {
                        re = "package_guid";
                    }
                    break;

                default:
                    re = orderByKey;
                    break;
            }
            return re;
        }

        /// <summary>
        /// 回傳資料表的標題或姓名
        /// </summary>
        /// <param name="tables"></param>
        /// <returns></returns>
        public static string tableTitleKey(string tables, dynamic dataList)
        {
            Model DB = new Model();
            string re = "";
            switch (tables)
            {
                case "user":
                    re = dataList["name"];
                    break;

                default:
                    re = dataList["title"];
                    break;
            }
            return re;
        }
    }
}