﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.IO;
using System.Security.Cryptography.X509Certificates;
using Google.Apis.Auth.OAuth2;
using Google.Apis.Analytics.v3;
using System.Threading;
using System.Web.Mvc;
using Google.Apis.Services;
using System.Configuration;
using Web.Models;
using Newtonsoft.Json;

namespace Web.Service
{
    public class GoogleAnalyticsService : Controller
    {
        public string GaProfileId = ConfigurationManager.ConnectionStrings["GaProfileId"].ConnectionString;
        private MornjoyContext DB = new MornjoyContext();
        //GaProfileId

        public object TasksService { get; private set; }

        /// <summary>
        /// 年齡層
        /// </summary>
        /// <returns></returns>
        public string Age(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API Age",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, "30daysAgo", "today", "ga:sessions");
                searches.Dimensions = "ga:userAgeBracket";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                List<Object> dic = new List<Object>();
                if (GaData.Rows != null)
                {
                    foreach (var item in GaData.Rows)
                    {
                        Dictionary<String, Object> dic2 = new Dictionary<string, object>();
                        dic2.Add("age", item[0]);
                        dic2.Add("visits", item[1]);
                        dic.Add(dic2);
                    }
                }

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 取得登入憑證
        /// </summary>
        /// <returns></returns>
        public ServiceAccountCredential GoogleAnalytics()
        {
            string[] scopes = new string[] {
                AnalyticsService.Scope.Analytics,               // view and manage your Google Analytics data
                AnalyticsService.Scope.AnalyticsEdit,           // Edit and manage Google Analytics Account
                AnalyticsService.Scope.AnalyticsManageUsers,    // Edit and manage Google Analytics Users
                AnalyticsService.Scope.AnalyticsReadonly};      // View Google Analytics Data

            string serverPath = System.Web.Hosting.HostingEnvironment.MapPath("~/");
            var keyFilePath = serverPath + "Content/Siteadmin/API Project-8e05910ca2e2.p12";
            var serviceAccountEmail = "ga-130@api-project-155771781585.e-creative.tw.iam.gserviceaccount.com";
            var certificate = new X509Certificate2(keyFilePath, "notasecret", X509KeyStorageFlags.MachineKeySet | X509KeyStorageFlags.PersistKeySet | X509KeyStorageFlags.Exportable);
            var credential = new ServiceAccountCredential(new ServiceAccountCredential.Initializer(serviceAccountEmail)
            {
                Scopes = scopes
            }.FromCertificate(certificate));

            //取得token
            string AuthenticationKey = "";
            if (credential.RequestAccessTokenAsync(CancellationToken.None).Result)
            {
                AuthenticationKey = credential.Token.AccessToken;
            }
            return credential;
        }

        /// <summary>
        /// 熱門頁面
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string Hotpages(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API HotPages",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest page = service.Data.Ga.Get(profileId, "30daysAgo", "today", "ga:pageviews,ga:avgTimeOnPage");
                page.Dimensions = "ga:pagePath";
                page.Sort = "-ga:pageviews";
                Google.Apis.Analytics.v3.Data.GaData GaData = page.Execute();

                Dictionary<int, List<string>> dic = new Dictionary<int, List<string>>();
                var num = 1;
                foreach (var item in GaData.Rows.Take(5))
                {
                    List<string> value = new List<string>();
                    value.Add(item[0]);
                    value.Add(item[1]);

                    var seconds = float.Parse(item[2]);
                    var timers = Convert.ToInt32(seconds) / 60;
                    var time = "";
                    if (timers >= 1)
                    {
                        time = "00:" + timers.ToString().PadLeft(2, '0') + ":" + (Convert.ToInt32(seconds) - (timers * 60));
                    }
                    else
                    {
                        time = "00:00:" + Convert.ToInt32(seconds);
                    }

                    value.Add(time);
                    dic.Add(num, value);
                    num++;
                }

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 熱門關鍵字
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string Keyword(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API Keyword",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, "30daysAgo", "today", "ga:users");
                searches.Dimensions = "ga:keyword";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                Dictionary<string, string> dic = new Dictionary<string, string>();
                foreach (var item in GaData.Rows)
                {
                    dic.Add(item[0], item[1]);
                }

                //針對次數排序(多>少)
                dic = dic.OrderByDescending(x => x.Value).ToDictionary(x => x.Key, x => x.Value);

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 線上使用者/單次頁數/停留時間/跳出率
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string NowUser(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "RealtimeData",
                });
                string profileId = GaProfileId;

                //線上使用者
                DataResource.RealtimeResource.GetRequest pageviews = service.Data.Realtime.Get(profileId, "rt:pageviews");
                Google.Apis.Analytics.v3.Data.RealtimeData realtimeData = pageviews.Execute();
                var nowuser = realtimeData.TotalsForAllResults["rt:pageviews"].ToString();

                DataResource.GaResource.GetRequest pae = service.Data.Ga.Get(profileId, "30daysAgo", "today", "ga:pageviewsPerSession,ga:avgTimeOnPage,ga:exitRate");
                Google.Apis.Analytics.v3.Data.GaData result = pae.Execute();

                //單次頁數
                var pageviewspersession = float.Parse(result.TotalsForAllResults["ga:pageviewsPerSession"]).ToString("F2");

                //停留時間
                var avg = float.Parse(result.TotalsForAllResults["ga:avgTimeOnPage"]);
                var timers = Convert.ToInt32(avg) / 60;
                var avgtimeonpage = "";
                if (timers >= 1)
                {
                    avgtimeonpage = "00:" + timers.ToString().PadLeft(2, '0') + ":" + (Convert.ToInt32(avg) - (timers * 60));
                }
                else
                {
                    avgtimeonpage = "00:00:" + Convert.ToInt32(avg);
                }

                //跳出率
                var exitrate = float.Parse(result.TotalsForAllResults["ga:exitRate"]).ToString("F2") + "%";

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("nowUser", nowuser);
                dic.Add("pageviewsPerSession", pageviewspersession);
                dic.Add("avgTimeOnPage", avgtimeonpage);
                dic.Add("exitRate", exitrate);

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 流量來源
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string OrganicSearches(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API medium",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, "30daysAgo", "today", "ga:sessions");
                searches.Dimensions = "ga:medium";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                int organic = 0;
                int direct = 0;
                int referral = 0;
                foreach (var item in GaData.Rows)
                {
                    //搜尋
                    if (item[0].IndexOf("organic") != -1)
                    {
                        organic = organic + int.Parse(item[1].ToString());
                    }
                    //直接
                    else if (item[0].IndexOf("direct") != -1)
                    {
                        direct = direct + int.Parse(item[1].ToString());
                    }
                    //推薦
                    else if (item[0].IndexOf("referral") != -1)
                    {
                        referral = referral + int.Parse(item[1].ToString());
                    }
                }

                //百分比
                int totalTraffic = organic + direct + referral;
                double organicPer = Math.Round((float.Parse(organic.ToString()) / totalTraffic * 100), 1);
                double directPer = Math.Round((float.Parse(direct.ToString()) / totalTraffic * 100), 1);
                double referralPer = Math.Round((float.Parse(referral.ToString()) / totalTraffic * 100), 1);

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("organic", organic.ToString());
                dic.Add("direct", direct.ToString());
                dic.Add("referral", referral.ToString());
                dic.Add("organicPer", organicPer.ToString());
                dic.Add("directPer", directPer.ToString());
                dic.Add("referralPer", referralPer.ToString());

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 流量來源(波浪圖)
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string OrganicSearchesList(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API medium List",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, "7daysAgo", "today", "ga:sessions");
                searches.Dimensions = "ga:medium,ga:date";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                Dictionary<string, int> dayData = new Dictionary<string, int>();
                foreach (var item in GaData.Rows)
                {
                    if (item[0].IndexOf("organic") != -1)
                    {
                        if (dayData.ContainsKey(item[1].ToString() + "_organic"))
                        {
                            dayData[item[1].ToString() + "_organic"] = dayData[item[1].ToString() + "_organic"] + int.Parse(item[2].ToString());
                        }
                        else
                        {
                            dayData.Add(item[1].ToString() + "_organic", int.Parse(item[2].ToString()));
                        }
                    }
                    else if (item[0].IndexOf("direct") != -1)
                    {
                        if (dayData.ContainsKey(item[1].ToString() + "_direct"))
                        {
                            dayData[item[1].ToString() + "_direct"] = dayData[item[1].ToString() + "_organic"] + int.Parse(item[2].ToString());
                        }
                        else
                        {
                            dayData.Add(item[1].ToString() + "_organic", int.Parse(item[2].ToString()));
                        }
                    }
                    else if (item[0].IndexOf("referral") != -1)
                    {
                        if (dayData.ContainsKey(item[1].ToString() + "_referral"))
                        {
                            dayData[item[1].ToString() + "_referral"] = dayData[item[1].ToString() + "_referral"] + int.Parse(item[2].ToString());
                        }
                        else
                        {
                            dayData.Add(item[1].ToString() + "_referral", int.Parse(item[2].ToString()));
                        }
                    }
                }

                string traffic_organic = "";
                string traffic_direct = "";
                string traffic_referral = "";

                foreach (var dayDataitem in dayData)
                {
                    DateTime NewDate = DateTime.ParseExact(dayDataitem.Key.Replace("_", "").Replace("organic", "").Replace("direct", "").Replace("referral", ""), "yyyyMMdd", null, System.Globalization.DateTimeStyles.AllowWhiteSpaces);

                    //搜尋
                    if (dayDataitem.Key.IndexOf("_organic") != -1)
                    {
                        traffic_organic += "{\"date\": \"" + NewDate.ToString("yyyy-MM-dd") + "T16:00:00.00Z\",\"value\": " + dayDataitem.Value + "},";
                    }
                    else if (dayDataitem.Key.IndexOf("_direct") != -1)
                    {
                        traffic_direct += "{\"date\": \"" + NewDate.ToString("yyyy-MM-dd") + "T16:00:00.00Z\",\"value\": " + dayDataitem.Value + "},";
                    }
                    else if (dayDataitem.Key.IndexOf("_referral") != -1)
                    {
                        traffic_referral += "{\"date\": \"" + NewDate.ToString("yyyy-MM-dd") + "T16:00:00.00Z\",\"value\": " + dayDataitem.Value + "},";
                    }
                }

                if (!string.IsNullOrEmpty(traffic_organic))
                {
                    traffic_organic = "[" + traffic_organic.Substring(0, traffic_organic.Length - 1) + "]";
                }
                if (!string.IsNullOrEmpty(traffic_direct))
                {
                    traffic_direct = "[" + traffic_direct.Substring(0, traffic_direct.Length - 1) + "]";
                }
                if (!string.IsNullOrEmpty(traffic_referral))
                {
                    traffic_referral = "[" + traffic_referral.Substring(0, traffic_referral.Length - 1) + "]";
                }

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("traffic_organic", traffic_organic);
                dic.Add("traffic_direct", traffic_direct);
                dic.Add("traffic_referral", traffic_referral);

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 寫入瀏覽器使用
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ThisBrowser(ServiceAccountCredential credential)
        {
            var service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Browser",
            });

            string profileId = GaProfileId;
            string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
            string metrics = "ga:sessions";
            DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
            request.Dimensions = "ga:browser";
            Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

            return GaData;
        }

        /// <summary>
        /// 寫入年度數量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public Google.Apis.Analytics.v3.Data.GaData ThisYear(ServiceAccountCredential credential)
        {
            var service = new AnalyticsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Analytics API Year",
            });

            string profileId = GaProfileId;
            string startDate = DateTime.Now.ToString("yyyy") + "-01-01";
            string endDate = DateTime.Now.ToString("yyyy") + "-12-31";
            string metrics = "ga:visits";
            DataResource.GaResource.GetRequest request = service.Data.Ga.Get(profileId, startDate, endDate, metrics);
            request.Dimensions = "ga:month";
            Google.Apis.Analytics.v3.Data.GaData GaData = request.Execute();

            return GaData;
        }

        /// <summary>
        /// 今日參觀
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string TodayOrganicSearches(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API medium List",
                });
                string profileId = GaProfileId;

                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, "today", "today", "ga:sessions");
                searches.Dimensions = "ga:medium";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("TodayOrganicSearches", GaData.TotalsForAllResults["ga:sessions"].ToString());
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        public string TotalContact()
        {
            try
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 總流量
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string TotalOrganicSearches(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API medium List",
                });
                string profileId = GaProfileId;

                var date = DateTime.Now.AddYears(-10).ToString("yyyy-MM-dd");
                DataResource.GaResource.GetRequest searches = service.Data.Ga.Get(profileId, date, "today", "ga:sessions");
                searches.Dimensions = "ga:medium";
                Google.Apis.Analytics.v3.Data.GaData GaData = searches.Execute();

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("TotalOrganicSearches", GaData.TotalsForAllResults["ga:sessions"].ToString());
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 總瀏覽次數
        /// </summary>
        /// <param name="credential"></param>
        /// <returns></returns>
        public string TotalPathViews(ServiceAccountCredential credential)
        {
            try
            {
                var service = new AnalyticsService(new BaseClientService.Initializer()
                {
                    HttpClientInitializer = credential,
                    ApplicationName = "Analytics API HotPages",
                });
                string profileId = GaProfileId;

                var date = DateTime.Now.AddYears(-10).ToString("yyyy-MM-dd");
                DataResource.GaResource.GetRequest page = service.Data.Ga.Get(profileId, date, "today", "ga:pageviews");
                page.Dimensions = "ga:pagePath";
                Google.Apis.Analytics.v3.Data.GaData GaData = page.Execute();

                Dictionary<string, string> dic = new Dictionary<string, string>();
                dic.Add("PathViews", GaData.TotalsForAllResults["ga:pageviews"].ToString());
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 瀏覽器使用率
        /// </summary>
        /// <returns></returns>
        public string ViewBrowser()
        {
            try
            {
                List<object> dic = new List<object>();
                google_analytics GAThisBrowser = DB.google_analytics.Where(x => x.area == "ThisBrowser").FirstOrDefault();
                if (GAThisBrowser != null && !string.IsNullOrEmpty(GAThisBrowser.content))
                {
                    var mJObj = Newtonsoft.Json.Linq.JArray.Parse(GAThisBrowser.content.ToString());
                    var total = 0;
                    Dictionary<string, int> num = new Dictionary<string, int>();

                    for (int i = 0; i < mJObj.Count; i++)
                    {
                        var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());
                        switch (subObj[0].ToString())
                        {
                            case "Chrome":
                                num.Add("GoogleChrome", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Firefox":
                                num.Add("MozilaFirefox", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Internet Explorer":
                                num.Add("InterentExplorer", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;

                            case "Safari":
                                num.Add("Safari", Int32.Parse(subObj[1].ToString()));
                                total += Int32.Parse(subObj[1].ToString());
                                break;
                        }
                    }

                    //百分比
                    List<string> list = new List<string>();
                    list.AddRange(num.Keys);
                    foreach (var item in list)
                    {
                        Dictionary<String, object> dic2 = new Dictionary<string, object>();
                        float n = num[item];
                        num[item] = Int32.Parse(((n / total) * 100).ToString("N0"));
                        dic2.Add(item, num[item]);
                        dic.Add(dic2);
                    }
                }

                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 回傳年度數量
        /// </summary>
        /// <returns></returns>
        public string ViewYear()
        {
            try
            {
                List<Object> dic = new List<Object>();
                google_analytics GAThisYear = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();
                if (GAThisYear != null && !string.IsNullOrEmpty(GAThisYear.content))
                {
                    var mJObj = Newtonsoft.Json.Linq.JArray.Parse(GAThisYear.content.ToString());

                    for (int i = 0; i <= 11; i++)
                    {
                        Dictionary<String, Object> dic2 = new Dictionary<string, object>();

                        var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());

                        dic2.Add("age", (i + 1) + "月");
                        dic2.Add("visits", subObj[1]);
                        dic.Add(dic2);
                    }
                }
                string json = JsonConvert.SerializeObject(dic, Formatting.Indented);
                return json;
            }
            catch (Exception ex)
            {
                string json = JsonConvert.SerializeObject("Error", Formatting.Indented);
                return json;
            }
        }

        /// <summary>
        /// 回傳年度數量(目前月份)
        /// </summary>
        /// <returns></returns>
        public string ViewYearUseMonth(string month)
        {
            List<Object> dic = new List<Object>();

            google_analytics google_analytics = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();

            string visits = "0";
            if (google_analytics != null && !string.IsNullOrEmpty(google_analytics.content))
            {
                var mJObj = Newtonsoft.Json.Linq.JArray.Parse(google_analytics.content.ToString());

                for (int i = 0; i <= 11; i++)
                {
                    var subObj = Newtonsoft.Json.Linq.JArray.Parse(mJObj[i].ToString());
                    if ((i + 1).ToString() == month)
                    {
                        visits = subObj[1].ToString();
                    }
                }
            }
            return visits;
        }
    }
}