﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using Web.Models;
using Web.ViewModel;

namespace Web.Service
{
    public class ApiService
    {
        private MornjoyContext _context = new MornjoyContext();

        //取得體檢人員名單 By 公司名稱 起始日 結束日 姓名
        public List<QueryCustome> GetQueryCustome(string CustomerName, string StartDate, string EndDate, string Name)
        {
            try
            {
                CustomerName = HttpUtility.UrlEncode(CustomerName);
                Name = HttpUtility.UrlEncode(Name);

                var url = "http://211.75.23.69:7777/QueryCustomer?" + CustomerName + "?" + StartDate + "?" + EndDate + "?" + Name;
                var request = WebRequest.Create(url);

                request.Method = "GET";
                request.ContentType = "application/json;charset=UTF-8";

                var response = request.GetResponse() as HttpWebResponse;
                var responseStream = response.GetResponseStream();
                var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                var srcString = reader.ReadToEnd();

                ApiViewModel Model = new ApiViewModel();
                Model.QueryCustome_ListViewModel = JsonConvert.DeserializeObject<List<QueryCustome>>(srcString);

                return Model.QueryCustome_ListViewModel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得身體評估表 By Barcode
        public List<AssessTable> GetAssessTable(List<QueryCustome> Model)
        {
            try
            {
                ApiViewModel ViewModel = new ApiViewModel();
                List<AssessTable> ListViewModel = new List<AssessTable>();
                foreach (var item in Model)
                {
                    var encode = HttpUtility.UrlEncode(item.Barcode);
                    var url = "http://211.75.23.69:7777/GETASSESSTABLE?" + encode;
                    var request = WebRequest.Create(url);

                    request.Method = "GET";
                    request.ContentType = "application/json;charset=UTF-8";

                    var response = request.GetResponse() as HttpWebResponse;
                    var responseStream = response.GetResponseStream();
                    var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    var srcString = reader.ReadToEnd();

                    AssessTable AssessTableViewModel = new AssessTable();
                    AssessTableViewModel = JsonConvert.DeserializeObject<AssessTable>(srcString);
                    AssessTableViewModel.Barcode = item.Barcode;
                    ListViewModel.Add(AssessTableViewModel);
                }

                return ListViewModel;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得生理量測資料 By Barcode
        public List<GaugeData> GetGaugeData(List<QueryCustome> Model)
        {
            try
            {
                ApiViewModel ViewModel = new ApiViewModel();
                List<GaugeData> gaugeDataList = new List<GaugeData>();

                foreach (var item in Model)
                {
                    var encode = HttpUtility.UrlEncode(item.Barcode);
                    var url = "http://211.75.23.69:7777/GETGAUGEDATA?" + encode;
                    var request = WebRequest.Create(url);

                    request.Method = "GET";
                    request.ContentType = "application/json;charset=UTF-8";

                    var response = request.GetResponse() as HttpWebResponse;
                    var responseStream = response.GetResponseStream();
                    var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    var srcString = reader.ReadToEnd();

                    var result = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, dynamic>>>(srcString);

                    foreach (var r in result.Values)
                    {
                        GaugeData gaugedata = new GaugeData();
                        List<GaugeDataitem> gaugeDataitemList = new List<GaugeDataitem>();
                        foreach (var data in r)
                        {
                            switch (data.Key)
                            {
                                case "Barcode":
                                    gaugedata.Barcode = item.Barcode;
                                    gaugedata.Old_Barcode = data.Value;
                                    break;

                                case "CheckDate":
                                    gaugedata.CheckDate = data.Value;
                                    break;

                                default:
                                    if (data.Key.Contains("Item"))
                                    {
                                        GaugeDataitem gaugedataitem = new GaugeDataitem();
                                        gaugedataitem = JsonConvert.DeserializeObject<GaugeDataitem>(data.Value.ToString());
                                        gaugeDataitemList.Add(gaugedataitem);
                                    }
                                    break;
                            }
                        }
                        gaugedata.Item = gaugeDataitemList;
                        gaugeDataList.Add(gaugedata);
                    }
                }
                return gaugeDataList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        //取得檢驗資料 By Barcode
        public List<LabData> GetLabData(List<QueryCustome> Model)
        {
            try
            {
                ApiViewModel ViewModel = new ApiViewModel();
                List<LabData> labDataList = new List<LabData>();

                foreach (var item in Model)
                {
                    var encode = HttpUtility.UrlEncode(item.Barcode);
                    var url = "http://211.75.23.69:7777/GETLABDATA?" + encode;
                    var request = WebRequest.Create(url);

                    request.Method = "GET";
                    request.ContentType = "application/json;charset=UTF-8";

                    var response = request.GetResponse() as HttpWebResponse;
                    var responseStream = response.GetResponseStream();
                    var reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    var srcString = reader.ReadToEnd();

                    var result = JsonConvert.DeserializeObject<Dictionary<string, Dictionary<string, dynamic>>>(srcString);

                    foreach (var r in result.Values)
                    {
                        LabData labdata = new LabData();
                        List<LabDataitem> labDataitemList = new List<LabDataitem>();
                        foreach (var data in r)
                        {
                            switch (data.Key)
                            {
                                case "Barcode":
                                    labdata.Barcode = item.Barcode;
                                    labdata.Old_Barcode = data.Value;
                                    break;

                                case "CheckDate":
                                    labdata.CheckDate = data.Value;
                                    break;

                                default:
                                    if (data.Key.Contains("Item"))
                                    {
                                        LabDataitem labdataitem = new LabDataitem();
                                        labdataitem = JsonConvert.DeserializeObject<LabDataitem>(data.Value.ToString());
                                        labDataitemList.Add(labdataitem);
                                    }
                                    break;
                            }
                        }
                        labdata.Item = labDataitemList;
                        labDataList.Add(labdata);
                    }
                }
                return labDataList;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public bool InsertData(List<QueryCustome> Q, List<AssessTable> A, List<GaugeData> G, List<LabData> L)
        {
            try
            {
                var error = 0;
                //QueryCustom
                Dictionary<string, string> q_guid = new Dictionary<string, string>();
                foreach (var item in Q)
                {
                    //檢查是否重複新增 (檢查Barcode 身分證號碼)
                    var check = _context.query_custome.Where(x => x.status != "D" && x.barcode == item.Barcode && x.user_id == item.ID).Any();
                    if (check == false)
                    {
                        var guid = Guid.NewGuid().ToString();
                        query_custome q = new query_custome();
                        q.guid = guid;
                        q.title = item.Name;
                        q.barcode = item.Barcode;
                        q.customername = item.CustomerName;
                        q.user_id = item.ID;
                        q.name = item.Name;
                        q.sex = item.Sex;
                        q.birthday = item.Birthday;
                        q.checkdate = item.CheckDate;
                        q.no = item.No;
                        q.age = item.Age;
                        q.chart = item.Chart;
                        q.sogi = item.Sogi;
                        q.email = item.EMail;
                        q.status = "N";
                        q.modifydate = DateTime.Now;
                        q.create_date = DateTime.Now;
                        q.lang = "tw";
                        _context.Entry(q).State = System.Data.Entity.EntityState.Added;

                        q_guid.Add(guid, item.Barcode);
                    }
                    else
                    {
                        error += 1;
                    }
                }

                //AssessTable
                foreach (var item in A)
                {
                    //檢查是否重複新增 (檢查Barcode 身分證號碼)
                    var check = _context.assess_table.Where(x => x.status != "D" && x.barcode == item.Barcode && x.user_id == item.ID).Any();
                    if (check == false && q_guid.Where(x => x.Value == item.Barcode).Any())
                    {
                        assess_table a = new assess_table();
                        a.guid = Guid.NewGuid().ToString();
                        a.barcode = item.Barcode;
                        a.user_id = item.ID;
                        a.name = item.Name;
                        a.sex = item.Sex;
                        a.birthday = item.Birthday;
                        a.checkdate = item.CheckDate;
                        a.no = item.No;
                        a.age = item.Age;
                        a.chart = item.Chart;
                        a.sogi = item.Sogi;
                        a.email = item.Email;
                        a.chiefcomplaint = item.ChiefComplaint;
                        a.pasthistory = item.PastHistory;
                        a.familyhistory = item.FamilyHistory;
                        a.allergyhistory = item.AllergyHistory;
                        a.vaccinationhistory = item.VaccinationHistory;
                        a.livinghabit = item.LivingHabit;
                        a.status = "Y";
                        a.modifydate = DateTime.Now;
                        a.create_date = DateTime.Now;
                        a.lang = "tw";
                        a.query_guid = q_guid.Where(x => x.Value == item.Barcode).First().Key;
                        _context.Entry(a).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                //GaugeData
                foreach (var item in G)
                {
                    //檢查是否重複新增 (檢查Barcode 體檢日)
                    var check = _context.gauge_data.Where(x => x.status != "D" && x.barcode == item.Barcode && x.checkdate == item.CheckDate).Any();
                    if (check == false && q_guid.Where(x => x.Value == item.Barcode).Any())
                    {
                        List<GaugeDataitem> result = new List<GaugeDataitem>();

                        //處理項目
                        foreach (var Dataitem in item.Item)
                        {
                            //取得對應類別
                            var category = _context.data_category.Where(x => x.status == "Y" && x.itemNo == Dataitem.ItemNo).SingleOrDefault();
                            if (category != null)
                            {
                                Dataitem.system_name = category.itemSystem;
                                Dataitem.category_name = category.itemCategory;

                                result.Add(Dataitem);
                            }
                        }

                        //將資料至轉換成JSON
                        var jsondata = JsonConvert.SerializeObject(result);

                        //寫入資料庫
                        gauge_data g = new gauge_data();
                        g.guid = Guid.NewGuid().ToString();
                        g.barcode = item.Barcode;
                        g.old_barcode = item.Old_Barcode;
                        g.checkdate = item.CheckDate;
                        g.content = jsondata;
                        g.status = "Y";
                        g.modifydate = DateTime.Now;
                        g.create_date = DateTime.Now;
                        g.lang = "tw";
                        g.query_guid = q_guid.Where(x => x.Value == item.Barcode).First().Key;
                        _context.Entry(g).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                //LabData
                foreach (var item in L)
                {
                    //檢查是否重複新增 (檢查Barcode 體檢日)
                    var check = _context.lab_data.Where(x => x.status != "D" && x.barcode == item.Barcode && x.checkdate == item.CheckDate).Any();
                    if (check == false && q_guid.Where(x => x.Value == item.Barcode).Any())
                    {
                        List<LabDataitem> result = new List<LabDataitem>();

                        //處理項目
                        foreach (var Dataitem in item.Item)
                        {
                            //取得對應類別
                            var category = _context.data_category.Where(x => x.status == "Y" && x.itemNo == Dataitem.ItemNo).SingleOrDefault();
                            if (category != null)
                            {
                                Dataitem.system_name = category.itemSystem;
                                Dataitem.category_name = category.itemCategory;

                                result.Add(Dataitem);
                            }
                        }

                        //將資料轉換成JSON
                        var jsondata = JsonConvert.SerializeObject(result);

                        //寫入資料庫
                        lab_data l = new lab_data();
                        l.guid = Guid.NewGuid().ToString();
                        l.barcode = item.Barcode;
                        l.old_barcode = item.Old_Barcode;
                        l.checkdate = item.CheckDate;
                        l.content = jsondata;
                        l.status = "Y";
                        l.modifydate = DateTime.Now;
                        l.create_date = DateTime.Now;
                        l.lang = "tw";
                        l.query_guid = q_guid.Where(x => x.Value == item.Barcode).First().Key;
                        _context.Entry(l).State = System.Data.Entity.EntityState.Added;
                    }
                    else
                    {
                        error += 1;
                    }
                }

                if (error > 0)
                {
                    return false;
                }
                else
                {
                    _context.SaveChanges();
                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}