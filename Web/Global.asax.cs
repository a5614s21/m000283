﻿using Google.Apis.Auth.OAuth2;
using Newtonsoft.Json;
using Pechkin;
using System;
using System.Collections.Generic;
using System.Drawing.Printing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Web.Models;
using Web.Repository;
using Web.Service;

namespace Web
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            UpdateGAWork work = new UpdateGAWork();
            work.StarWork();
        }

        public class UpdateGAWork
        {
            public static object oLock = new object();
            private static Timer timer;

            //開始背景作業
            public void StarWork()
            {
                TimeSpan delayTime = new TimeSpan(0, 0, 5);         //應用程式起動後多久開始執行
                TimeSpan intervalTime = new TimeSpan(0, 1, 0);          //應用程式起動後間隔多久重複執行
                TimerCallback timerDelegate = new TimerCallback(BatchMethod);           //委派呼叫方法
                timer = new Timer(timerDelegate, null, delayTime, intervalTime);            //產生 timer
            }

            public void startprint(string bid, string userid, string filePath, string guid)
            {
                try
                {
                    //Language
                    string lang = "tw";

                    //Model
                    QueryCustomeService queryCustomeService = new QueryCustomeService();

                    //string url = "http://localhost:9283/siteadmin/DownloadReport?bid=" + bid;
                    //string url = "http://iis.youweb.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;
                    string url = "http://medirep.mornjoy.com.tw/siteadmin/DownloadReport?bid=" + bid;
                    //string url = "http://iis.24241872.tw/projects/public/m000283/test/siteadmin/DownloadReport?bid=" + bid;

                    //var decodeUserid = Encoding.UTF8.GetString(Convert.FromBase64String(userid));
                    var decodeUserid = userid;
                    var judgedata = queryCustomeService.GetData(lang, bid);

                    if (judgedata == null)
                    {

                    }
                    else
                    {
                        string judgeUserid = judgedata.user_id.Trim();

                        if (judgeUserid != decodeUserid)
                        {

                        }
                        else
                        {
                            var data = queryCustomeService.GetData_Backend(lang, bid);

                            using (IPechkin pechkin = Factory.Create(new GlobalConfig().SetMargins(0, 0, 0, 0).SetPaperSize(PaperKind.A4)))
                            {
                                ObjectConfig oc = new ObjectConfig();

                                oc.SetPrintBackground(true)
                                     .SetPageUri(url);

                                byte[] pdf = pechkin.Convert(oc);


                                //FileStream fs = new FileStream(filePath,FileMode.Create,FileAccess.Write);

                                //var writer = new BinaryWriter(File.OpenWrite(filePath));
                                //writer.Write(pdf);


                                //string folderName = @"c:\";
                                //var stream = File.Create(folderName);
                                //stream.Write(pdf,0,pdf.Length);

                                //return File(pdf, "application/pdf", data.name + "_報告書.pdf");




                                // Specify a name for your top-level folder.
                                string folderName = @"c:\report";

                                // To create a string that specifies the path to a subfolder under your
                                // top-level folder, add a name for the subfolder to folderName.
                                //string pathString = System.IO.Path.Combine(folderName, bid);
                                string pathString = System.IO.Path.Combine(folderName);

                                // You can write out the path name directly instead of using the Combine
                                // method. Combine just makes the process easier.
                                string pathString2 = @"c:\report\";

                                // You can extend the depth of your path if you want to.
                                //pathString = System.IO.Path.Combine(pathString, "SubSubFolder");

                                // Create the subfolder. You can verify in File Explorer that you have this
                                // structure in the C: drive.
                                //    Local Disk (C:)
                                //        Top-Level Folder
                                //            SubFolder
                                System.IO.Directory.CreateDirectory(pathString);

                                // Create a file name for the file you want to create.
                                //string fileName = System.IO.Path.GetRandomFileName() + ".pdf";
                                string fileName = guid + bid + ".pdf";

                                // This example uses a random string for the name, but you also can specify
                                // a particular name.
                                //string fileName = "MyNewFile.txt";

                                // Use Combine again to add the file name to the path.
                                //pathString = System.IO.Path.Combine(pathString, fileName)
                                pathString = System.IO.Path.Combine(filePath, fileName);


                                // Verify the path that you have constructed.
                                Console.WriteLine("Path to my file: {0}\n", pathString);

                                // Check that the file doesn't already exist. If it doesn't exist, create
                                // the file and write integers 0 - 99 to it.
                                // DANGER: System.IO.File.Create will overwrite the file if it already exists.
                                // This could happen even with random file names, although it is unlikely.
                                if (!System.IO.File.Exists(pathString))
                                {
                                    try
                                    {
                                        using (System.IO.FileStream fs = System.IO.File.Create(pathString))
                                        {
                                            foreach (var i in pdf)
                                            {
                                                fs.WriteByte(i);
                                            }
                                        }
                                    }
                                    catch (System.IO.IOException e)
                                    {
                                        Console.WriteLine(e.Message);
                                        MornjoyContext DB = new MornjoyContext();
                                        system_log syslog = new system_log();
                                        syslog.guid = Guid.NewGuid().ToString();
                                        syslog.ip = FunctionService.GetIP();
                                        syslog.types = "";

                                        syslog.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                        syslog.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                                        DB.system_log.Add(syslog);
                                        DB.SaveChanges();
                                    }

                                }
                                else
                                {
                                    try
                                    {
                                        using (System.IO.FileStream fs = System.IO.File.Create(pathString))
                                        {
                                            foreach (var i in pdf)
                                            {
                                                fs.WriteByte(i);
                                            }
                                        }
                                    }
                                    catch (System.IO.IOException e)
                                    {
                                        Console.WriteLine(e.Message);
                                        MornjoyContext DB = new MornjoyContext();
                                        system_log syslog = new system_log();
                                        syslog.guid = Guid.NewGuid().ToString();
                                        syslog.ip = FunctionService.GetIP();
                                        syslog.types = "";

                                        syslog.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                        syslog.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                                        DB.system_log.Add(syslog);
                                        DB.SaveChanges();

                                    }

                                }

                                // Read and display the data from your file.
                                try
                                {
                                    byte[] readBuffer = System.IO.File.ReadAllBytes(pathString);
                                    foreach (byte b in readBuffer)
                                    {
                                        Console.Write(b + " ");
                                    }

                                }
                                catch (System.IO.IOException e)
                                {
                                    Console.WriteLine(e.Message);
                                    MornjoyContext DB = new MornjoyContext();
                                    system_log syslog = new system_log();
                                    syslog.guid = Guid.NewGuid().ToString();
                                    syslog.ip = FunctionService.GetIP();
                                    syslog.types = "";

                                    syslog.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                                    syslog.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                                    DB.system_log.Add(syslog);
                                    DB.SaveChanges();

                                }

                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    MornjoyContext DB = new MornjoyContext();
                    system_log syslog = new system_log();
                    syslog.guid = Guid.NewGuid().ToString();
                    syslog.ip = FunctionService.GetIP();
                    syslog.types = "";

                    syslog.create_date = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                    syslog.modifydate = DateTime.Parse(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));

                    DB.system_log.Add(syslog);
                    DB.SaveChanges();
                }
            }

            //背景批次方法
            private void BatchMethod(object pStatus)
            {
                lock (oLock)
                {
                    //排程
                    string exchangeIpdateTime = DateTime.Now.ToString("HH");
                    string exchangeIpdateTimeMin = DateTime.Now.ToString("mm");
                    string exchangeIpdateTimeDay = DateTime.Now.ToString("dd");

                    var GaData = new GoogleAnalyticsService();
                    try
                    {
                        ServiceAccountCredential credential = GaData.GoogleAnalytics();

                        MornjoyContext DB = new MornjoyContext();

                        #region 年度數量

                        Google.Apis.Analytics.v3.Data.GaData ThisYear = GaData.ThisYear(credential);
                        string Yearjson = JsonConvert.SerializeObject(ThisYear.Rows);
                        int ThisYearNum = DB.google_analytics.Where(m => m.area == "ThisYear").ToList().Count;
                        if (ThisYearNum == 0)
                        {
                            google_analytics GAThisYear = new google_analytics();
                            GAThisYear.guid = Guid.NewGuid();
                            GAThisYear.area = "ThisYear";
                            GAThisYear.content = "";
                            GAThisYear.create_date = DateTime.Now;
                            GAThisYear.modifydate = DateTime.Now;
                            DB.google_analytics.Add(GAThisYear);
                        }
                        else
                        {
                            google_analytics GAThisYear = DB.google_analytics.Where(m => m.area == "ThisYear").FirstOrDefault();
                            GAThisYear.content = Yearjson;
                        }

                        #endregion

                        #region 瀏覽器使用

                        Google.Apis.Analytics.v3.Data.GaData ThisBrowser = GaData.ThisBrowser(credential);
                        string Browserjson = JsonConvert.SerializeObject(ThisBrowser.Rows);
                        int ThisBrowserNum = DB.google_analytics.Where(m => m.area == "ThisBrowser").ToList().Count;

                        if (ThisBrowserNum == 0)
                        {
                            google_analytics GAThisBrowser = new google_analytics();
                            GAThisBrowser.guid = Guid.NewGuid();
                            GAThisBrowser.area = "ThisBrowser";
                            GAThisBrowser.content = "";
                            GAThisBrowser.create_date = DateTime.Now;
                            GAThisBrowser.modifydate = DateTime.Now;
                            DB.google_analytics.Add(GAThisBrowser);
                        }
                        else
                        {
                            google_analytics GAThisBrowser = DB.google_analytics.Where(m => m.area == "ThisBrowser").FirstOrDefault();
                            GAThisBrowser.content = Browserjson;
                        }

                        #endregion

                        DB.SaveChanges();
                        Thread.Sleep(500);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            }
        }
    }
}