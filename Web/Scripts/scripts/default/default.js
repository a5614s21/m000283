// JavaScript Document
$(document).ready(function() {
/*document STAR*/

    /* ==========================================================================
            	* default
       ==========================================================================*/
 	
	/*== default  =========================== */
    h = $(window).height();
    w = $(window).width();
	

	
    /* ==========================================================================
         	* resize 螢幕縮放動作
    ==========================================================================*/
     resizeCss();
	$(window).resize(function() {
        h = $(window).height();
        w = $(window).width();
		
        resizeCss();
		
    });

    function resizeCss() {
        h = $(window).height();
        w = $(window).width();
		/*== default [ 等比形狀 ]   ================= */
        $('.pro55').each(function() {
            $(this).css("height", $(this).width());
        });
        $('.pro64').each(function() {
            $(this).css("height", $(this).width() / 1.54);
        });
        $('.pro21').each(function() {
            $(this).css("height", $(this).width() / 2);
        });
		
        /*== * 響應式圖片 [  img ]  ============================ */
        $('.jqimgFill').each(function() {
            $(this).imgLiquid();
        });
        $('.jqimgFill-tl').each(function() {
            $(this).imgLiquid({
                horizontalAlign: "left",
                verticalAlign: "top"
            });
        });
        $('.jqimgFill-tr').each(function() {
            $(this).imgLiquid({
                horizontalAlign: "right",
                verticalAlign: "top"
            });
        });
    }
	/* ==========================================================================
         	* load 
    ==========================================================================*/
	$(window).load(function(){
        
				
	});

/*document END*/
});