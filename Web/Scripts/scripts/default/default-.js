// JavaScript Document
$(document).ready(function() {
/*document STAR*/

    /* ==========================================================================
            	* default
       ==========================================================================*/
 	
	/*== default  =========================== */
    h = $(window).height();
    w = $(window).width();
	
	cooY = h / 2;
	cooX = w / 2;
	/*== welcom  =========================== */
	  
	  /*定義物件*/
	  var welcom_div = $('#welcom');
	  var welcom_logo = $('#welcom .logo');
	  var welcom_h1   = $('#welcom .text h1 span');
	  var welcom_text = $('#welcom .text');
	  var welcom_h2 = $('#welcom .text h2 span');
	  var welcom_line = $('#welcom .text .line');
	  var welcom_skip = $('#welcom .skip')
	  
	  /*畫面一開始位置*/
	  welcom_logo.css({transform: 'scale(1) rotate(-200deg)',top:"0%",left:"30%"});
	  welcom_text.css({top:cooY-100,left:cooX - 120});
	  welcom_h1.css({top:-15,opacity:0});
	  welcom_h2.css({bottom:-15,opacity:0});
	  welcom_line.css({opacity:0});
	  welcom_skip.css({top:cooY + 100,left:cooX - 54});
	  
   		welcom_div.each(function() {
			welcom_logo.animate({
				transform: 'scale(0.5) rotate(0deg)',
				top:cooY - 175,
				left: cooX - 250,
			},5000,"easeOutCubic");
		
			welcom_line.delay( 2500 ).animate({
				opacity:1,
			},1000,"easeOutCubic");
		
			welcom_h1.delay( 3700 ).animate({
				top:0,
				opacity:1
			},2000,"easeInOutQuad");
			
			welcom_h2.delay( 3000 ).animate({
				bottom:0,
				opacity:1
			},2000,"easeInOutQuad");
				
		});
	
    /* ==========================================================================
         	* resize 螢幕縮放動作
    ==========================================================================*/
     resizeCss();
	$(window).resize(function() {
        h = $(window).height();
        w = $(window).width();
		welcom();
        resizeCss();
		
    });

    function resizeCss() {
        h = $(window).height();
        w = $(window).width();
		/*== default [ 等比形狀 ]   ================= */
        $('.pro55').each(function() {
            $(this).css("height", $(this).width());
        });
        $('.pro64').each(function() {
            $(this).css("height", $(this).width() / 1.54);
        });
        $('.pro21').each(function() {
            $(this).css("height", $(this).width() / 2);
        });
		
        /*== * 響應式圖片 [  img ]  ============================ */
        $('.jqimgFill').each(function() {
            $(this).imgLiquid();
        });
        $('.jqimgFill-tl').each(function() {
            $(this).imgLiquid({
                horizontalAlign: "left",
                verticalAlign: "top"
            });
        });
        $('.jqimgFill-tr').each(function() {
            $(this).imgLiquid({
                horizontalAlign: "right",
                verticalAlign: "top"
            });
        });
    }
	/* ==========================================================================
         	* load 
    ==========================================================================*/
	$(window).load(function(){
				
	});
/*document END*/
});