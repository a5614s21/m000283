// JavaScript Document
$(document).ready(function() {
	$('.report .print-Box').each(function() {
		
		printPage = 1514;//設定一頁高度
		printheight =  $(this).height();//每個主題頁面高度
		
		printInnerPage = Math.ceil( printheight / printPage );
		pageNum = 1; //預設分頁數
		printName = $(this).attr("title");//每個主題標題
		
		//設定每主題分頁後最小高
		$(this).css({
			  height : printPage *printInnerPage
		});
		printheightNew =  $(this).height();//每個主題頁面高度
		//alert(printheightNew);
		
		/*== 主題內分頁判斷  =========================== */
		$(this).find('.pageb').each(function() {
			var top = $(this).position().top; //取得每個 pageb y值
			var height = $(this).height();//取得每個 pageb 高
			
			if (top + height > printPage * pageNum - 100) {
				pagebottomPosition = printPage * pageNum - top -2;
				//alert(pagebottomPosition);
				$(this).before('<div class="footerH"><div class="footer"></div></div><div class="head"></div>');
				$(this).prevAll(".footerH").css("height",pagebottomPosition);
				pageNum ++;
			}
			
		});
        
		/*== 主題內標題  =========================== */
		/*頁首 - 標題*/
		$(this).find('.head').each(function() {
			$(this).append('<div class="stitle">王曉明先生 │ 體檢號 ‧ 2658012 │ 體檢日 ‧ 2014/09/24</div>');
		});
        
		/*頁首 - 標題靠左靠右*/
		$('.report .stitle:even').each(function() {
			$(this).addClass('even'); 
		});
		
		/*頁尾 - 頁碼*/
		$(this).find('.footer').each(function() {
			var index = $('.report .footer').index(this) + 1;
			$(this).append( '<span class="pageName">' + printName + '</span> / '+'<span class="pageNum">'+ index+ '</span>' );
		});
	});
});
