// JavaScript Document
$(window).load(function () {
    var max_pages = 100;
    var page_count = 0;
    var userTemplate = $('.head-template').html();
    var printName;

    function snipMe() {
        page_count++;
        if (page_count > max_pages) {
            return;
        }
        // 當前預設A4總高度 - A4紙張高度
        var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
        // 編列所有元件群
        var children = $(this)
            .children()
            .toArray();
        // 需要分離的元件
        var removed = [];

        // 如果剩餘高度大於 0 且元件群內還有元件
        while (long > 0 && children.length > 0) {
            // 取出元件
            var child = children.pop();
            // console.log(child);
            // 從DOM中刪除此元件
            $(child).detach();
            // 分離列表中添加此元件
            removed.unshift(child);
            // 計算目前所剩餘的高度
            long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
            // console.log(long);
        }
        if (removed.length > 0) {
            // 分頁元素完成時進入
            var a4 = $('<div class="A4"></div>');
            a4.append('<div class="head"></div>');
            a4.append(removed);
            $(this).after(a4);
            snipMe.call(a4[0]);
        }
    }

    function tableBreaker() {
        $('.tabelList').each(function () {
            var table = $(this);
            var classname = table.attr('class');
            var tr = table.find('tr');
            tr.each(function (i) {
                var splitBy = i;
                var newTable = $(
                    '<table class="' + classname + '"><tbody></tbody></table>'
                );
                newTable.find('tbody').append(this);
                table.before(newTable);
                table
                    .find('tr')
                    .slice(splitBy)
                    .remove();
            });
        });
    }

    function pageSplit() {
        $('.report .print-Box').each(function () {
            printName = $(this).attr('title');
            $(this)
                .find('.A4')
                .each(function () {
                    snipMe.call(this);
                });
            $(this)
                .find('.A4')
                .each(function () {
                    var footer =
                        '<div class="footer">' +
                        '<span class="pageName">' +
                        printName +
                        '</span> / ' +
                        '<span class="pageNum">' +
                        pageNum +
                        '</span>' +
                        '</div>';
                    $(this).append(footer);
                    $(this).find('.head').append(userTemplate);
                    $(this).attr("page", pageNum);
                    pageNum++;
                });
        });

        $('.pagenum').each(function () {
            var id = $(this).attr("title");

            var page = $('div.A4[id=' + id + ']').attr("page");
            var subpage = $('.pagenumber[id="' + id + '"]').parent('div.A4').attr("page");
            var digitalpage = $('.pagenumber[id="' + id + '"]').parents('div.A4').attr("page");

            if (page != undefined) {
                $(this).text(page);
            }
            else if (subpage != undefined) {
                $(this).text(subpage);
            }
            else {
                $(this).text(digitalpage);
            }
        });
    }

    var pageIndex = 1;
    var pageNum = 1;
    tableBreaker();
    pageSplit();

    var page = $('.A4');
    var pageBlood = 45;
    // Menu Breaker
    var listItem = $('#page-catalog.A4-nomodify .list > div');
    if (listItem.length > 36) {
        var splitList = $('<div class="list"></div>');
        var splitListItems = listItem.splice(35);
        splitList.append(splitListItems);
        $('#page-catalog.A4-nomodify .main').append(splitList);
    }
    page.each(function (i) {
        var element = $(this).children().not(".head, .header");
        var head = $(this).find('.head');
        var header = $(this).find('.header');

        if (i % 2 != 0) {
            element.css('transform', 'translateX(' + pageBlood + 'px)');
            head.css('paddingLeft', 100 + 'px');
            head.css('paddingRight', 30 + 'px');
            header.css('paddingLeft', 135 + 'px');
            header.css('paddingRight', 45 + 'px');
        } else {
            element.css('transform', 'translateX(' + pageBlood * -1 + 'px)');
            head.css('paddingRight', 100 + 'px');
            head.css('paddingLeft', 30 + 'px');
            header.css('paddingRight', 135 + 'px');
            header.css('paddingLeft', 45 + 'px');
        }
    });

    $('#page-body > *').css('transform', 'translateX(' + pageBlood * -1 + 'px)');
    $('#page-catalog > *').css('transform', 'translateX(' + pageBlood + 'px)');
});