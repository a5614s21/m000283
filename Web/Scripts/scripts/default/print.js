// JavaScript Document
$(document).ready(function () {
    var limit = 1024;
    var max_pages = 100;
    var page_count = 0;
    var userTemplate = $('.head-template').html();
    var printName;

    var ww = $(window).outerWidth();
    var menu = $('.menu-fixed');
    var btnMenu = $('.btn-menu');
    var totalHeight = $(document).height();
    var pageHeight = 1300;
    var pageTotal = Math.ceil(totalHeight / pageHeight);

    function snipMe() {
        page_count++;
        if (page_count > max_pages) {
            return;
        }
        // 當前預設A4總高度 - A4紙張高度
        // console.log($(this)[0]);
        // console.log($(this)[0].scrollHeight);
        var long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
        // 編列所有元件群
        var children = $(this)
            .children()
            .toArray();
        // 需要分離的元件
        var removed = [];

        // 每1300就分離一次

        // 如果剩餘高度大於 0 且元件群內還有元件
        while (long > 0 && children.length > 0) {
            // 取出元件
            var child = children.pop();
            // console.log(child);
            // 從DOM中刪除此元件
            $(child).detach();
            // 分離列表中添加此元件
            removed.unshift(child);
            // 計算目前所剩餘的高度
            long = $(this)[0].scrollHeight - Math.ceil($(this).innerHeight());
            // console.log(long);
        }
        if (removed.length > 0) {
            // 分頁元素完成時進入
            var a4 = $('<div class="A4"></div>');
            a4.append('<div class="head"></div>');
            a4.append(removed);
            $(this).after(a4);
            snipMe.call(a4[0]);
        }
    }

    function tableBreaker() {
        $('.tabelList').each(function () {
            var table = $(this);
            var classname = table.attr('class');
            var tr = table.find('tr');
            tr.each(function (i) {
                var splitBy = i;
                var newTable = $(
                    '<table class="' + classname + '"><tbody></tbody></table>'
                );
                newTable.find('tbody').append(this);
                table.before(newTable);
                table
                    .find('tr')
                    .slice(splitBy)
                    .remove();
            });
        });
    }

    function pageSplit() {
        $('.report .print-Box').each(function () {
            printName = $(this).attr('title');
            $(this)
                .find('.A4')
                .each(function () {
                    snipMe.call(this);
                });
            $(this)
                .find('.A4')
                .each(function () {
                    var footer =
                        '<div class="footer">' +
                        '<span class="pageName">' +
                        printName +
                        '</span> / ' +
                        '<span class="pageNum">' +
                        pageNum +
                        '</span>' +
                        '</div>';
                    $(this).append(footer);
                    $(this).find('.head').append(userTemplate);
                    $(this).attr("page", pageNum);
                    pageNum++;
                });
        });
    }

    if (ww > limit) {
        var pageIndex = 1;
        var pageNum = 1;
        tableBreaker();
        pageSplit();
    }
    // Menu Breaker
    var listItem = $('#page-catalog.A4-nomodify .list > div');
    if (listItem.length > 36) {
        var splitList = $('<div class="list"></div>');
        var splitListItems = listItem.splice(35);
        splitList.append(splitListItems);
        $('#page-catalog.A4-nomodify .main').append(splitList);
    }
    btnMenu.click(function (e) {
        e.preventDefault();
        $(this).toggleClass('active');
        menu.toggleClass('active');
    });
    $('.menu-fixed .main a').click(function () {
        menu.removeClass('active');
        btnMenu.removeClass('active');
    });

    function refresh() {
        ww = $(window).width();
        var w =
            ww < limit ?
                location.reload(true) :
                ww > limit ?
                    location.reload(true) :
                    (ww = limit);
    }
    var tOut;
    $(window).resize(function () {
        var resW = $(window).outerWidth();
        clearTimeout(tOut);
        if ((ww > limit && resW < limit) || (ww < limit && resW > limit)) {
            tOut = setTimeout(refresh, 100);
        }
    });
});