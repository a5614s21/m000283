﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Web.ViewModel
{
    public class LoginViewModel
    {
        public Login_PostViewModel Login_PostViewModel { get; set; }

        public Phone_PostViewModel Phone_PostViewModel { get; set; }

        public Verification_PostViewModel Verification_PostViewModel { get; set; }

        public ForgetVerification_PostViewModel ForgetVerification_PostViewModel { get; set; }

        public Password_PostViewModel Password_PostViewModel { get; set; }

        public Forget_PostViewModel Forget_PostViewModel { get; set; }
    }

    public class Login_PostViewModel
    {
        [Required]
        public string usernumber { get; set; }

        [Required]
        public int year { get; set; }

        [Required]
        public int month { get; set; }

        [Required]
        public int day { get; set; }

        public string birthday { get; set; }

        [Required]
        public string password { get; set; }
    }

    public class Phone_PostViewModel
    {
        [Required]
        public string phone { get; set; }
    }

    public class Forget_PostViewModel
    {
        [Required]
        public string phone { get; set; }
    }


    public class Verification_PostViewModel
    {
        [Required]
        public string code { get; set; }
    }

    public class ForgetVerification_PostViewModel
    {
        [Required]
        public string code { get; set; }
    }

    public class Password_PostViewModel
    {
        [Required]
        public string password { get; set; }

        [Required]
        public string password_confirm { get; set; }
    }
}