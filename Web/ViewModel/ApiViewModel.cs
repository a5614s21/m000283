﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModel
{
    public class ApiViewModel
    {
        //體檢人員名單
        public List<QueryCustome> QueryCustome_ListViewModel { get; set; }

        //身體評估表
        public List<AssessTable> AssessTable_ListViewModel { get; set; }

        //生理量測資料
        public List<GaugeData> GaugeData_ListViewModel { get; set; }

        //檢驗資料
        public List<LabData> LabData_ListViewModel { get; set; }
    }

    public class QueryCustome
    {
        public string Barcode { get; set; }

        public string CustomerName { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public string Sex { get; set; }

        public string Birthday { get; set; }

        public string CheckDate { get; set; }

        public string No { get; set; }

        public string Age { get; set; }

        public string Chart { get; set; }

        public string Sogi { get; set; }

        public string EMail { get; set; }
    }

    public class AssessTable
    {
        public string Barcode { get; set; }

        public string ID { get; set; }

        public string Name { get; set; }

        public string Sex { get; set; }

        public string Birthday { get; set; }

        public string CheckDate { get; set; }

        public string No { get; set; }

        public string Age { get; set; }

        public string Chart { get; set; }

        public string Sogi { get; set; }
        public string Email { get; set; }

        public string ChiefComplaint { get; set; }

        public string PastHistory { get; set; }

        public string FamilyHistory { get; set; }

        public string AllergyHistory { get; set; }

        public string VaccinationHistory { get; set; }

        public string LivingHabit { get; set; }
    }

    public class GaugeData
    {
        public string Barcode { get; set; }

        public string CheckDate { get; set; }

        public string Old_Barcode { get; set; }

        public List<GaugeDataitem> Item { get; set; }
    }

    public class GaugeDataitem
    {
        public string ItemNo { get; set; }

        public string ItemName { get; set; }

        public string Category { get; set; }

        public string Ecategory { get; set; }

        public string CheckCategory { get; set; }

        public string EcheckCategory { get; set; }

        public string EItemName { get; set; }

        public string ItemCategory { get; set; }

        public string EItemCategory { get; set; }

        public string Err { get; set; }

        public string Value { get; set; }

        public string Ref { get; set; }

        public string Suggest { get; set; }

        public string ESuggest { get; set; }

        public string OrderIndex { get; set; }

        public string system_name { get; set; }

        public string category_name { get; set; }
    }

    public class LabData
    {
        public string Barcode { get; set; }

        public string Old_Barcode { get; set; }

        public string CheckDate { get; set; }

        public List<LabDataitem> Item { get; set; }
    }

    public class LabDataitem
    {
        public string ItemNo { get; set; }

        public string ItemName { get; set; }

        public string Category { get; set; }

        public string Ecategory { get; set; }

        public string CheckCategory { get; set; }

        public string EcheckCategory { get; set; }

        public string EItemName { get; set; }

        public string ItemCategory { get; set; }

        public string EItemCategory { get; set; }

        public string Err { get; set; }

        public string Value { get; set; }

        public string Ref { get; set; }

        public string Suggest { get; set; }

        public string ESuggest { get; set; }

        public string OrderIndex { get; set; }

        public string system_name { get; set; }

        public string category_name { get; set; }
    }
}