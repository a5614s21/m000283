﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Web.ViewModel
{
    public class BackEndViewModel
    {
        public pic_ViewModel pic_ViewModel { get; set; }

        public picalt_ViewModel picalt_ViewModel { get; set; }
    }

    public class pic_ViewModel
    {
        public string pic_7010 { get; set; }
        public string pic_7011 { get; set; }
        public string pic_7012 { get; set; }
        public string pic_7013 { get; set; }
        public string pic_7015 { get; set; }
        public string pic_7014 { get; set; }
        public string pic_7016 { get; set; }

        public string pic_7049 { get; set; }
        public string pic_7050 { get; set; }
        public string pic_7051 { get; set; }
        public string pic_7052 { get; set; }
        public string pic_7053 { get; set; }


        public string pic_7033 { get; set; }
        public string pic_7034 { get; set; }
        public string pic_7035 { get; set; }
        public string pic_7036 { get; set; }
        public string pic_7037 { get; set; }
        public string pic_7038 { get; set; }
        public string pic_7039 { get; set; }
        public string pic_7040 { get; set; }
        public string pic_7061 { get; set; }
        public string pic_7062 { get; set; }
        public string pic_8009 { get; set; }
        public string pic_8010 { get; set; }
        public string pic_8011 { get; set; }
        public string pic_8012 { get; set; }
        public string pic_8013 { get; set; }
        public string pic_8014 { get; set; }
        public string pic_7093 { get; set; }

        public string pic_7054 { get; set; }
        public string pic_7055 { get; set; }

        public string pic_7045 { get; set; }
        public string pic_7046 { get; set; }
        public string pic_7029 { get; set; }
        public string pic_7030 { get; set; }
        public string pic_7031 { get; set; }
        public string pic_7070 { get; set; }
        public string pic_7032 { get; set; }
        public string pic_7063 { get; set; }
        public string pic_7076 { get; set; }

        public string pic_7021 { get; set; }
        public string pic_7075 { get; set; }

        public string pic_7091 { get; set; }
        public string pic_7020 { get; set; }
        public string pic_7060 { get; set; }

        public string pic_7008 { get; set; }
        public string pic_7007 { get; set; }

        public string pic_7042 { get; set; }
        public string pic_7041 { get; set; }

        public string pic_7023 { get; set; }
    }

    public class picalt_ViewModel
    {
        public string pic_7010_alt { get; set; }
        public string pic_7011_alt { get; set; }
        public string pic_7012_alt { get; set; }
        public string pic_7013_alt { get; set; }
        public string pic_7015_alt { get; set; }
        public string pic_7014_alt { get; set; }
        public string pic_7016_alt { get; set; }

        public string pic_7049_alt { get; set; }
        public string pic_7050_alt { get; set; }
        public string pic_7051_alt { get; set; }
        public string pic_7052_alt { get; set; }
        public string pic_7053_alt { get; set; }
        public string pic_7033_alt { get; set; }
        public string pic_7034_alt { get; set; }
        public string pic_7035_alt { get; set; }
        public string pic_7036_alt { get; set; }
        public string pic_7037_alt { get; set; }
        public string pic_7038_alt { get; set; }
        public string pic_7039_alt { get; set; }
        public string pic_7040_alt { get; set; }
        public string pic_7061_alt { get; set; }
        public string pic_7062_alt { get; set; }
        public string pic_8009_alt { get; set; }
        public string pic_8010_alt { get; set; }
        public string pic_8011_alt { get; set; }
        public string pic_8012_alt { get; set; }
        public string pic_8013_alt { get; set; }
        public string pic_8014_alt { get; set; }
        public string pic_7093_alt { get; set; }

        public string pic_7054_alt { get; set; }
        public string pic_7055_alt { get; set; }

        public string pic_7045_alt { get; set; }
        public string pic_7046_alt { get; set; }
        public string pic_7029_alt { get; set; }
        public string pic_7030_alt { get; set; }
        public string pic_7031_alt { get; set; }
        public string pic_7070_alt { get; set; }
        public string pic_7032_alt { get; set; }
        public string pic_7063_alt { get; set; }
        public string pic_7076_alt { get; set; }

        public string pic_7021_alt { get; set; }
        public string pic_7075_alt { get; set; }
        public string pic_7091_alt { get; set; }
        public string pic_7020_alt { get; set; }
        public string pic_7060_alt { get; set; }

        public string pic_7008_alt { get; set; }
        public string pic_7007_alt { get; set; }

        public string pic_7042_alt { get; set; }
        public string pic_7041_alt { get; set; }

        public string pic_7023_alt { get; set; }
    }
}