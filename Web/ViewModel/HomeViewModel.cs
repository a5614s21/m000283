﻿using PagedList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Web.Models;

namespace Web.ViewModel
{
    public class HomeViewModel
    {
        public List<query_custome> QueryCustome_ListViewModel { get; set; }

        public IPagedList<query_custome> QueryCustome_PagedListViewModel { get; set; }

        public web_data WebData_ViewModel { get; set; }

        public Dictionary<string, int> SystemOverview_Left_ViewModel { get; set; }

        public Dictionary<string, int> SystemOverview_Right_ViewModel { get; set; }

        public query_custome QueryCustome_ViewModel { get; set; }

        public assess_table BasicAssessment_ViewModel { get; set; }

        public List<Abnormality> Abnormality_ViewModel { get; set; }

        public Dictionary<string, List<Physical>> Physical_ViewModel { get; set; }

        public Dictionary<string, List<Examine>> Examine_ViewModel { get; set; }

        public List<Imaging> Ultrasound_ViewModel { get; set; }

        //public Dictionary<string, List<Digital>> Digital_ViewModel { get; set; }
        public List<Digital> Digital_ViewModel { get; set; }
        //public List<Imaging> Digital_ViewModel { get; set; }

        public List<Imaging> ChestDualEnergy_ViewModel { get; set; }

        public List<Imaging> Xray_ViewModel { get; set; }

        public List<Imaging> Triple_ViewModel { get; set; }

        public List<Imaging> Endoscope_ViewModel { get; set; }

        public List<Imaging> Body_ViewModel { get; set; }

        public List<Imaging> Electrocardiogram_ViewModel { get; set; }

        public List<Imaging> Autonomic_ViewModel { get; set; }

        public List<Imaging> Fundus_ViewModel { get; set; }

        public Dictionary<string, List<Other>> Other_ViewModel { get; set; }
    }

    public class Abnormality
    {
        public string name { get; set; }

        public string value { get; set; }

        public string reference { get; set; }

        public string suggest { get; set; }

        public string e_suggest { get; set; }

        public int orderindex { get; set; }
    }

    public class Physical
    {
        public string name { get; set; }

        public string value { get; set; }

        public string reference { get; set; }

        public int orderindex { get; set; }
        public string err { get; set; }
    }

    public class Examine
    {
        public string name { get; set; }

        public string value { get; set; }

        public string reference { get; set; }

        public int orderindex { get; set; }

        public string category { get; set; }

        public string err { get; set; }
    }

    public class Imaging
    {
        public string name { get; set; }

        public string value { get; set; }

        public int orderindex { get; set; }

        public int itemno { get; set; }

        public string height { get; set; }
        public string width { get; set; }

        public Dictionary<int, List<string>> pic { get; set; }

        public Dictionary<int, List<string>> pic_alt { get; set; }
    }

    public class Digital
    {
        public string name { get; set; }

        public string value { get; set; }

        public int orderindex { get; set; }

        public int itemno { get; set; }
        public string height { get; set; }
        public string width { get; set; }

        public Dictionary<int, List<string>> pic { get; set; }

        public Dictionary<int, List<string>> pic_alt { get; set; }
    }

    public class Other
    {
        public string name { get; set; }

        public string value { get; set; }

        public string reference { get; set; }

        public int orderindex { get; set; }

        public string category { get; set; }

        public int itemno { get; set; }
    }
}